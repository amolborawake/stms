﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using MySql.Data.MySqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Services;

public partial class ProjectMaster : System.Web.UI.Page
{
    static int ProjID;
    static ArrayList idList;
    static ArrayList nameList;
    public void selectdata()
    {
        //string qryStr = "select Name as 'Project Name',StartDate as 'Start Date',EndDate as 'End Date',Status from tbl_ProjectMaster";
        //gv_ProjectMaster.DataSource = Connection.loadData(qryStr);
        //gv_ProjectMaster.DataBind();

    }
    void clearField()
    {
        txtProjName.Text = String.Empty;
        dtpStartDate.Text = String.Empty;
        dtpEndDate.Text = String.Empty;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            dtpStartDate.Attributes.Add("readonly", "readonly");
            dtpEndDate.Attributes.Add("readonly", "readonly");
            if (Connection.conn.State == ConnectionState.Open)
            {
                Connection.conn.Close();
            }
            // set as current month &year as selected 23/03/2016
           // dtpStartDate.Text = System.DateTime.Now.AddHours(12).AddMinutes(30).ToString("dd/MM/yyyy");

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_ProjectMaster", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            idList = new ArrayList();
            nameList = new ArrayList();
            while (dr.Read())
            {
                idList.Add(dr[0]);
                nameList.Add(dr[1]);
            }

            dr.Close();
            Connection.conn.Close();
            selectdata();

            if (!Page.IsPostBack)
            {
                getproject();
                BindData();
                getemp();
                loademp();
                loadgaint();
                display();
                //getEmployeeNames();
            }

            //if (ViewState["pid"] != null)
            //{
            //    projectID.Value = Convert.ToString(ViewState["pid"]);
            //    BindData();
            //}//
            BindData();
        }
        catch
        {
            Connection.conn.Close();
        }
    }



    protected void btnClear_Click(object sender, EventArgs e)
    {
        clearField();
        btnAdd.Text = "Add";
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            int statusValue = 0;
            if (chkStatus.Checked == true)
            {
                statusValue = 1;
            }
            if (btnAdd.Text == "Update")
            {
                //string qry = "insert into tbl_ProjectMaster(Name,StartDate,EndDate,Status) values('" + txtProjName.Text.ToString() + "','" + dtpStartDate.Text.ToString() + "','" + dtpEndDate.Text.ToString() + "'," + statusValue + ")";
                int id = Convert.ToInt32(drpproject.SelectedItem.Value);
                double plantime = Convert.ToDouble(txtplantime.Text == "" ? "0" : txtplantime.Text);
                double pcom = Convert.ToDouble(txtpercentageofcom.Text == "" ? "0" : txtpercentageofcom.Text);
                string qry = "Update tbl_ProjectMaster set Name='" + txtProjName.Text.ToString() + "',StartDate='" + dtpStartDate.Text.ToString() + "',EndDate='" + dtpEndDate.Text.ToString() + "',Status=" + statusValue + ",PlanTime=" + plantime + ",Percetageofcom=" + pcom + " where ProjID=" + id;
                Connection.updateData(qry);
                Connection.conn.Close();
                BindData();
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Details Updated successfully');</script>");
            //clearField();
            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_ProjectMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //Connection.HighLightSelectedRow(gv_ProjectMaster);
            //ProjID = gv_ProjectMaster.SelectedIndex;
            //txtProjName.Text = gv_ProjectMaster.SelectedRow.Cells[0].Text.ToString();
            //dtpStartDate.Text = gv_ProjectMaster.SelectedRow.Cells[1].Text.ToString();
            //dtpEndDate.Text = gv_ProjectMaster.SelectedRow.Cells[2].Text.ToString();
            //string statusValue = gv_ProjectMaster.SelectedRow.Cells[3].Text.ToString();
            //if (statusValue == "Inactive")
            //    chkStatus.Checked = false;
            //else if (statusValue == "Active")
            //    chkStatus.Checked = true;

            btnAdd.Text = "Modify";
        }
        catch
        {
            Connection.conn.Close();
        }
    }

    protected void gv_ProjectMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (e.Row.Cells[3].Text.ToString() == "0")
        //        e.Row.Cells[3].Text = "Inactive";
        //    else if (e.Row.Cells[3].Text.ToString() == "1")
        //        e.Row.Cells[3].Text = "Active";

        //    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gv_ProjectMaster, "Select$" + e.Row.RowIndex);
        //    e.Row.ToolTip = "Click to select this row.";
        //}
    }
    public void getproject()
    {
        // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
        string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.StartDate,tbl_ProjectMaster.EndDate,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_ProjectMaster";
        //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        //Connection.conn.Close();
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();

        // cmd.ExecuteReader();

        MySqlDataReader reader = cmd.ExecuteReader();

        reader.Close();
        drpproject.DataSource = cmd.ExecuteReader();
        drpproject.DataTextField = "Project Name";
        drpproject.DataValueField = "ProjID";
        drpproject.DataBind();

        //drpproject.Items.Insert(0, "-- Select --");
        Connection.conn.Close();
    }

    public void getemp()
    {
        // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
        string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
        //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        //Connection.conn.Close();
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();

        // cmd.ExecuteReader();

        MySqlDataReader reader = cmd.ExecuteReader();
        //projectList = new List<Tuple<string, string, string>>();
        //while (reader.Read())
        //{
        //    projectList.Add(new Tuple<string, string, string>(reader["Project Name"] as string, reader["StartDate"] as string, reader["EndDate"] as string));
        //}
        reader.Close();
        drpresorses.DataSource = cmd.ExecuteReader();
        drpresorses.DataTextField = "Employee Name";
        drpresorses.DataValueField = "UserID";
        drpresorses.DataBind();

        //drpproject.Items.Insert(0, "-- Select --");
        Connection.conn.Close();
    }
    string gvUniqueID = String.Empty;
    int gvNewPageIndex = 0;
    int gvEditIndex = -1;
    string gvSortExpr = String.Empty;

    protected void BindData()
    {
        //int id = Convert.ToInt32(ViewState["pid"].ToString());
        int id = Convert.ToInt32(drpproject.SelectedItem.Value);
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
        Connection.conn.Open();
        string cmdstr = "SELECT TaskID, ProjID, Name, TaskDetails, PlanStart, PlanEnd, ActulalStart, ActualEnd,PlanTime,ActualTime,percentageOfComplete FROM tbl_TaskMaster where ProjID=" + id;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
        MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
        adp.Fill(ds);
        cmd.ExecuteNonQuery();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            GridView1.DataSource = FromTable;
            GridView1.DataBind();


        }
        else
        {
            Get_EmptyDataTable();

        }
        ds.Dispose();
        Connection.conn.Close();
    }

    protected void BindData1(int id)
    {

        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
        Connection.conn.Open();
        string cmdstr = "SELECT TaskID, ProjID, Name, TaskDetails, PlanStart, PlanEnd, ActulalStart,ActualEnd,PlanTime,ActualTime,percentageOfComplete FROM tbl_TaskMaster where ProjID=" + id;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
        MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
        adp.Fill(ds);
        cmd.ExecuteNonQuery();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            GridView1.DataSource = FromTable;
            GridView1.DataBind();


        }
        else
        {
            Get_EmptyDataTable();

        }
        ds.Dispose();
        Connection.conn.Close();
    }

    public void Get_EmptyDataTable()
    {
        DataTable dtEmpty = new DataTable();


        dtEmpty.Columns.Add(new DataColumn("TaskID", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ProjID", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("TaskDetails", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("Name", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("PlanStart", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("PlanEnd", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ActulalStart", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ActualEnd", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("PlanTime", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ActualTime", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("percentageOfComplete", typeof(string)));

        DataRow datatRow = dtEmpty.NewRow();

        DataRow dr = dtEmpty.NewRow();
        // dtEmpty.Rows.Add(datatRow);

        dr = dtEmpty.NewRow();
        dr["TaskID"] = 0;
        dr["ProjID"] = string.Empty;
        dr["TaskDetails"] = string.Empty;
        dr["Name"] = string.Empty;
        dr["PlanStart"] = string.Empty;
        dr["PlanEnd"] = string.Empty;
        dr["ActulalStart"] = string.Empty;
        dr["ActualEnd"] = string.Empty;
        dr["PlanTime"] = string.Empty;
        dr["ActualTime"] = string.Empty;
        dr["percentageOfComplete"] = string.Empty;



        //Inserting a new row,datatable .newrow creates a blank row
        dtEmpty.Rows.Add(dr);//adding row to the datatable
        GridView1.DataSource = dtEmpty;
        GridView1.DataBind();
    }

    public void Get_EmptyDataTable1() //use sub task
    {

        DataTable dtEmpty = new DataTable();
        dtEmpty.Columns.Add(new DataColumn("SubTaskID", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("Name", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("SubTaskDetail", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("PlanStart", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("PlanEnd", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ActualStart", typeof(string)));
        dtEmpty.Columns.Add(new DataColumn("ActualEnd", typeof(string)));

        DataRow datatRow = dtEmpty.NewRow();

        DataRow dr = dtEmpty.NewRow();
        // dtEmpty.Rows.Add(datatRow);

        dr = dtEmpty.NewRow();
        dr["SubTaskID"] = 0;
        dr["Name"] = string.Empty;
        dr["SubTaskDetail"] = string.Empty;
        dr["PlanStart"] = string.Empty;
        dr["PlanEnd"] = string.Empty;
        dr["ActualStart"] = string.Empty;
        dr["ActualEnd"] = string.Empty;


        //Inserting a new row,datatable .newrow creates a blank row
        dtEmpty.Rows.Add(dr);//adding row to the datatable
        GridView1.DataSource = dtEmpty;
        GridView1.DataBind();
    }

    private string gvSortDir
    {

        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }

    }

    private string GetSortDirection()
    {
        switch (gvSortDir)
        {
            case "ASC":
                gvSortDir = "DESC";
                break;

            case "DESC":
                gvSortDir = "ASC";
                break;
        }
        return gvSortDir;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow row = e.Row;
        string strSort = string.Empty;

        // Make sure we aren't in header/footer rows
        if (row.DataItem == null)
        {
            return;
        }

        //Find Child GridView control
        GridView gv = new GridView();
        gv = (GridView)row.FindControl("GridView2");

        //Check if any additional conditions (Paging, Sorting, Editing, etc) to be applied on child GridView
        if (gv.UniqueID == gvUniqueID)
        {
            gv.PageIndex = gvNewPageIndex;
            gv.EditIndex = gvEditIndex;
            //Check if Sorting used
            if (gvSortExpr != string.Empty)
            {
                GetSortDirection();
                strSort = " ORDER BY " + string.Format("{0} {1}", gvSortExpr, gvSortDir);
            }
            //Expand the Child grid
            ClientScript.RegisterStartupScript(GetType(), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" + ((DataRowView)e.Row.DataItem)["TaskID"].ToString() + "','one');</script>");
        }

        //Prepare the query for Child GridView by passing the Customer ID of the parent row


        string strQRY = "";
        Label t1 = (Label)e.Row.FindControl("lblTaskID");
        int task1 = Convert.ToInt32(t1.Text);


        strQRY = "SELECT SubTaskID,TaskID, Name, SubTaskDetail, PlanStart, PlanEnd,ActualStart,ActualEnd FROM tbl_SubTask WHERE TaskID=" + task1 + " ";
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(strQRY, Connection.conn);
        if (Connection.conn.State == ConnectionState.Closed)
            Connection.conn.Open();
        MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
        var dt = new DataTable();
        dt.Load(dr);
        Connection.conn.Close();
        if (dt.Rows.Count > 0)
        {
            gv.DataSource = dt;//ChildDataSource(((DataRowView)e.Row.DataItem)["TaskID"].ToString(), strSort);
            gv.AllowPaging = true;
            gv.DataBind();
        }
        else
        {
            DataTable dtEmpty = new DataTable();

            dtEmpty.Columns.Add(new DataColumn("TaskID", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("SubTaskID", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("Name", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("SubTaskDetail", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("PlanStart", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("PlanEnd", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("ActualStart", typeof(string)));
            dtEmpty.Columns.Add(new DataColumn("ActualEnd", typeof(string)));

            DataRow datatRow = dtEmpty.NewRow();

            DataRow dr1 = dtEmpty.NewRow();
            // dtEmpty.Rows.Add(datatRow);

            dr1 = dtEmpty.NewRow();
            dr1["TaskID"] = task1;
            dr1["SubTaskID"] = 0;
            dr1["Name"] = string.Empty;
            dr1["SubTaskDetail"] = string.Empty;
            dr1["PlanStart"] = string.Empty;
            dr1["PlanEnd"] = string.Empty;
            dr1["ActualStart"] = string.Empty;
            dr1["ActualEnd"] = string.Empty;




            //Inserting a new row,datatable .newrow creates a blank row
            dtEmpty.Rows.Add(dr1);//adding row to the datatable
            gv.DataSource = dtEmpty;
            gv.DataBind();
        }
        //Add delete confirmation message for Customer//use delete
        //LinkButton l = (LinkButton)e.Row.FindControl("linkDeleteCust");
        //l.Attributes.Add("onclick", "javascript:return " +
        //"confirm('Are you sure you want to delete this Customer " +
        //DataBinder.Eval(e.Row.DataItem, "TaskID") + "')");
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        Label id = GridView1.Rows[e.RowIndex].FindControl("lblTaskID") as Label;
        TextBox name = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        TextBox taskdetail = GridView1.Rows[e.RowIndex].FindControl("txtTaskDetails") as TextBox;
        TextBox planstart = GridView1.Rows[e.RowIndex].FindControl("txtPlanStart1") as TextBox;
        TextBox planend = GridView1.Rows[e.RowIndex].FindControl("txtPlanEnd") as TextBox;
        //TextBox actualstart = GridView1.Rows[e.RowIndex].FindControl("txtActulalStart") as TextBox;
        //TextBox actualend = GridView1.Rows[e.RowIndex].FindControl("txtActualEnd") as TextBox;
        TextBox plantime = GridView1.Rows[e.RowIndex].FindControl("txtplantime") as TextBox;
        //TextBox actualtime = GridView1.Rows[e.RowIndex].FindControl("txtactualtime") as TextBox;
        TextBox percentageOfComplete = GridView1.Rows[e.RowIndex].FindControl("txtpercentageOfComplete") as TextBox;



        GridViewRow row = GridView1.Rows[e.RowIndex];
        //string column1Value = ((TextBox)row.Cells[4].Controls[4]).Text;

        Connection.conn.Open();
        //updating the record 

        //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("Update tbl_TaskMaster set Name='" + name.Text + "',TaskDetails='" + taskdetail.Text + "',PlanStart='" + planstart.Text + "',PlanEnd='" + planend.Text + "',ActulalStart='" + actualstart.Text + "',ActualEnd='" + actualend.Text + "' ,PlanTime=" + Convert.ToDouble(plantime.Text) + ",ActualTime=" + Convert.ToDouble(actualtime.Text) + ",percentageOfComplete=" + Convert.ToDouble(percentageOfComplete.Text) + " where TaskID=" + Convert.ToInt32(id.Text), Connection.conn);

        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("Update tbl_TaskMaster set Name='" + name.Text + "',TaskDetails='" + taskdetail.Text + "',PlanStart='" + planstart.Text + "',PlanEnd='" + planend.Text + "' ,PlanTime=" + Convert.ToDouble(plantime.Text) + ",percentageOfComplete=" + Convert.ToDouble(percentageOfComplete.Text) + " where TaskID=" + Convert.ToInt32(id.Text), Connection.conn);

        cmd.ExecuteNonQuery();
        Connection.conn.Close();
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
        GridView1.EditIndex = -1;
        //Call ShowData method for displaying updated data  
        BindData();
        loadgaint();

    }
    protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.Equals("ADD"))
        {
            int id = Convert.ToInt32(drpproject.SelectedItem.Value);
            TextBox txtName1 = (TextBox)GridView1.FooterRow.FindControl("txtaddName");
            TextBox txtTaskDetails1 = (TextBox)GridView1.FooterRow.FindControl("txtaddTaskDetails");
            TextBox txtPlanStart11 = (TextBox)GridView1.FooterRow.FindControl("txtaddPlanStart1");
            TextBox txtPlanEnd1 = (TextBox)GridView1.FooterRow.FindControl("txtaddPlanEnd");
            //TextBox txtActulalStart1 = (TextBox)GridView1.FooterRow.FindControl("txtaddActulalStart");
            // TextBox txtActualEnd1 = (TextBox)GridView1.FooterRow.FindControl("txtaddActualEnd");

            TextBox txtPlanTime1 = (TextBox)GridView1.FooterRow.FindControl("txtaddplantime");
            //TextBox txtActualTime1 = (TextBox)GridView1.FooterRow.FindControl("txtaddactualtime");
            TextBox txtpercentageOfComplete1 = (TextBox)GridView1.FooterRow.FindControl("txtaddpercentageOfComplete");

            Connection.conn.Open();
            //string cmdstr = "insert into tbl_TaskMaster(Name,TaskDetails,PlanStart,PlanEnd,ActulalStart,ActualEnd,ProjID,PlanTime,ActualTime,percentageOfComplete) values('" + txtName1.Text.ToString() + "','" + txtTaskDetails1.Text.ToString() + "','" + txtPlanStart11.Text.ToString() + "','" + txtPlanEnd1.Text.ToString() + "','" + txtActulalStart1.Text.ToString() + "','" + txtActualEnd1.Text.ToString() + "'," + id + "," + Convert.ToDouble(txtPlanTime1.Text) + "," + Convert.ToDouble(txtActualTime1.Text) + "," + Convert.ToDouble(txtpercentageOfComplete1.Text) + ")";
            string cmdstr = "insert into tbl_TaskMaster(Name,TaskDetails,PlanStart,PlanEnd,ProjID,PlanTime,ActualTime,percentageOfComplete) values('" + txtName1.Text.ToString() + "','" + txtTaskDetails1.Text.ToString() + "','" + txtPlanStart11.Text.ToString() + "','" + txtPlanEnd1.Text.ToString() + "'," + id + "," + Convert.ToDouble(txtPlanTime1.Text) + "," + Convert.ToDouble(0) + "," + Convert.ToDouble(txtpercentageOfComplete1.Text) + ")";

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);

            cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Saved')", true);

            txtName1.Text = "";
            txtTaskDetails1.Text = "";
            txtPlanStart11.Text = "";
            txtPlanEnd1.Text = "";
            //txtActulalStart1.Text = "";
            //txtActualEnd1.Text = "";

            Connection.conn.Close();
            BindData();
            loadgaint();

        }
    }
    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindData();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();

    }


    #region GridView2 Event Handlers
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("AddSubTask"))
        {
            //GridViewRow row = e.Row;
            //Label t1 = (Label)e.Row.FindControl("lblTaskID");
            GridView gvTemp = (GridView)sender;
            //Label id = gvTemp.FindControl("lblTaskID") as Label;
            gvUniqueID = gvTemp.UniqueID;

            string id = gvTemp.DataKeys[0].Value.ToString();

            int task1 = Convert.ToInt32(id);


            //HiddenField txt = (HiddenField)gvTemp.FooterRow.FindControl("hidenfield123");
            TextBox txtsubname = (TextBox)gvTemp.FooterRow.FindControl("txtsubName");
            TextBox txtsubdetial = (TextBox)gvTemp.FooterRow.FindControl("txtSubTaskDetail");
            TextBox txtsubPlanStart = (TextBox)gvTemp.FooterRow.FindControl("txtsubPlanStartadd");
            TextBox txtsubPlanEnd1 = (TextBox)gvTemp.FooterRow.FindControl("txtsubPlanEndadd");
            TextBox txtsubActulalStart = (TextBox)gvTemp.FooterRow.FindControl("txtsubActualStartadd");
            TextBox txtsubActualEnd = (TextBox)gvTemp.FooterRow.FindControl("txtsubActualEndadd");

            Connection.conn.Open();
            string cmdstr = "insert into tbl_SubTask(Name,SubTaskDetail,PlanStart,PlanEnd,ActualStart,ActualEnd,TaskID) values('" + txtsubname.Text.ToString() + "','" + txtsubdetial.Text.ToString() + "','" + txtsubPlanStart.Text.ToString() + "','" + txtsubPlanEnd1.Text.ToString() + "','" + txtsubActulalStart.Text.ToString() + "','" + txtsubActualEnd.Text.ToString() + "'," + task1 + ")";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);

            cmd.ExecuteNonQuery();
            ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('SubTask Saved successfully');</script>");

            txtsubname.Text = "";
            txtsubdetial.Text = "";
            txtsubPlanStart.Text = "";
            txtsubPlanEnd1.Text = "";
            txtsubActulalStart.Text = "";
            txtsubActualEnd.Text = "";

            //Connection.conn.Close();

        }
    }

    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView gvTemp = (GridView)sender;
        gvUniqueID = gvTemp.UniqueID;
        gvEditIndex = e.NewEditIndex;
        GridView1.DataBind();
    }

    protected void GridView2_CancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView gvTemp = (GridView)sender;
        gvUniqueID = gvTemp.UniqueID;
        gvEditIndex = -1;
        GridView1.DataBind();
    }

    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;


            //string id = gvTemp.DataKeys[0].Value.ToString();


            string subtaskid = ((HiddenField)gvTemp.Rows[e.RowIndex].FindControl("hidenfield1")).Value;
            int task1 = Convert.ToInt32(subtaskid);


            //string subtaskid = ((Label)gvTemp.Rows[e.RowIndex].FindControl("SubTaskID")).Text;
            string subtaskname = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtsubName12")).Text;
            string subtaskdetail = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtSubTaskDetail12")).Text;
            string subplansart = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtsubPlanStart")).Text;
            string subplanend = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtsubPlanEnd")).Text;
            string subactualstart = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtsubActualStart")).Text;
            string subactualend = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtsubActualEnd")).Text;

            Connection.conn.Open();
            //updating the record 

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("Update tbl_SubTask set Name='" + subtaskname + "',SubTaskDetail='" + subtaskdetail + "',PlanStart='" + subplansart + "',PlanEnd='" + subplanend + "',ActualStart='" + subactualstart + "',ActualEnd='" + subactualend + "' where SubTaskID=" + task1, Connection.conn);
            cmd.ExecuteNonQuery();
            Connection.conn.Close();

            ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('subtask updated successfully');</script>");

            //Reset Edit Index
            gvEditIndex = -1;

            GridView1.DataBind();
        }
        catch
        {

        }
    }

    protected void GridView2_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {

    }

    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridView2_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
    }
    #endregion


    protected void drpproject_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(drpproject.SelectedItem.Value);
        // BindData1(id);
        BindData();
        display();
        loadgaint();
        loademp();
        //loadstackedchart();
    }
    public void display()//use display project
    {
        int id = Convert.ToInt32(drpproject.SelectedItem.Value);
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
        Connection.conn.Open();
        string cmdstr = "SELECT Name,StartDate,EndDate,plantime,Percetageofcom FROM tbl_ProjectMaster where ProjID=" + id;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
        MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
        adp.Fill(ds);
        cmd.ExecuteNonQuery();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            txtProjName.Text = FromTable.Rows[0]["Name"].ToString();
            dtpStartDate.Text = Convert.ToString(FromTable.Rows[0]["StartDate"]);
            dtpEndDate.Text = Convert.ToString(FromTable.Rows[0]["EndDate"]);
            txtplantime.Text = Convert.ToString(FromTable.Rows[0]["plantime"]);
            txtpercentageofcom.Text = Convert.ToString(FromTable.Rows[0]["Percetageofcom"]);
        }
        Connection.conn.Close();
    }
    protected void btnassigen_Click(object sender, EventArgs e)//use assigen task
    {


        int pid = Convert.ToInt32(drpproject.SelectedItem.Value);
        foreach (ListItem li in drpresorses.Items)
        {
            if (li.Selected == true)
            {
                int uid = Convert.ToInt32(li.Value);
                DataSet ds = new DataSet();
                DataTable FromTable = new DataTable();
                DataTable FromTable1 = new DataTable();
                Connection.conn.Open();
                string cmdstr = "Select TaskID from tbl_TaskMaster where ProjID=" + pid;
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
                MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.ExecuteNonQuery();
                Connection.conn.Close();
                FromTable = ds.Tables[0];
                if (FromTable.Rows.Count > 0)
                {

                    for (int i = 0; i < FromTable.Rows.Count; i++)
                    {
                        Connection.conn.Open();
                        string cmdstr1 = "Select TaskID from tbl_User_Project where ProjID=" + pid + " and UserID=" + uid + " and TaskID=" + FromTable.Rows[i][0].ToString();
                        MySql.Data.MySqlClient.MySqlCommand cmd1 = new MySql.Data.MySqlClient.MySqlCommand(cmdstr1, Connection.conn);
                        MySql.Data.MySqlClient.MySqlDataAdapter adp1 = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd1);
                        ds = new DataSet();
                        adp1.Fill(ds);
                        cmd.ExecuteNonQuery();
                        Connection.conn.Close();
                        FromTable1 = ds.Tables[0];
                        if (FromTable1.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            string qry = "insert into tbl_User_Project(UserID,ProjID,TaskID) values(" + uid + "," + pid + "," + FromTable.Rows[i][0].ToString() + ")";
                            Connection.updateData(qry);
                        }
                    }

                }
                Connection.conn.Close();


            }
        }
    }


    //..........................gantt chart......................................

    static List<Tuple<string, string, string>> projectList;
    string date = "";
    string endDate = "";

    //public void getEmployeeNames()
    //{
    //    // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
    //    string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.StartDate,tbl_ProjectMaster.EndDate,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_User_Project,tbl_ProjectMaster WHERE tbl_ProjectMaster.ProjID=tbl_User_Project.ProjID";
    //    //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

    //    //Connection.conn.Close();
    //    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
    //    Connection.conn.Open();

    //    // cmd.ExecuteReader();

    //    MySqlDataReader reader = cmd.ExecuteReader();
    //    projectList = new List<Tuple<string, string, string>>();
    //    while (reader.Read())
    //    {
    //        projectList.Add(new Tuple<string, string, string>(reader["Project Name"] as string, reader["StartDate"] as string, reader["EndDate"] as string));
    //    }
    //    reader.Close();
    //    drpproject.DataSource = cmd.ExecuteReader();
    //    drpproject.DataTextField = "Project Name";
    //    drpproject.DataValueField = "ProjID";
    //    drpproject.DataBind();

    //    drpproject.Items.Insert(0, "-- Select --");
    //    Connection.conn.Close();
    //}

    public void loadgaint()
    {
        try
        {
            date = "";// projectList[cmbEmpName.SelectedIndex - 1].Item2.ToString();//"04/02/2015";
            endDate = "";// projectList[cmbEmpName.SelectedIndex - 1].Item3.ToString();// "12/12/2016";

            string APBFlg = cmbSelected.SelectedValue;

            int ProjID = Convert.ToInt16(drpproject.SelectedItem.Value.ToString());

            //string qry = "SELECT DISTINCT tbl_TaskMaster.TaskID,tbl_TaskMaster.PlanEnd,tbl_TaskMaster.PlanStart,tbl_TaskMaster.ActulalStart,tbl_TaskMaster.ActualEnd,tbl_TaskMaster.percentageOfComplete,tbl_TaskMaster.Name as 'Task Name' FROM tbl_User_Project,tbl_TaskMaster WHERE tbl_TaskMaster.TaskID=tbl_User_Project.TaskID and tbl_User_Project.ProjID=" + ProjID;  //and tbl_User_Project.UserID=" + Session["loggedInUserID"]

            string qry = "SELECT DISTINCT TaskID,PlanEnd,PlanStart,ActulalStart,ActualEnd,percentageOfComplete,Name as 'Task Name' FROM tbl_TaskMaster WHERE ProjID=" + ProjID;  
            
            /*DataSet ds = new DataSet();//
            MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(qry, Connection.conn);
          //da.SelectCommand.CommandTimeout = 1000;
            da.Fill(ds);*/

            //New Start 2016/02/09
            List<Tuple<string, string, string, string, string, string, string>> taskDetailsArray = new List<Tuple<string, string, string, string, string, string, string>>();
            taskDetailsArray.Clear();
            MySql.Data.MySqlClient.MySqlCommand command = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            try
            {
                MySqlDataReader dataRead = command.ExecuteReader();
                string taskid;
                string planDate;
                while (dataRead.Read())
                {
                    taskid = dataRead["TaskID"].ToString();
                    taskDetailsArray.Add(new Tuple<string, string, string, string, string, string, string>
                         (taskid as string,
                         dataRead["Task Name"] as string,
                         dataRead["PlanStart"] as string,
                         dataRead["PlanEnd"] as string,
                         dataRead["ActulalStart"] as string,
                         dataRead["ActualEnd"] as string,
                         dataRead["percentageOfComplete"] as string));

                    //Plan Start date

                    planDate = dataRead["PlanStart"] as string;
                    if (date.Equals("")) { date = planDate; }
                    string[] dateFor = new string[3];
                    dateFor = date.Split('/');
                    DateTime startComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                    dateFor = planDate.Split('/');
                    DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                    if (DateTime.Compare(compareDate, startComDate) <= 0)//&& DateTime.Compare(compareDate, endComDate) < 0)
                    {
                        date = planDate;
                    }
                    planDate = dataRead["PlanEnd"] as string;
                    if (endDate.Equals("")) { endDate = planDate; }
                    dateFor = planDate.Split('/');
                    compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                    dateFor = endDate.Split('/');
                    DateTime endComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                    dateFor = planDate.Split('/');

                    if (DateTime.Compare(compareDate, endComDate) >= 0)//&& DateTime.Compare(compareDate, endComDate) < 0)
                    {
                        endDate = planDate;
                    }
                    //Plan End date
                }
            }
            catch
            {
                Connection.conn.Close();
            }
            Connection.conn.Close();

            //New Work

            string[] a = new string[3];
            a = date.Split('/');

            string[] a1 = new string[3];
            a1 = endDate.Split('/');

            DateTime start_Date = new DateTime(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]), Convert.ToInt32(a[0])); //exception
            DateTime end_Date = new DateTime(Convert.ToInt32(a1[2]), Convert.ToInt32(a1[1]), Convert.ToInt32(a1[0]));

            //Coloums
            int months = (end_Date.Year - start_Date.Year) * 12 + end_Date.Month - start_Date.Month;

            int noOfDays = DateTime.DaysInMonth(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]));
            DataTable dt = new DataTable();

            dt.Clear(); //rohit

            dt.Columns.Add("Task Names");
            int startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).AddDays(7).Day;
            List<string> colNames = new List<string>();
            for (int count = 0; count <= months; count++)
            {
                for (int i = startDay; i <= noOfDays; i = i + 7)
                {
                    string str = count.ToString();
                    string ColumnName = i.ToString();     // + str;
                    if (ColumnName.Length == 1) ColumnName = "0" + ColumnName;
                    if (start_Date.Month.ToString().Length != 1)
                    {
                        ColumnName = ColumnName + "/" + start_Date.Month;
                    }
                    else
                    {
                        ColumnName = ColumnName + "/0" + start_Date.Month;
                    }
                    DataColumn col = new DataColumn(ColumnName);
                    col.DataType = typeof(string);
                    dt.Columns.Add(col);
                    ColumnName = ColumnName + "/" + start_Date.Year;
                    colNames.Add(ColumnName);
                }

                DateTime nextdate = new DateTime(start_Date.Year, start_Date.Month, 1).AddMonths(1);
                start_Date = nextdate;
                noOfDays = DateTime.DaysInMonth(nextdate.Year, nextdate.Month);

                startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).AddDays(7).Day;
                if (start_Date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                }
                if (start_Date.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    startDay = start_Date.Day;      //AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                }
            }

            Connection.conn.Close();

            a = new string[3];

            a = date.Split('/');

            start_Date = new DateTime(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]), Convert.ToInt32(a[0]));

            List<Tuple<string, string, string>> taskChInTimenew = new List<Tuple<string, string, string>>();
            taskChInTimenew.Clear(); //rohit
            string QUERY = "select * from tbl_ChkInOutDetails";
            string query = "";
            for (int m = 0; m < months; m++)
            {

                if (start_Date.Month.ToString().Length == 1)
                {
                    query = QUERY + start_Date.Year.ToString() + "0" + start_Date.Month.ToString();
                }
                else
                {
                    query = QUERY + start_Date.Year.ToString() + start_Date.Month.ToString();
                }

                MySql.Data.MySqlClient.MySqlCommand comd = new MySql.Data.MySqlClient.MySqlCommand(query, Connection.conn);
                Connection.conn.Open();
                try
                {
                    MySqlDataReader dataRead = comd.ExecuteReader();
                    string taskid;
                    while (dataRead.Read())
                    {
                        taskid = dataRead["TaskID"].ToString();
                        taskChInTimenew.Add(new Tuple<string, string, string>(dataRead["Date"] as string, taskid as string, dataRead["CheckInTime"] as string));
                    }
                    dataRead.Close(); //rohit
                }
                catch
                {
                    Connection.conn.Close();
                }
                Connection.conn.Close();
                DateTime nextdate = new DateTime(start_Date.Year, start_Date.Month, 1).AddMonths(1);
                start_Date = nextdate;
            }


            
            string currentTaskID;
            for (int i = 0; i < taskDetailsArray.Count; i++)
            {
                DataRow drnewRow = dt.NewRow();
                currentTaskID = taskDetailsArray[i].Item1.ToString();
                drnewRow["Task Names"] = taskDetailsArray[i].Item2.ToString(); ;

                string[] dateSplit = new string[3];
                dateSplit = taskDetailsArray[i].Item3.ToString().Split('/');
                DateTime planStartDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
                dateSplit = taskDetailsArray[i].Item4.ToString().Split('/');
                DateTime planEndDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
                if (APBFlg.Equals("Plan") || APBFlg.Equals("Both"))
                {
                    for (int j = 0; j < colNames.Count - 1; j++)
                    {
                        string[] dateFor = new string[3];
                        dateFor = colNames[j].Split('/');
                        DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                        if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) <= 0)
                        {
                            dateFor = colNames[j].Split('/');   //use plan date
                            drnewRow[dateFor[0] + "/" + dateFor[1]] = "Green";
                        }
                    }
                }
                for (int row = 0; row < taskChInTimenew.Count; row++)
                {
                    int col = 0;
                    if (currentTaskID.Equals(taskChInTimenew[row].Item2.ToString()))
                    {
                        for (; col < colNames.Count - 1; col++)
                        {
                            string[] dateFor = new string[3];
                            dateFor = colNames[col].Split('/');
                            DateTime startComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                            dateFor = colNames[col + 1].Split('/');
                            DateTime endComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                            dateFor = taskChInTimenew[row].Item1.ToString().Split('/');
                            DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));


                            if (DateTime.Compare(compareDate, startComDate) >= 0 && DateTime.Compare(compareDate, endComDate) < 0)
                            {
                                if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) > 0)
                                {
                                    if (APBFlg.Equals("Both"))
                                    {
                                        dateFor = colNames[col].Split('/');
                                        drnewRow[dateFor[0] + "/" + dateFor[1]] = "Red";
                                    }
                                }
                                else
                                {
                                    if (APBFlg.Equals("Actual") || APBFlg.Equals("Both"))
                                    {
                                        dateFor = colNames[col].Split('/');
                                        drnewRow[dateFor[0] + "/" + dateFor[1]] = "Blue";
                                    }
                                }
                                break;
                            }
                            /*if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) < 0)
                            {
                                dateFor = colNames[col].Split('/');
                                drnewRow[dateFor[0] + "/" + dateFor[1]] = "Green";
                            }
                            else*/


                        }
                    }
                    else
                    {
                    }
                }


                dt.Rows.Add(drnewRow);
            }

            //End Rows
            gv_TimesheetMonthly.DataSource = dt;
            gv_TimesheetMonthly.DataBind();
            for (int i = 0; i < gv_TimesheetMonthly.Rows.Count; i++)
            {
                for (int col = 1; col < gv_TimesheetMonthly.Rows[i].Cells.Count; col++)
                {

                    if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Blue"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Blue;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Blue;
                    }
                    else if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Green"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Green;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Green;
                    }
                    else if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Red"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Red;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
        catch
        {
            Connection.conn.Close();
            gv_TimesheetMonthly.DataSource = null;
            gv_TimesheetMonthly.DataBind();
        }
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {

        try
        {
            date = "";// projectList[cmbEmpName.SelectedIndex - 1].Item2.ToString();//"04/02/2015";
            endDate = "";// projectList[cmbEmpName.SelectedIndex - 1].Item3.ToString();// "12/12/2016";

            string APBFlg = cmbSelected.SelectedValue;

            int ProjID = Convert.ToInt16(drpproject.SelectedItem.Value.ToString());

            string qry = "SELECT DISTINCT tbl_TaskMaster.TaskID,tbl_TaskMaster.PlanEnd,tbl_TaskMaster.PlanStart,tbl_TaskMaster.ActulalStart,tbl_TaskMaster.ActualEnd,tbl_TaskMaster.percentageOfComplete,tbl_TaskMaster.Name as 'Task Name' FROM tbl_User_Project,tbl_TaskMaster WHERE tbl_TaskMaster.TaskID=tbl_User_Project.TaskID and tbl_User_Project.ProjID=" + ProjID;  //and tbl_User_Project.UserID=" + Session["loggedInUserID"]
            /*DataSet ds = new DataSet();//
            MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(qry, Connection.conn);
          //da.SelectCommand.CommandTimeout = 1000;
            da.Fill(ds);*/

            //New Start 2016/02/09
            List<Tuple<string, string, string, string, string, string, string>> taskDetailsArray = new List<Tuple<string, string, string, string, string, string, string>>();

            MySql.Data.MySqlClient.MySqlCommand command = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            try
            {
                MySqlDataReader dataRead = command.ExecuteReader();
                string taskid;
                string planDate;
                while (dataRead.Read())
                {
                    taskid = dataRead["TaskID"].ToString();
                    taskDetailsArray.Add(new Tuple<string, string, string, string, string, string, string>
                         (taskid as string,
                         dataRead["Task Name"] as string,
                         dataRead["PlanStart"] as string,
                         dataRead["PlanEnd"] as string,
                         dataRead["ActulalStart"] as string,
                         dataRead["ActualEnd"] as string,
                         dataRead["percentageOfComplete"] as string));

                    //Plan Start date

                    planDate = dataRead["PlanStart"] as string;
                    if (date.Equals("")) { date = planDate; }
                    string[] dateFor = new string[3];
                    dateFor = date.Split('/');
                    DateTime startComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                    dateFor = planDate.Split('/');
                    DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                    if (DateTime.Compare(compareDate, startComDate) <= 0)//&& DateTime.Compare(compareDate, endComDate) < 0)
                    {
                        date = planDate;
                    }
                    planDate = dataRead["PlanEnd"] as string;
                    if (endDate.Equals("")) { endDate = planDate; }
                    dateFor = planDate.Split('/');
                    compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                    dateFor = endDate.Split('/');
                    DateTime endComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                    dateFor = planDate.Split('/');

                    if (DateTime.Compare(compareDate, endComDate) >= 0)//&& DateTime.Compare(compareDate, endComDate) < 0)
                    {
                        endDate = planDate;
                    }
                    //Plan End date
                }
            }
            catch
            {
                Connection.conn.Close();
            }
            Connection.conn.Close();

            //New Work

            string[] a = new string[3];
            a = date.Split('/');

            string[] a1 = new string[3];
            a1 = endDate.Split('/');

            DateTime start_Date = new DateTime(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]), Convert.ToInt32(a[0]));
            DateTime end_Date = new DateTime(Convert.ToInt32(a1[2]), Convert.ToInt32(a1[1]), Convert.ToInt32(a1[0]));

            //Coloums
            int months = (end_Date.Year - start_Date.Year) * 12 + end_Date.Month - start_Date.Month;

            int noOfDays = DateTime.DaysInMonth(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]));
            DataTable dt = new DataTable();
            dt.Columns.Add("Task Names");
            int startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).AddDays(7).Day;
            List<string> colNames = new List<string>();
            for (int count = 0; count <= months; count++)
            {
                for (int i = startDay; i <= noOfDays; i = i + 7)
                {
                    string str = count.ToString();
                    string ColumnName = i.ToString();     // + str;
                    if (ColumnName.Length == 1) ColumnName = "0" + ColumnName;
                    if (start_Date.Month.ToString().Length != 1)
                    {
                        ColumnName = ColumnName + "/" + start_Date.Month;
                    }
                    else
                    {
                        ColumnName = ColumnName + "/0" + start_Date.Month;
                    }
                    DataColumn col = new DataColumn(ColumnName);
                    col.DataType = typeof(string);
                    dt.Columns.Add(col);
                    ColumnName = ColumnName + "/" + start_Date.Year;
                    colNames.Add(ColumnName);
                }

                DateTime nextdate = new DateTime(start_Date.Year, start_Date.Month, 1).AddMonths(1);
                start_Date = nextdate;
                noOfDays = DateTime.DaysInMonth(nextdate.Year, nextdate.Month);

                startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).AddDays(7).Day;
                if (start_Date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                }
                if (start_Date.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    startDay = start_Date.Day;      //AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                }
            }

            Connection.conn.Close();

            a = new string[3];

            a = date.Split('/');

            start_Date = new DateTime(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]), Convert.ToInt32(a[0]));

            List<Tuple<string, string, string>> taskChInTimenew = new List<Tuple<string, string, string>>();
            string QUERY = "select * from tbl_ChkInOutDetails";
            string query = "";
            for (int m = 0; m < months; m++)
            {

                if (start_Date.Month.ToString().Length == 1)
                {
                    query = QUERY + start_Date.Year.ToString() + "0" + start_Date.Month.ToString();
                }
                else
                {
                    query = QUERY + start_Date.Year.ToString() + start_Date.Month.ToString();
                }

                MySql.Data.MySqlClient.MySqlCommand comd = new MySql.Data.MySqlClient.MySqlCommand(query, Connection.conn);
                Connection.conn.Open();
                try
                {
                    MySqlDataReader dataRead = comd.ExecuteReader();
                    string taskid;
                    while (dataRead.Read())
                    {
                        taskid = dataRead["TaskID"].ToString();
                        taskChInTimenew.Add(new Tuple<string, string, string>(dataRead["Date"] as string, taskid as string, dataRead["CheckInTime"] as string));
                    }
                }
                catch
                {
                    Connection.conn.Close();
                }
                Connection.conn.Close();
                DateTime nextdate = new DateTime(start_Date.Year, start_Date.Month, 1).AddMonths(1);
                start_Date = nextdate;
            }


            //Rows

            /*DataView view = ds.Tables[0].DefaultView;
            DataTable tbl_ProjectMaster = view.ToTable(true, "Task Name"); 
            string currentTaskID ="";
            bool flg = false;
            foreach (DataRow drProjName in tbl_ProjectMaster.Rows)
            {
                DataRow drnewRow = dt.NewRow();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    flg = false;
                    TimeSpan ts = TimeSpan.Zero;
                    if (drProjName["Task Name"].Equals(dr["Task Name"]))
                    {
                        currentTaskID = dr["TaskID"].ToString();
                           drnewRow["Task Names"] = dr["Task Name"];
                           flg = true;
                    }
                    if (flg)
                    {
                        for (int row = 0; row < taskChInTimenew.Count; row++)
                        {
                            if (currentTaskID.Equals(taskChInTimenew[row].Item2.ToString()))
                            {
                                for (int col = 0; col < colNames.Count - 1; col++)
                                {
                                    string[] dateFor = new string[3];
                                    dateFor = colNames[col].Split('/');
                                    DateTime startComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                                    dateFor = colNames[col + 1].Split('/');
                                    DateTime endComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                                    dateFor = taskChInTimenew[row].Item1.ToString().Split('/');
                                    DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                                    if (DateTime.Compare(compareDate, startComDate) >= 0 && DateTime.Compare(compareDate, endComDate) < 0)
                                    {
                                        dateFor = colNames[col].Split('/');
                                        drnewRow[dateFor[0] + "/" + dateFor[1]] = "CheCk";
                                        break;
                                    }
                                }
                            }
                            else
                            { //break;
                            }
                        }
                    }
                }
                dt.Rows.Add(drnewRow);

            }*/
            string currentTaskID;
            for (int i = 0; i < taskDetailsArray.Count; i++)
            {
                DataRow drnewRow = dt.NewRow();
                currentTaskID = taskDetailsArray[i].Item1.ToString();
                drnewRow["Task Names"] = taskDetailsArray[i].Item2.ToString(); ;

                string[] dateSplit = new string[3];
                dateSplit = taskDetailsArray[i].Item3.ToString().Split('/');
                DateTime planStartDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
                dateSplit = taskDetailsArray[i].Item4.ToString().Split('/');
                DateTime planEndDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
                if (APBFlg.Equals("Plan") || APBFlg.Equals("Both"))
                {
                    for (int j = 0; j < colNames.Count - 1; j++)
                    {
                        string[] dateFor = new string[3];
                        dateFor = colNames[j].Split('/');
                        DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));

                        if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) <= 0)
                        {
                            dateFor = colNames[j].Split('/');
                            drnewRow[dateFor[0] + "/" + dateFor[1]] = "Green";
                        }
                    }
                }
                for (int row = 0; row < taskChInTimenew.Count; row++)
                {
                    int col = 0;
                    if (currentTaskID.Equals(taskChInTimenew[row].Item2.ToString()))
                    {
                        for (; col < colNames.Count - 1; col++)
                        {
                            string[] dateFor = new string[3];
                            dateFor = colNames[col].Split('/');
                            DateTime startComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                            dateFor = colNames[col + 1].Split('/');
                            DateTime endComDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));
                            dateFor = taskChInTimenew[row].Item1.ToString().Split('/');
                            DateTime compareDate = new DateTime(Convert.ToInt32(dateFor[2]), Convert.ToInt32(dateFor[1]), Convert.ToInt32(dateFor[0]));


                            if (DateTime.Compare(compareDate, startComDate) >= 0 && DateTime.Compare(compareDate, endComDate) < 0)
                            {
                                if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) > 0)
                                {
                                    if (APBFlg.Equals("Both"))
                                    {
                                        dateFor = colNames[col].Split('/');
                                        drnewRow[dateFor[0] + "/" + dateFor[1]] = "Red";
                                    }
                                }
                                else
                                {
                                    if (APBFlg.Equals("Actual") || APBFlg.Equals("Both"))
                                    {
                                        dateFor = colNames[col].Split('/');
                                        drnewRow[dateFor[0] + "/" + dateFor[1]] = "Blue";
                                    }
                                }
                                break;
                            }
                            /*if (DateTime.Compare(compareDate, planStartDate) >= 0 && DateTime.Compare(compareDate, planEndDate) < 0)
                            {
                                dateFor = colNames[col].Split('/');
                                drnewRow[dateFor[0] + "/" + dateFor[1]] = "Green";
                            }
                            else*/


                        }
                    }
                    else
                    {
                    }
                }


                dt.Rows.Add(drnewRow);
            }

            //End Rows
            gv_TimesheetMonthly.DataSource = dt;
            gv_TimesheetMonthly.DataBind();
            for (int i = 0; i < gv_TimesheetMonthly.Rows.Count; i++)
            {
                for (int col = 1; col < gv_TimesheetMonthly.Rows[i].Cells.Count; col++)
                {

                    if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Blue"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Blue;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Blue;
                    }
                    else if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Green"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Green;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Green;
                    }
                    else if (gv_TimesheetMonthly.Rows[i].Cells[col].Text.Equals("Red"))
                    {
                        gv_TimesheetMonthly.Rows[i].Cells[col].BackColor = System.Drawing.Color.Red;
                        gv_TimesheetMonthly.Rows[i].Cells[col].ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
        catch
        {
            Connection.conn.Close();
        }

    }

    protected void gv_TimesheetMonthly_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //if (drpproject.SelectedIndex == 0) return;
                // string date = projectList[cmbEmpName.SelectedIndex-1].Item2.ToString();//"04/02/2014";
                // string endDate = projectList[cmbEmpName.SelectedIndex-1].Item3.ToString();// "24/01/2016";

                string[] a = new string[3];
                a = date.Split('/');

                string[] a1 = new string[3];
                a1 = endDate.Split('/');

                DateTime start_Date = new DateTime(Convert.ToInt32(a[2]), Convert.ToInt32(a[1]), Convert.ToInt32(a[0]));
                DateTime end_Date = new DateTime(Convert.ToInt32(a1[2]), Convert.ToInt32(a1[1]), Convert.ToInt32(a1[0]));

                int noOfMonth = (end_Date.Year - start_Date.Year) * 12 + end_Date.Month - start_Date.Month;


                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                String[] months = new string[12] { "jan", "Feb", "Mar", "April", "May", "Jun", "jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "";
                HeaderCell.ColumnSpan = 1;

                HeaderGridRow.Cells.Add(HeaderCell);
                int startMonth = start_Date.Month;
                for (int i = 0; i <= noOfMonth; i++)
                {
                    HeaderCell = new TableCell();
                    HeaderCell.Text = months[startMonth - 1] + start_Date.Year;

                    int startDay = start_Date.AddDays(-(start_Date.DayOfWeek - DayOfWeek.Monday)).AddDays(7).Day;
                    if (start_Date.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        startDay = start_Date.AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                    }
                    if (start_Date.DayOfWeek.Equals(DayOfWeek.Monday))
                    {
                        startDay = start_Date.Day;//AddDays((DayOfWeek.Monday - start_Date.DayOfWeek)).Day;
                    }
                    int noOfWeek = 0;
                    for (int j = startDay; j <= DateTime.DaysInMonth(start_Date.Year, start_Date.Month); j = j + 7)
                    {
                        noOfWeek++;
                    }
                    HeaderCell.ColumnSpan = noOfWeek;

                    HeaderGridRow.Cells.Add(HeaderCell);

                    DateTime nextdate = new DateTime(start_Date.Year, start_Date.Month, 1).AddMonths(1);
                    start_Date = nextdate;
                    startMonth = nextdate.Month;
                }
                gv_TimesheetMonthly.Controls[0].Controls.AddAt(0, HeaderGridRow);
            }
        }
        catch(Exception ex)
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TimesheetMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
    {



        //if (e.Row.RowType == DataControlRowType.DataRow) {
        //    for (int i = 1; i < gv_TimesheetMonthly.Columns.Count - 1; i++)
        //    {
        //        if (gv_TimesheetMonthly.Rows.c[i].HorizontalAlign = HorizontalAlign.Center) ;
        //        {
        //            ((TextBox)e.Row.Cells[i].Controls[0]).MaxLength = 2;
        //        }

        //    }
        //}
       // }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (txtnewproject.Text != "" && txtnewstartdate.Text != "")
        {
            try
            {
                int statusValue = 0;
                if (chkstatus1.Checked == true)
                {
                    statusValue = 1;
                }
                if (btnAdd.Text == "Add")
                {
                    string qry = "insert into tbl_ProjectMaster(Name,StartDate,EndDate,Status) values('" + txtnewproject.Text.ToString() + "','" + txtnewstartdate.Text.ToString() + "','" + txtnewenddate.Text.ToString() + "'," + statusValue + ")";
                    Connection.updateData(qry);
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select max(ProjID) from tbl_ProjectMaster", Connection.conn);
                    Connection.conn.Open();
                    projectID.Value = Convert.ToString(cmd.ExecuteScalar());
                    Connection.conn.Close();
                    BindData();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Details saved successfully');</script>");
                clearField();
                selectdata();
            }
            catch
            {
                Connection.conn.Close();
            }
        }
    }
    [WebMethod]
    public static string saveproject()
    {
        string name = HttpContext.Current.Request.QueryString["name"];
        string startdate = HttpContext.Current.Request.QueryString["startdate"];
        string enddate = HttpContext.Current.Request.QueryString["enddate"];
        string flag1 = HttpContext.Current.Request.QueryString["flag"];
        string plantime = HttpContext.Current.Request.QueryString["plantime"];
        if (name != "" && startdate != "")
        {
            string qry = "insert into tbl_ProjectMaster(Name,StartDate,EndDate,Status,PlanTime,Percetageofcom) values('" + name + "','" + startdate + "','" + enddate + "'," + flag1 + "," + plantime + "," + 0 +")";
            Connection.updateData(qry);
        }
        else
        {
            return "Please Enter Project";
        }
        //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select max(ProjID) from tbl_ProjectMaster", Connection.conn);
        //Connection.conn.Open();
        // cmd.ExecuteScalar();
        Connection.conn.Close();

        return "Project Saved";

    }

    public void loademp()
    {
        int id = Convert.ToInt32(drpproject.SelectedItem.Value);
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
        Connection.conn.Open();
        string cmdstr = "SELECT DISTINCT UserID FROM tbl_User_Project where ProjID=" + id;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
        MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
        adp.Fill(ds);
        cmd.ExecuteNonQuery();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            foreach (DataRow row in FromTable.Rows)
            {
                for (int j = 0; j < drpresorses.Items.Count; j++)
                {
                    string id1 = row["UserID"].ToString();
                    if (id1 == drpresorses.Items[j].Value)
                    {
                        drpresorses.Items[j].Selected = true;

                    }
                }

            }
        }
        Connection.conn.Close();
    }
    protected void cmbSelected_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadgaint();
        BindData();

    }
}