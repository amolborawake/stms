﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="LineGraph.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Line Chart"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblEmpName" runat="server" Text="Project Name*"></asp:Label>
                <asp:DropDownList ID="cmbEmpName" runat="server" CssClass="combobox" AutoPostBack="True"
                    OnSelectedIndexChanged="cmbEmpName_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbEmpName"
                    Display="Dynamic" ErrorMessage="Please Select Project Name." ForeColor="Red"
                    InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="Label1" runat="server" Text="Chart Type:"></asp:Label>
                <asp:DropDownList ID="drpcharttype" runat="server" CssClass="combobox" AutoPostBack="True"
                    OnSelectedIndexChanged="drpcharttype_SelectedIndexChanged">
                    <asp:ListItem Value="LineChart1">Line Chart1</asp:ListItem>
                    <asp:ListItem Value="LineChart2">Line Chart2</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnPreview" runat="server" Text="Preview" Visible="False" />
            </div>
            <div class="EmpMasterConDiv">
                &nbsp;
            </div>
            <div style="padding-left: 9%;">
                <br />
                <table>
                    <tr>
                        <td>
                            <div style="overflow:scroll; height:300px;">
                                <asp:GridView ID="piechartgrid" runat="server" AutoGenerateColumns="false">
                                 <Columns>
                                 <asp:boundfield datafield="Date" headertext="Date"/>
                                 <asp:boundfield datafield="ActualTime" headertext="ActualTime"/>
                                 <asp:boundfield datafield="PlannedTime" headertext="PlannedTime"/>
                                <%-- <asp:boundfield datafield="PercentComplete" headertext="PercentComplete"/>--%>
                                 </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                        <td>
                            <div>
                                <%--chart div--%>
                                <%--<asp:Chart ID="chtCategoriesProductCountBarChart" runat="server">
                                    <Titles>
                                        <asp:Title Text="Number of Products in Categories">
                                        </asp:Title>
                                    </Titles>
                                   <Series>
                                        <asp:Series Name="Categories" ChartType="Line" ChartArea="MainChartArea" BorderWidth="5"
                                            Color="Red">
                                        </asp:Series>
                                         <asp:Series Name="Categories1" ChartType="Line" ChartArea="MainChartArea" BorderWidth="5"
                                            Color="Blue">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="MainChartArea">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>--%>
                                <%--<asp:Chart ID="Chart1" runat="server" Width="1000px" Height="500px"></asp:Chart>--%>
                                <asp:Chart ID="Chart1" runat="server" Width="600px" Height="300px">
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
