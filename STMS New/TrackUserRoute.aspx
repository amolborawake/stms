﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="TrackUserRoute.aspx.cs" Inherits="TrackUserRoute" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Literal ID="js" runat="server"></asp:Literal>
    <body onload="initialize()">
        <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
            height: auto; padding-bottom: 5%;">
            <div style="padding-left: 5%;">
                <div align="center" style="padding-top: 2%; height: 48px;">
                    <b>
                        <asp:Label ID="lblHeadings" runat="server" Text="Track User Route"></asp:Label></b>
                </div>
                <div class="MiddleContentDiv" style="padding-left: 19%;">
                    <asp:RadioButton ID="rbtnTeamWise" runat="server" Text="Team Wise" GroupName="TrackType"
                        AutoPostBack="True" OnCheckedChanged="rbtnTeamWise_CheckedChanged" Visible="False" />
                    <asp:RadioButton ID="rbtnSingleEmp" runat="server" Text="Single Employee" GroupName="TrackType"
                        AutoPostBack="True" OnCheckedChanged="rbtnSingleEmp_CheckedChanged" Checked="True" />
                </div>
                <div class="MiddleContentDiv">
                    <asp:Label CssClass="lblWidth" ID="lblTeam" runat="server" Text="Select Team*"></asp:Label><asp:DropDownList
                        ID="cmbName" runat="server" CssClass="combobox">
                    </asp:DropDownList>
                </div>
                <div class="EmpMasterConDiv">
                    &nbsp;</div>
                <div style="padding-top: 5px; text-align: center;">
                    <asp:Button ID="btnShowRoute" runat="server" Text="Show Route Traveled on Map" OnClick="btnShowRoute_Click" />
                </div>
                <div style="padding-top: 5px; text-align: center; width: 97%; height: 500px">
                    <div id="map_canvas" style="width: 100%; height: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
    </body>
</asp:Content>
