﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

public partial class CheckInOut : System.Web.UI.Page
{

    string date = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy");
    string time = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("HH:mm");
    int breakID;
    string breakStartTime = string.Empty;
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        selectdata();
    }
    void selectdata()
    {
        string qry = "Select * from " + Connection.tbl_BreakTimeName + " where Date='" + date + "' and UserID=" + Session["loggedInUserID"].ToString();
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);

        Connection.conn.Open();
        dt.Load(cmd.ExecuteReader());
        Connection.conn.Close();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["BreakEndTime"].ToString() == "")
            {
                txtDetail.Enabled = false;
                btnBreakStart.Enabled = false;
                btnBreakStop.Enabled = true;
                btnCancel.Enabled = false;
                txtDetail.Text = dt.Rows[i]["Detail"].ToString();
                breakID = int.Parse(dt.Rows[i]["BreakID"].ToString());
                breakStartTime = dt.Rows[i]["BreakStartTime"].ToString();
                break;
            }
            else
            {
                txtDetail.Enabled = true;
                btnBreakStart.Enabled = true;
                btnBreakStop.Enabled = false;
                btnCancel.Enabled = true;
                //txtDetail.Text = "";
            }
        }
    }
    protected void btnBreakStart_Click(object sender, EventArgs e)
    {
        try
        {
            string date = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy");
            string time = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("HH:mm");

            string qry = "INSERT INTO " + Connection.tbl_BreakTimeName + "(Date,UserID,BreakStartTime,Detail) VALUES ('" + date + "'," + Session["loggedInUserID"].ToString() + ",'" + time + "','" + txtDetail.Text.ToString() + "')";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            cmd.ExecuteNonQuery();
            Connection.conn.Close();
            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnBreakStop_Click(object sender, EventArgs e)
    {
        try
        {
            string date = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy");
            string time = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("HH:mm");

            string difference = DateTime.Parse(time).Subtract(DateTime.Parse(breakStartTime)).ToString("t");
            string spentTime1 = DateTime.Parse(difference).ToString("HH:mm");
            string spentTime = spentTime1.Replace(":", ".");


            string qry = "Update " + Connection.tbl_BreakTimeName + " set BreakEndTime='" + time + "',TotalBreakTime='" + spentTime + "' where BreakID=" + breakID;
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            cmd.ExecuteNonQuery();
            Connection.conn.Close();
            selectdata();
            Response.Redirect("CheckInOut.aspx");
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("CheckInOut.aspx");
    }
}