﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="ProjectMaster.aspx.cs" Inherits="ProjectMaster" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {

            $("#<%=dtpEndDate.ClientID %>").bind('change', function () {

                var strtime = $("input[type=text][id*=dtpStartDate]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=dtpEndDate]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=dtpEndDate]").val("");

                    //                    $("input[type=text][id*=dtpEndDate]").after(" <div style='margin-left: 46%'><span style='color:Red';>Date must be greater than Start Date</span> </div>");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                else {
                    return true;
                }
            })

            $("input[type=text][id*=txtaddPlanEnd]").change(function () {
                var strtime = $("input[type=text][id*=txtaddPlanStart1]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtaddPlanEnd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {
                    $("input[type=text][id*=txtaddPlanEnd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                return true;

            })

            $("input[type=text][id*=txtPlanEnd]").change(function () {
                var strtime = $("input[type=text][id*=txtPlanStart1]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtPlanEnd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=txtPlanEnd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                return true;

            })












            $("input[type=text][id*=txtaddplantime]").keypress(function (e) {


                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Plan Time Digits Only");
                    return false;
                }
                else { return true; }

            });


            $("input[type=text][id*=txtaddpercentageOfComplete]").keypress(function (e) {


                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Complete(%) Digits Only");
                    return false;
                }
                else { return true; }

            });


            $("input[type=text][id*=txtplantime]").keypress(function (e) {


                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Plan Time Digits Only");
                    return false;
                }
                else { return true; }

            });


            $("input[type=text][id*=txtpercentageOfComplete]").keypress(function (e) {


                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Complete(%) Digits Only");
                    return false;
                }
                else { return true; }

            });


        });
       
    </script>
    <script language="javascript" type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "minus.gif";
                }
                else {
                    img.src = "minus.gif";
                }
                img.alt = "Close to view other Customers";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "plus.gif";
                }
                else {
                    img.src = "plus.gif";
                }
                img.alt = "Expand to show Orders";
            }
        } 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[type=text][id*=txtPlanStart1]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtPlanEnd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtActulalStart]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtActualEnd]").datepicker({ dateFormat: 'dd/mm/yy' });


            $("input[type=text][id*=txtaddPlanStart1]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtaddPlanEnd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtaddActulalStart]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtaddActualEnd]").datepicker({ dateFormat: 'dd/mm/yy' });


            $("input[type=text][id*=txtsubPlanStart]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubPlanEnd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubActualStart]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubActualEnd]").datepicker({ dateFormat: 'dd/mm/yy' });


            $("input[type=text][id*=txtsubPlanStartadd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubPlanEndadd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubActualStartadd]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtsubActualEndadd]").datepicker({ dateFormat: 'dd/mm/yy' });

            $("input[type=text][id*=txtnewstartdate]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=txtnewenddate]").datepicker({ dateFormat: 'dd/mm/yy' });

            $("input[type=text][id*=dtpStartDate]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("input[type=text][id*=dtpEndDate]").datepicker({ dateFormat: 'dd/mm/yy' });
        });
       
    </script>
    <script type="text/javascript">
        $(function () {
            $("#btnnew").click(function () {

                $(function () {
                    $("#dialog").dialog();
                });


            });

            $("#btnsaveproject").click(function () {
                var flag1 = 0;
                var name = $("#<%=txtnewproject.ClientID %>").val();
                var startdate = $("#<%=txtnewstartdate.ClientID %>").val();
                var enddate = $("#<%=txtnewenddate.ClientID %>").val();
                var plantime = $("#<%=txtplantime1.ClientID %>").val();
                
                if ($("#<%=chkstatus1.ClientID %>").attr('checked') == true) {

                    flag1 = 1;
                }
                if (name != "") {
                    $.ajax({
                        type: "POST",
                        url: "ProjectMaster.aspx/saveproject?name=" + name + "&startdate=" + startdate + "&enddate=" + enddate + "&flag=" + flag1 + "&plantime=" + plantime,
                        //data: "{ID=" + JSON.stringify(ID) + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processData: false,
                        success: function (Result) {
                            var Result = Result.d;
                            alert(Result);
                            $("#dialog").dialog('close');
                            $("#<%=txtnewproject.ClientID %>").val("");
                            $("#<%=txtnewstartdate.ClientID %>").val("");
                            $("#<%=txtnewenddate.ClientID %>").val("");
                            location.reload();

                        },

                        error: function () {
                            alert("error");
                        }

                    });
                }
                else {
                    alert("Please Enter Project");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {



            $("table[id*=GridView2] input[id*=txtsubPlanEnd]").change(function () {
                var strtime = $("input[type=text][id*=txtsubPlanStart]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtsubPlanEnd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=txtsubPlanEnd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                return true;

            })

            $("table[id*=GridView2] input[id*=txtsubPlanEndadd]").change(function () {
                var strtime = $("input[type=text][id*=txtsubPlanStartadd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtsubPlanEndadd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=txtsubPlanEndadd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                    alert();
                }
                return true;

            })

            $("input[type=text][id*=txtsubActualEndadd]").change(function () {
                var strtime = $("input[type=text][id*=txtsubActualStartadd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtsubActualEndadd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=txtsubActualEndadd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                return true;

            })


            $("input[type=text][id*=txtsubActualEnd]").change(function () {
                var strtime = $("input[type=text][id*=txtsubActualStart]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                var endtime = $("input[type=text][id*=txtsubActualEnd]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");

                if (new Date(strtime) > new Date(endtime)) {

                    $("input[type=text][id*=txtsubActualEnd]").val("");
                    alert("Date must be greater than Start Date");

                    return false;
                }
                return true;

            })
        });
    
    </script>
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 15px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Project Master"></asp:Label></b>
            </div>
            <div>
                <div style="width: 75%; height: 160px; align-content: left">
                    <div style="float: left; width: 80%;">
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="Label1" runat="server" Text="Select Project"></asp:Label>
                            <asp:DropDownList Width="160px" ID="drpproject" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="drpproject_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="MiddleContentDiv" style="display: none;">
                            <asp:Label CssClass="lblWidth" ID="lblProjectName" runat="server" Text="Project Name*"></asp:Label>
                            <asp:TextBox ID="txtProjName" Width="150px" runat="server" MaxLength="150" required></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtProjName"
                                Display="Dynamic" ErrorMessage="Please Enter Project Name." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblStartDate" runat="server" Text="Start Date*"></asp:Label>
                            <asp:TextBox Width="150px" ID="dtpStartDate" runat="server"></asp:TextBox><%--<img src="Images/calender.png" />--%>
                            <div style="margin-left: 150px">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="dtpStartDate"
                                    Display="Dynamic" ErrorMessage="Select Start Date." ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                            <asp:TextBox ID="dtpEndDate" runat="server" Width="150px"></asp:TextBox><%--<img src="Images/calender.png" />--%>
                            <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="dtpStartDate"
                                ControlToValidate="dtpEndDate" Display="Dynamic" ErrorMessage="Date must be greater than Start Date"
                                ForeColor="Red" Operator="GreaterThan" Type="Date"></asp:CompareValidator>--%>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="Label7" runat="server" Text="(%)Of Complete"></asp:Label>
                            <asp:TextBox ID="txtpercentageofcom" runat="server" Width="150px"></asp:TextBox><%--<img src="Images/calender.png" />--%>
                           <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="dtpStartDate"
                                ControlToValidate="dtpEndDate" Display="Dynamic" ErrorMessage="Date must be greater than Start Date"
                                ForeColor="Red" Operator="GreaterThan" Type="Date"></asp:CompareValidator>--%>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="Label8" runat="server" Text="Plan Time"></asp:Label>
                            <asp:TextBox ID="txtplantime" runat="server" Width="150px"></asp:TextBox><%--<img src="Images/calender.png" />--%>
                           <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="dtpStartDate"
                                ControlToValidate="dtpEndDate" Display="Dynamic" ErrorMessage="Date must be greater than Start Date"
                                ForeColor="Red" Operator="GreaterThan" Type="Date"></asp:CompareValidator>--%>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblStatus" runat="server" Text="Status"></asp:Label><asp:CheckBox
                                ID="chkStatus" runat="server" Text="Active/Inactive" />
                        </div>
                    </div>
                    <div style="float: right; width: 100px;">
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblresorses" runat="server" Text="Select Employee"></asp:Label>
                            <%--<asp:DropDownList ID="drpresorses" runat="server" AutoPostBack="True">
                </asp:DropDownList>--%>
                            <asp:ListBox ID="drpresorses" runat="server" SelectionMode="Multiple"></asp:ListBox>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Button ID="btnassigen" runat="server" Text="Assign Task" OnClick="btnassigen_Click" />
                        </div>
                    </div>
                </div>
                <div style="padding-top: 5px; text-align: center;">
                    <input type="button" id="btnnew" value="New Project" />
                    <asp:Button ID="btnAdd" runat="server" Text="Update" OnClick="btnAdd_Click" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" ValidationGroup="UnvalidatedControl" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" ValidationGroup="UnvalidatedControl" />
                </div>
            </div>
            <div>
                <div style="padding-top: 5px; text-align: center;">
                </div>
            </div>
            <div>
                <asp:HiddenField ID="projectID" runat="server" />
            </div>
            <div style="text-align: center; overflow: scroll; width: 100%; height: auto;" class="EmpMasterConDiv">
                <asp:GridView ID="GridView1" AllowPaging="True" BackColor="#F1F1F1" AutoGenerateColumns="False"
                    DataKeyNames="TaskID" ShowFooter="True" Font-Size="12px" Font-Names="Verdana"
                    runat="server" GridLines="None" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand"
                    OnRowUpdating="GridView1_RowUpdating" BorderStyle="Outset" OnRowDeleting="GridView1_RowDeleting"
                    OnRowDeleted="GridView1_RowDeleted" OnRowUpdated="GridView1_RowUpdated" AllowSorting="True"
                    OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                    OnRowEditing="GridView1_RowEditing" EnableViewState="False">
                    <RowStyle BackColor="Gainsboro" />
                    <AlternatingRowStyle BackColor="White" />
                    <HeaderStyle BackColor="#0083C1" ForeColor="White" />
                    <FooterStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href="javascript:expandcollapse('div<%# Eval("TaskID") %>', 'one');">
                                    <img id="imgdiv<%# Eval("TaskID") %>" alt="Click to show/hide Orders for Customer <%# Eval("TaskID") %>"
                                        width="9px" border="0" src="plus.gif" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Task ID" SortExpression="TaskID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskID" Text='<%# Eval("TaskID") %>' runat="server" Style="display: none;"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblTaskID" Text='<%# Eval("TaskID") %>' runat="server" Style="display: none;"></asp:Label>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox ID="txtTaskID" Text='' runat="server"></asp:TextBox>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project ID" SortExpression="ProjID" Visible="false">
                            <ItemTemplate>
                                <%# Eval("ProjID")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtProjID" Text='<%# Eval("ProjID") %>' runat="server" Style="display: none;"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox ID="txtProjID" Text='' runat="server"></asp:TextBox>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Task Name" SortExpression="Name">
                            <ItemTemplate>
                                <%# Eval("Name")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="100px" ID="txtName" Text='<%# Eval("Name") %>' runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="100px" ID="txtaddName" runat="server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Task Details" SortExpression="TaskDetails">
                            <ItemTemplate>
                                <%# Eval("TaskDetails")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="100px" ID="txtTaskDetails" Text='<%# Eval("TaskDetails") %>'
                                    runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="100px" ID="txtaddTaskDetails" runat="server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plan Start" SortExpression="PlanStart">
                            <ItemTemplate>
                                <%# Eval("PlanStart")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="80px" ID="txtPlanStart1" Text='<%# Eval("PlanStart") %>' runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="80px" ID="txtaddPlanStart1" runat="server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plan End" SortExpression="PlanEnd">
                            <ItemTemplate>
                                <%# Eval("PlanEnd")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="80px" ID="txtPlanEnd" Text='<%# Eval("PlanEnd") %>' runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="80px" ID="txtaddPlanEnd" runat="server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actual Start" SortExpression="ActulalStart">
                            <ItemTemplate>
                                <%# Eval("ActulalStart")%></ItemTemplate>
                            <EditItemTemplate>
                                <%--<asp:TextBox Width="80px" ID="txtActulalStart" Text='<%# Eval("ActulalStart") %>' runat="server"></asp:TextBox>--%>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%-- <asp:TextBox Width="80px" ID="txtaddActulalStart" runat="server"></asp:TextBox>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actual End" SortExpression="ActualEnd">
                            <ItemTemplate>
                                <%# Eval("ActualEnd")%></ItemTemplate>
                            <EditItemTemplate>
                                <%--<asp:TextBox Width="80px" ID="txtActualEnd" Text='<%# Eval("ActualEnd") %>' runat="server"></asp:TextBox>--%>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox Width="80px" ID="txtaddActualEnd" runat="server"></asp:TextBox>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plan Time" SortExpression="ActualEnd">
                            <ItemTemplate>
                                <%# Eval("PlanTime")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="80px" ID="txtplantime" Text='<%# Eval("PlanTime") %>' runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="80px" ID="txtaddplantime" runat="server" Text="0"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actual Time" SortExpression="ActualEnd">
                            <ItemTemplate>
                                <%# Eval("ActualTime")%></ItemTemplate>
                            <EditItemTemplate>
                                <%--<asp:TextBox Width="80px" ID="txtactualtime" Text='<%# Eval("ActualTime") %>' runat="server"></asp:TextBox>--%>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox Width="80px" ID="txtaddactualtime" runat="server"></asp:TextBox>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complete(%)" SortExpression="ActualEnd">
                            <ItemTemplate>
                                <%# Eval("percentageOfComplete")%></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="80px" ID="txtpercentageOfComplete" Text='<%# Eval("percentageOfComplete") %>'
                                    runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="80px" ID="txtaddpercentageOfComplete" runat="server" Text="0"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <%--<ItemTemplate>
                                    <asp:LinkButton ID="linkDeleteCust" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                                </ItemTemplate>--%>
                            <FooterTemplate>
                                <asp:LinkButton ID="linkAddCust" CommandName="ADD" runat="server" ForeColor='#3399FF'>Add</asp:LinkButton>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:CommandField HeaderText="Edit" ShowEditButton="True" ItemStyle-BackColor="#3399FF" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <tr>
                                    <td colspan="100%">
                                        <div id="div<%# Eval("TaskID") %>" style="display: none; position: relative; left: 15px;
                                            overflow: auto; width: 97%">
                                            <asp:GridView ID="GridView2" DataKeyNames="TaskID" AllowPaging="True" EmptyDataText="No Sub task"
                                                ShowHeaderWhenEmpty="true" AllowSorting="true" ShowHeader="true" BackColor="White"
                                                Width="100%" Font-Size="smaller" AutoGenerateColumns="false" Font-Names="Verdana"
                                                runat="server" ShowFooter="true" OnPageIndexChanging="GridView2_PageIndexChanging"
                                                OnRowUpdating="GridView2_RowUpdating" OnRowCommand="GridView2_RowCommand" OnRowEditing="GridView2_RowEditing"
                                                GridLines="None" OnRowUpdated="GridView2_RowUpdated" OnRowCancelingEdit="GridView2_CancelingEdit"
                                                OnRowDataBound="GridView2_RowDataBound" OnRowDeleting="GridView2_RowDeleting"
                                                OnRowDeleted="GridView2_RowDeleted" OnSorting="GridView2_Sorting" BorderStyle="Double"
                                                BorderColor="#0083C1">
                                                <RowStyle BackColor="Gainsboro" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <HeaderStyle BackColor="#0083C1" ForeColor="White" />
                                                <FooterStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SubTaskID" SortExpression="SubTaskID" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSubTaskID12" Text='<%# Eval("SubTaskID") %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <%--<asp:Label ID="lblSubTaskID" Text='<%# Eval("SubTaskID") %>' runat="server"></asp:Label>--%>
                                                            <asp:HiddenField ID="hidenfield1" runat="server" Value='<%# Eval("SubTaskID") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SubTask Name" SortExpression="Name">
                                                        <ItemTemplate>
                                                            <%# Eval("Name")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="100px" ID="txtsubName12" Text='<%# Eval("Name")%>' runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="100px" ID="txtsubName" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SubTask Detail" SortExpression="SubTaskDetail">
                                                        <ItemTemplate>
                                                            <%# Eval("SubTaskDetail")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="100px" ID="txtSubTaskDetail12" Text='<%# Eval("SubTaskDetail")%>'
                                                                runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="100px" ID="txtSubTaskDetail" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Plan Start" SortExpression="PlanStart">
                                                        <ItemTemplate>
                                                            <%# Eval("PlanStart")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubPlanStart" Text='<%# Eval("PlanStart")%>' runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubPlanStartadd" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Plan End" SortExpression="PlanEnd">
                                                        <ItemTemplate>
                                                            <%# Eval("PlanEnd")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubPlanEnd" Text='<%# Eval("PlanEnd")%>' runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubPlanEndadd" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Actual Start" SortExpression="ActualStart">
                                                        <ItemTemplate>
                                                            <%# Eval("ActualStart")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubActualStart" Text='<%# Eval("ActualStart")%>'
                                                                runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubActualStartadd" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Actual End" SortExpression="ActualEnd" ControlStyle-CssClass="dateTextBox">
                                                        <ItemTemplate>
                                                            <%# Eval("ActualEnd")%></ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubActualEnd" Text='<%# Eval("ActualEnd")%>' runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox Width="70px" ID="txtsubActualEndadd" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <%--<ItemTemplate>
                                                                <asp:LinkButton ID="linkDeleteCust" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                                                            </ItemTemplate>--%>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="linkAddOrder" CommandName="AddSubTask" runat="server" Font-Bold='true'
                                                                ForeColor='#3399FF' formnovalidate>Add</asp:LinkButton>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Edit" ShowEditButton="True" ItemStyle-BackColor="#3399FF" />
                                                </Columns>
                                            </asp:GridView>
                                            <%-- <asp:SqlDataSource ID="GetSubtask" runat="server" ConnectionString="<%$ ConnectionStrings:STMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:STMSConnectionString.ProviderName %>" SelectCommand="SELECT SubTaskID, Name, SubTaskDetail, PlanStart, PlanEnd,ActualStart,ActualEnd FROM tbl_SubTask WHERE ProjID=1 and TaskID=1">
                                                </asp:SqlDataSource>--%>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <%--<asp:SqlDataSource ID="GetTask" runat="server" ConnectionString="<%$ ConnectionStrings:STMSConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:STMSConnectionString.ProviderName %>" SelectCommand="SELECT TaskID, ProjID, Name, TaskDetails, PlanStart, PlanEnd, ActulalStart, ActualEnd, percentageOfComplete FROM tbl_TaskMaster">
                    </asp:SqlDataSource>--%></div>
        </div>
        <div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="Label2" runat="server" Text="SelectSheet*"></asp:Label>

                <asp:DropDownList ID="cmbSelected" runat="server" CssClass="combobox" AutoPostBack="True" onselectedindexchanged="cmbSelected_SelectedIndexChanged" >
                   <asp:ListItem>Plan</asp:ListItem>  
                   <asp:ListItem>Actual</asp:ListItem>
                   <asp:ListItem>Both</asp:ListItem>               

                </asp:DropDownList>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblSelectDate" runat="server" Text="Select Date*"
                    Visible="False"></asp:Label>
                <asp:DropDownList ID="cmbMonth" runat="server" Visible="False">
                    <asp:ListItem Selected="True" Value="01">Jan</asp:ListItem>
                    <asp:ListItem Value="02">Feb</asp:ListItem>
                    <asp:ListItem Value="03">Mar</asp:ListItem>
                    <asp:ListItem Value="04">Apr</asp:ListItem>
                    <asp:ListItem Value="05">May</asp:ListItem>
                    <asp:ListItem Value="06">Jun</asp:ListItem>
                    <asp:ListItem Value="07">Jul</asp:ListItem>
                    <asp:ListItem Value="08">Aug</asp:ListItem>
                    <asp:ListItem Value="09">Sept</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dec</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cmbYear" runat="server" Visible="False">
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem>2016</asp:ListItem>
                    <asp:ListItem>2017</asp:ListItem>
                    <asp:ListItem>2018</asp:ListItem>
                    <asp:ListItem>2019</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
                &nbsp;</div>
            <div style="padding-top: 5px; text-align: center; display: none;">
                <asp:Button ID="btnPreview" runat="server" Text="Preview" OnClick="btnPreview_Click" />
            </div>
            <div class="EmpMasterConDiv">
                &nbsp;</div>
            <div style="overflow: scroll">
                <br />
                <asp:GridView ID="gv_TimesheetMonthly" HorizontalAlign="Center" runat="server" CellPadding="4"
                    ForeColor="#333333" ShowHeaderWhenEmpty="True" OnRowCreated="gv_TimesheetMonthly_RowCreated"
                    OnRowDataBound="gv_TimesheetMonthly_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <%--<Columns>
                        <asp:BoundField HeaderText="Task Name" DataField="Task Name" />
                        <asp:BoundField HeaderText="01" DataField="01"/>
                        <asp:BoundField HeaderText="02" DataField="02"/>
                        <asp:BoundField HeaderText="03" DataField="03"/>
                        <asp:BoundField HeaderText="04" DataField="04"/>
                        <asp:BoundField HeaderText="05" DataField="05"/>
                        <asp:BoundField HeaderText="06" DataField="06"/>
                        <asp:BoundField HeaderText="07" DataField="07"/>
                        <asp:BoundField HeaderText="08" DataField="08"/>
                        <asp:BoundField HeaderText="09" DataField="09"/>
                        <asp:BoundField HeaderText="10" DataField="10"/>
                        <asp:BoundField HeaderText="11" DataField="11" />
                        <asp:BoundField HeaderText="12" DataField="12"/>
                        <asp:BoundField HeaderText="13" DataField="13"/>
                        <asp:BoundField HeaderText="14" DataField="14"/>
                        <asp:BoundField HeaderText="15" DataField="15"/>
                        <asp:BoundField HeaderText="16" DataField="16"/>
                        <asp:BoundField HeaderText="17" DataField="17"/>
                        <asp:BoundField HeaderText="18" DataField="18"/>
                        <asp:BoundField HeaderText="19" DataField="19"/>
                        <asp:BoundField HeaderText="20" DataField="20"/>
                        <asp:BoundField HeaderText="21" DataField="21"/>
                        <asp:BoundField HeaderText="22" DataField="22"/>
                        <asp:BoundField HeaderText="23" DataField="23"/>
                        <asp:BoundField HeaderText="24" DataField="24"/>
                        <asp:BoundField HeaderText="25" DataField="25"/>
                        <asp:BoundField HeaderText="26" DataField="26"/>
                        <asp:BoundField HeaderText="27" DataField="27"/>
                        <asp:BoundField HeaderText="28" DataField="28"/>
                        <asp:BoundField HeaderText="29" DataField="29"/>
                        <asp:BoundField HeaderText="30" DataField="30"/>
                        <asp:BoundField HeaderText="31" DataField="31"/>
                    </Columns>--%>
                    <EditRowStyle BackColor="#999999" HorizontalAlign="Center" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div id="dialog" title="New Project" style="display: none;">
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label3" runat="server" Text="Project Name*"></asp:Label>
            <asp:TextBox ID="txtnewproject" Width="150px" runat="server" MaxLength="150" required="Plz Enter Project"
                placeholder="Project Name"></asp:TextBox>
        </div>
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label4" runat="server" Text="Start Date*"></asp:Label>
            <asp:TextBox ID="txtnewstartdate" Width="150px" runat="server" MaxLength="150" required></asp:TextBox>
        </div>
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label5" runat="server" Text="End Date*"></asp:Label>
            <asp:TextBox ID="txtnewenddate" Width="150px" runat="server" MaxLength="150"></asp:TextBox>
        </div>
         <div class="MiddleContentDiv">
          <asp:Label CssClass="lblWidth" ID="Label9" runat="server" Text="Plan Time*"></asp:Label>
          <asp:TextBox ID="txtplantime1" Width="150px" runat="server" MaxLength="150" ></asp:TextBox>                           
        </div>
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label6" runat="server" Text="Status">
            </asp:Label>
            <br />
            <asp:CheckBox ID="chkstatus1" runat="server" Text="Active/Inactive" />
        </div>
        <%-- <asp:Button ID="btnsave" runat="server" text="Save" onclick="btnsave_Click"/>--%>
        <input type="button" id="btnsaveproject" value="save" />
    </div>
</asp:Content>
