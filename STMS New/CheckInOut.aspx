﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="CheckInOut.aspx.cs" Inherits="CheckInOut" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script src="”http://code.jquery.com/jquery-1.9.1.js&#8221;" type="”text/javascript”"></script>
<script src="”http://code.jquery.com/ui/1.10.2/jquery-ui.js&#8221;" type="”text/javascript”"></script>
    --%>
    <script type="text/javascript">
        $(function () {
            $("input[type=text][id*=dtpDate]").datepicker({ dateFormat: 'dd/mm/yy' });
        });

    </script>
    <script type="text/javascript">
        $(function () {
            $("#<%=dtpDate.ClientID %>").bind('change', function () {
                var fullDate = new Date()
                var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

                var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();
                var UserDate = $("input[type=text][id*=dtpDate]").val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
                if (new Date(UserDate) >= new Date(currentDate)) {
                    var CheckDate = fullDate.getDate() - 1 + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
                    $("input[type=text][id*=dtpDate]").val(CheckDate);
                    alert("Date must be less than Today Date");
                    return false;
                }
                else {
                    return true;
                }
            })
        });
    </script>
    <script type="text/javascript">
        function cleartext() {
            $("#<%=txtprojectname.ClientID %>").val("");
            $("#<%=txttaskname.ClientID %>").val("");
            $("#<%=txtcheckin.ClientID %>").val("");

            $("#<%=lblCheckInOutID.ClientID %>").text("");
            $("#<%=lbldate.ClientID %>").text("");
            $("#<%=lblPid.ClientID %>").text("");
            $("#<%=lbltid.ClientID %>").text("");
        }


        function getcall() {
            cleartext();
            $.ajax({
                type: "POST",
                url: "CheckInOut.aspx/getdata",
                //data: "{ID=" + JSON.stringify(ID) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                processData: false,
                success: function (Result) {
                    var Result = Result.d;
                    var CheckInOutID = Result[0].CheckInOutID;
                    var Projectname = Result[0].Projectname;
                    var Taskname = Result[0].Taskname;
                    var Date = Result[0].Date;
                    var CheckInTime = Result[0].CheckInTime;
                    var pid = Result[0].Pid;
                    var tid = Result[0].tid;
                    $("#<%=lbldate.ClientID %>").text(Date);
                    $("#<%=lblCheckInOutID.ClientID %>").text(CheckInOutID);
                    $("#<%=txtprojectname.ClientID %>").val(Projectname);
                    $("#<%=txttaskname.ClientID %>").val(Taskname);
                    $("#<%=txtcheckin.ClientID %>").val(CheckInTime);
                    $("#<%=lblPid.ClientID %>").text(pid);
                    $("#<%=lbltid.ClientID %>").text(tid);

                    $("#dialog2").dialog();

                    //location.reload();

                },

                error: function () {
                    alert("error");
                }

            });

        }

        $(function () {

            $(function () {
                cleartext();
                $.ajax({
                    type: "POST",
                    url: "CheckInOut.aspx/getdata",
                    //data: "{ID=" + JSON.stringify(ID) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: false,
                    success: function (Result) {
                        var Result = Result.d;
                        var CheckInOutID = Result[0].CheckInOutID;
                        var Projectname = Result[0].Projectname;
                        var Taskname = Result[0].Taskname;
                        var Date = Result[0].Date;
                        var CheckInTime = Result[0].CheckInTime;
                        var pid = Result[0].Pid;
                        var tid = Result[0].tid;

                        $("#<%=lbldate.ClientID %>").text(Date);
                        $("#<%=lblCheckInOutID.ClientID %>").text(CheckInOutID);
                        $("#<%=txtprojectname.ClientID %>").val(Projectname);
                        $("#<%=txttaskname.ClientID %>").val(Taskname);
                        $("#<%=txtcheckin.ClientID %>").val(CheckInTime);

                        $("#<%=lblPid.ClientID %>").text(pid);
                        $("#<%=lbltid.ClientID %>").text(tid);

                        $("#dialog2").dialog();

                        //location.reload();

                    },

                    error: function () {
                        alert("error");
                    }

                });


            });




            $("#btncheckout").click(function () {
                var flag1 = 0;
                var id = $("#<%=lblCheckInOutID.ClientID %>").text();
                var a1 = $("#<%=drphrs.ClientID  %>").val();
                var a2 = $("#<%=drpmin.ClientID  %>").val();
                var checkout = a1 + ":" + a2;
                var checkin = $("#<%=txtcheckin.ClientID %>").val();
                var pid = $("#<%=lblPid.ClientID %>").text();
                var tid = $("#<%=lbltid.ClientID %>").text();
                var date = $("#<%=lbldate.ClientID %>").text();
               
                if (checkout != "" && checkin != "" && id != "" && pid !="" && tid !="" && date!="") {
                    $.ajax({
                        type: "POST",
                        url: "CheckInOut.aspx/savecheckout?CheckInOutID=" + id + "&checkout=" + checkout + "&checkin=" + checkin + "&pid=" + pid + "&tid=" + tid + "&date=" + date,
                        //data: "{ID=" + JSON.stringify(ID) + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processData: false,
                        success: function (Result) {
                            var Result = Result.d;
                            alert(Result);
                            $("#dialog2").dialog('close');
                            $("#<%=txtprojectname.ClientID %>").val("");
                            $("#<%=txttaskname.ClientID %>").val("");
                            $("#<%=txtcheckin.ClientID %>").val("");

                            $("#<%=lblCheckInOutID.ClientID %>").text("");
                            $("#<%=lblPid.ClientID %>").text("");
                            $("#<%=lbltid.ClientID %>").text("");
                            $("#<%=lbldate.ClientID %>").text("");
                            //location.reload();

                        },

                        error: function () {
                            alert("error");
                        }

                    });
                }
                else {
                    alert("Please Enter CheckOutTime");
                }
            });
        });
    </script>

    <%-- <script type="text/javascript">
        $(function () {
            $("#<%=btnChkIn.ClientID %>").click(function () {

                $(".se-pre-con").fadeIn("slow", function () { $(".se-pre-con").fadeOut("50000") });
            });

        });
    </script>--%>
    <%--   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div style="padding-top: 1%; margin-left: 40%; height: 21px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Check In-Out"></asp:Label></b>
                <b><a href="IssueTracker.aspx" style="margin-left: 27%;">
                    <asp:Label ID="lblIssue" runat="server" ForeColor="#FF3300"></asp:Label></a></b>
            </div>
            <div class="MiddleContentDiv" style="padding-left: 13%;">
                <asp:RadioButton ID="rbtnCurrentWorking" runat="server" Text="Current Working" GroupName="FillType"
                    Checked="True" AutoPostBack="True" OnCheckedChanged="rbtnCurrentWorking_CheckedChanged" />
                <asp:RadioButton ID="rbtnFillPrevious" runat="server" Text="Fill Previous" GroupName="FillType"
                    AutoPostBack="True" OnCheckedChanged="rbtnFillPrevious_CheckedChanged" />
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblProjName" runat="server" Text="Project Name*"></asp:Label><asp:DropDownList
                    ID="cmbProjName" runat="server" CssClass="combobox" OnSelectedIndexChanged="cmbProjName_SelectedIndexChanged"
                    AutoPostBack="True">
                    <asp:ListItem>-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbProjName"
                    Display="Dynamic" ErrorMessage="Please Select Project." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblTaskName" runat="server" Text="Task Name*"></asp:Label><asp:DropDownList
                    ID="cmbTaskName" runat="server" CssClass="combobox" OnSelectedIndexChanged="cmbTaskName_SelectedIndexChanged">
                    <asp:ListItem>-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="cmbTaskName"
                    Display="Dynamic" ErrorMessage="Please Select Task." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv" style="padding-left: 13%; display: none">
                <asp:RadioButton ID="rbtnIndoor" runat="server" Text="Indoor" GroupName="CheckInOutType"
                    Checked="True" OnCheckedChanged="rbtnIndoor_CheckedChanged" />
                <asp:RadioButton ID="rbtnOutdoor" runat="server" Text="Outdoor" GroupName="CheckInOutType"
                    OnCheckedChanged="rbtnOutdoor_CheckedChanged" />
            </div>
            <div class="MiddleContentDiv" style="display: none;">
                <asp:Label CssClass="lblWidth" ID="lblpercentComplete" runat="server" Text="percentageOfComplete*"></asp:Label>
                <asp:TextBox ID="txt_NoOfStrok" runat="server" Text="0" OnTextChanged="txt_NoOfStrok_TextChanged"></asp:TextBox>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblSelectDate" runat="server" Text="Select Date*"></asp:Label>
                <asp:TextBox ID="dtpDate" runat="server" Enabled="False" /><%--</asp:TextBox><asp:Image    ID="Image1" runat="server" Visible="false"  ImageUrl="~/images/calender.png"/>--%><%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbOutHrs"
                    Display="Dynamic" ErrorMessage="Please Select outtime Hrs. " ForeColor="Red" 
                    InitialValue="- -" ValidationGroup="Group1"></asp:RequiredFieldValidator>--%>
                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="dtpDate"
                    Display="Dynamic" ErrorMessage="Please Enter Date" ForeColor="Red" ValidationGroup="Group1"></asp:RequiredFieldValidator>--%></div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" Width="147px" ID="lblInTime" runat="server" Text="InTime"
                    Enabled="False"></asp:Label>
                <asp:DropDownList ID="cmbInHrs" runat="server" Width="50px" ValidationGroup="Group1"
                    Enabled="False">
                </asp:DropDownList>
                <asp:Label ID="lblFromHrs" runat="server" Text="Hrs." Enabled="False" Visible="False"></asp:Label>
                <asp:DropDownList ID="cmbInMin" runat="server" Width="50px" ValidationGroup="Group1"
                    Enabled="False">
                </asp:DropDownList>
                <asp:Label ID="lblFromMin" runat="server" Text="Min." Enabled="False"></asp:Label>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cmbInHrs"
                    Display="Dynamic" ErrorMessage="Please Select Intime Hrs." ForeColor="Red" 
                    InitialValue="- -" ValidationGroup="Group1"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cmbInMin"
                    Display="Dynamic" ErrorMessage="Select Intime Min." ForeColor="Red" 
                    InitialValue="- -" ValidationGroup="Group1"></asp:RequiredFieldValidator>--%>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" Width="147px" ID="lblOutTime" runat="server" Text="Out Time"
                    Enabled="False"></asp:Label>
                <asp:DropDownList ID="cmbOutHrs" runat="server" Width="50px" ValidationGroup="Group1"
                    Enabled="False">
                </asp:DropDownList>
                <asp:Label ID="lblToHrs" runat="server" Text="Hrs." Enabled="False"></asp:Label>
                <asp:DropDownList ID="cmbOutMin" runat="server" Width="50px" ValidationGroup="Group1"
                    Enabled="False">
                </asp:DropDownList>
                <asp:Label ID="lblToMin" runat="server" Text="Min." Enabled="False" Visible="False"></asp:Label>
                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbOutHrs"
                    Display="Dynamic" ErrorMessage="Please Select outtime Hrs. " ForeColor="Red" 
                    InitialValue="- -" ValidationGroup="Group1"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="cmbOutMin"
                    Display="Dynamic" ErrorMessage="Select OutTime Min." ForeColor="Red" 
                    InitialValue="- -" ValidationGroup="Group1"></asp:RequiredFieldValidator>--%>
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ForeColor="Red"
                    ErrorMessage="Outtime Should be greater than Intime." ControlToCompare="cmbInHrs"
                    ControlToValidate="cmbOutHrs" Operator="GreaterThanEqual" Enabled="False"></asp:CompareValidator>
            </div>
        </div>
        <div style="padding-top: 2px; text-align: center;">
            <asp:Button ID="btnChkIn" runat="server" Text="Check In" OnClick="btnChkIn_Click" />
            <asp:Button ID="btnChangeTask" runat="server" Text="Change Task" Enabled="False"
                OnClick="btnChangeTask_Click" />
            <asp:Button ID="btnChkOut" runat="server" Text="Check Out" OnClick="btnChkOut_Click"
                CausesValidation="False" />
            <asp:Button ID="btnTakeBreak" runat="server" Text="Take Break" OnClick="btnTakeBreak_Click"
                Visible="False" />
            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"
                Enabled="False" Visible="False" />
        </div>
        <div style="overflow: scroll">
            <br />
            <asp:GridView ID="gv_CheckInOut" HorizontalAlign="Center" runat="server" CellPadding="4"
                ForeColor="#333333" GridLines="Horizontal" ShowHeaderWhenEmpty="True" OnRowCreated="gv_CheckInOut_RowCreated"
                OnRowDataBound="gv_CheckInOut_RowDataBound" ShowFooter="True">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
    </div>
    <div style="clear: both;">
    </div>

    <div id="dialog2" title="Please CheckOut" style="display: none;">
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="lblPid" runat="server" style="display:none;"></asp:Label>
            <asp:Label CssClass="lblWidth" ID="lbltid" runat="server" style="display:none;"></asp:Label>
            <asp:Label CssClass="lblWidth" ID="lblCheckInOutID" runat="server" style="display:none;"></asp:Label>
            Date:<asp:Label CssClass="lblWidth" ID="lbldate" runat="server"></asp:Label>
            <asp:Label CssClass="lblWidth" ID="Label3" runat="server" Text="Project Name*"></asp:Label>
            <asp:TextBox ID="txtprojectname" Width="150px" runat="server" MaxLength="150" required="Plz Enter Project"
                placeholder="Project Name" ReadOnly></asp:TextBox>
        </div>
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label4" runat="server" Text="Task Name*"></asp:Label>
            <asp:TextBox ID="txttaskname" Width="150px" runat="server" MaxLength="150" required ReadOnly></asp:TextBox>
        </div>
        <div class="MiddleContentDiv">
            <asp:Label CssClass="lblWidth" ID="Label5" runat="server" Text="CheckInTime*"></asp:Label>
            <asp:TextBox ID="txtcheckin" Width="150px" runat="server" MaxLength="150" ReadOnly></asp:TextBox>
        </div>
        <%-- <div class="MiddleContentDiv">
          <asp:Label CssClass="lblWidth" ID="Label9" runat="server" Text="CheckOutTime*"></asp:Label>
          <asp:TextBox ID="txtcheckout" Width="150px" runat="server" MaxLength="150" ></asp:TextBox>                           
        </div>--%>
        <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" Width="147px" ID="Label1" runat="server" Text="Out Time"
                    Enabled="False"></asp:Label>
                <asp:DropDownList ID="drphrs" runat="server" Width="50px" >
                </asp:DropDownList>
                <asp:Label ID="Label2" runat="server" Text="Hrs."></asp:Label>
                <asp:DropDownList ID="drpmin" runat="server" Width="50px" >
                </asp:DropDownList>
                <asp:Label ID="Label6" runat="server" Text="Min."></asp:Label>               
            </div>
        <input type="button" id="btncheckout" value="CheckOut" />
    </div>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    <%-- <div class="se-pre-con">
    </div>--%>
</asp:Content>
