﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Collections;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            getproject();
            loadgrid();
            loadchart();
        }
    }
    public void getproject() //rohit load project
    {
        // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
        string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.StartDate,tbl_ProjectMaster.EndDate,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_ProjectMaster";
        //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        //Connection.conn.Close();
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();

        // cmd.ExecuteReader();

        MySqlDataReader reader = cmd.ExecuteReader();
        //projectList = new List<Tuple<string, string, string>>();
        //while (reader.Read())
        //{
        //    projectList.Add(new Tuple<string, string, string>(reader["Project Name"] as string, reader["StartDate"] as string, reader["EndDate"] as string));
        //}
        reader.Close();
        cmbEmpName.DataSource = cmd.ExecuteReader();
        cmbEmpName.DataTextField = "Project Name";
        cmbEmpName.DataValueField = "ProjID";
        cmbEmpName.DataBind();

        //drpproject.Items.Insert(0, "-- Select --");
        Connection.conn.Close();
    }
    //public void loadstackedchart()
    //{
    //    DataSet ds = new DataSet();
    //    int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
    //    MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select * from tbl_TaskMaster where ProjID=" + id, Connection.conn);
    //    Connection.conn.Open();
    //    dataAdapter.Fill(ds);
    //    Connection.conn.Close();
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //        {
    //            Chart1.Series["PlanStart"].Points.Add(new DataPoint(i, ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));
    //            //Chart1.Series["PlanEnd"].Points.Add(new DataPoint(i, ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));
    //            //Chart1.Series["ActulalStart"].Points.Add(new DataPoint(i, ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));
    //            //Chart1.Series["ActualEnd"].Points.Add(new DataPoint(i, ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));

    //            //Chart1.Series[0].Points[i].AxisLabel = ds.Tables[0].Rows[i]["Name"].ToString().Trim();
    //        }
    //    }
    //}
    public void loadgrid()
    {
        DataSet ds = new DataSet();
        int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
        MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select Name,ActualTime as 'Spent Time(HH:MM)' from tbl_TaskMaster where ProjID=" + id, Connection.conn);
        Connection.conn.Open();
        dataAdapter.Fill(ds);
        Connection.conn.Close();
        piechartgrid.DataSource = ds;
        piechartgrid.DataBind();
    }
    public void loadchart()
    {
        DataSet ds = new DataSet();
        int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
        MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select * from tbl_TaskMaster where ProjID=" + id, Connection.conn);
        Connection.conn.Open();
        dataAdapter.Fill(ds);
        Connection.conn.Close();
        ArrayList yValues = new ArrayList();
        ArrayList xValues = new ArrayList();
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
               // Chart1.Series["PlanStart"].Points.Add(new DataPoint(i, ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));
                yValues.Add( Convert.ToDouble(ds.Tables[0].Rows[i]["ActualTime"].ToString().Trim()));

                xValues.Add( ds.Tables[0].Rows[i]["Name"].ToString().Trim());
            }
        }
       
        Chart1.Series["Default"].Points.DataBindXY(xValues, yValues);

        //Chart1.Series["Default"].Points[0].Color = Color.MediumSeaGreen;
        //Chart1.Series["Default"].Points[1].Color = Color.PaleGreen;
        //Chart1.Series["Default"].Points[2].Color = Color.LawnGreen;

        Chart1.Series["Default"].ChartType = SeriesChartType.Pie;

        Chart1.Series["Default"]["PieLabelStyle"] = "Disabled";

        Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

        Chart1.Legends[0].Enabled = true;
    }
    protected void btnPreview_Click(object sender, EventArgs e)
    {
        //loadstackedchart();
        loadgrid();
        loadchart();
    }
    protected void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadgrid();
        loadchart();
    }
}