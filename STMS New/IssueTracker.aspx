﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="IssueTracker.aspx.cs" Inherits="IssueTracker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 15px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Issue Tracker"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv" style="margin-left: 300px">
                <asp:RadioButton Text="Incomming Issue" Checked="true" GroupName="Issue" runat="server"
                    ID="rdbtnIncommimg" OnCheckedChanged="rdbtnIncommimg_CheckedChanged" AutoPostBack="true" />
                <asp:RadioButton Text="My Issue" runat="server" ID="rdbtnMyIssue" OnCheckedChanged="rdbtnMyIssue_CheckedChanged"
                    AutoPostBack="true" GroupName="Issue" />
            </div>
            <asp:Panel runat="server" ID="panelMyIssue" Visible="false">
                <div style="width: 75%; height: 166px; padding-top: 15px; align-content: left">
                    <div style="float: left; width: 76%;">
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="Label1" runat="server" Text="Select Project"></asp:Label>
                            <asp:DropDownList CssClass="textbox" ID="drpproject" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblIssueName" runat="server" Text="Issue Name*"></asp:Label>
                            <asp:TextBox ID="txtIssueName" runat="server"></asp:TextBox>
                            <div style="margin-left: 150px">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtIssueName"
                                    Display="Dynamic" ErrorMessage="Please Fill Issue Name." ValidationGroup="s"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="lblPriority" runat="server" Text="Priority"></asp:Label>
                            <asp:DropDownList CssClass="textbox" ID="ddlPriority" runat="server">
                                <asp:ListItem Text="Low" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Medium" Value="1"></asp:ListItem>
                                <asp:ListItem Text="High" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="MiddleContentDiv">
                            <asp:Label CssClass="lblWidth" ID="Label2" runat="server" Text="Comment"></asp:Label>
                            <asp:TextBox ID="txtComment" TextMode="MultiLine" Width="150px" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div style="float: right; width: 108px; margin-top: 0px">
                        <asp:Label CssClass="lblWidth" ID="lblresorses" runat="server" Text="Issue To"></asp:Label>
                        <%--<asp:DropDownList ID="drpresorses" runat="server" AutoPostBack="True">
                </asp:DropDownList>--%>
                        <asp:ListBox ID="drpresorses" runat="server" SelectionMode="Multiple" Height="127px"
                            Width="127px"></asp:ListBox>
                        <asp:RequiredFieldValidator ValidationGroup="s" ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="drpresorses" Display="Dynamic" ErrorMessage="Please Select Issue To ."
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="padding-top: 5px; margin-left: 266px">
                    <asp:Button ID="btnRaiseIssue" CssClass="btn" ValidationGroup="s" Width="100px" runat="server"
                        Text="Raise Issue" OnClick="btnRaiseIssue_Click" />
                    <asp:Button ID="btnClear" CssClass="btn" Width="100px" runat="server" Text="Clear"
                        OnClick="btnClear_Click" />
                </div>
                <div style="text-align: center; overflow: scroll; width: 100%; height: auto;">
                    <asp:GridView ID="gvmyIssue" DataKeyNames="IssueID" AllowPaging="True" EmptyDataText="No My Issue"
                        ShowHeaderWhenEmpty="true" ShowHeader="true" BackColor="White" Width="100%" Font-Size="11px"
                        AutoGenerateColumns="false" Font-Names="Verdana" runat="server" ShowFooter="true"
                        BorderStyle="Double" BorderColor="#0083C1" OnRowEditing="gvmyIssue_RowEditing"
                        OnRowUpdating="gvmyIssue_RowUpdating" OnRowCommand="gvmyIssue_RowCommand" OnRowDataBound="gvmyIssue_RowDataBound"
                        OnSelectedIndexChanged="gvmyIssue_SelectedIndexChanged">
                        <RowStyle BackColor="Gainsboro" />
                        <AlternatingRowStyle BackColor="White" />
                        <HeaderStyle BackColor="#0083C1" ForeColor="White" />
                        <FooterStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" CausesValidation="false" Text="Mark As Resolve"
                                        CommandArgument='<%# Eval("IssueID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Issue ID" SortExpression="SubTaskID" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblIssueID" Text='<%# Eval("IssueID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hidenfield1" runat="server" Value='<%# Eval("IssueID") %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Issue Name" SortExpression="IssueName">
                                <ItemTemplate>
                                    <asp:Label ID="lblIssueName" Text='<%# Eval("IssueName")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="100px" ID="txtIssueName" Text='<%# Eval("IssueName")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                                <ItemTemplate>
                                    <asp:Label ID="lblIPriority" Text='<%# Eval("Priority")%>' runat="server"></asp:Label></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="100px" ID="txtPriority" Text='<%# Eval("Priority")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Date" SortExpression="OpenDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblIOpenDate" Text='<%# Eval("OpenDate")%>' runat="server"></asp:Label></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtOpenDate" Text='<%# Eval("OpenDate")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Close Date" SortExpression="CloseDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblCloseDate" Text='<%# Eval("CloseDate")%>' runat="server"></asp:Label></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtCloseDate" Text='<%# Eval("CloseDate")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project Name" SortExpression="Project Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" Text='<%# Eval("Name")%>' runat="server"></asp:Label></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtProjectID" Text='<%# Eval("Name")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment" SortExpression="Comment" ControlStyle-CssClass="dateTextBox">
                                <ItemTemplate>
                                    <asp:Label ID="lblComment" Text='<%# Eval("Comment")%>' runat="server"></asp:Label></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtComment" Text='<%# Eval("Comment")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:CommandField HeaderText="Edit" ShowEditButton="True" ItemStyle-BackColor="#3399FF" />--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="IncommingIssue">
                <div style="text-align: center; overflow: scroll; width: 100%; height: auto;" class="EmpMasterConDiv">
                    <asp:GridView ID="gvIncommingIssue" DataKeyNames="IssueID" AllowPaging="True" EmptyDataText="No Incomming Issue"
                        ShowHeaderWhenEmpty="true" ShowHeader="true" BackColor="White" Width="100%" Font-Size="11px"
                        AutoGenerateColumns="false" Font-Names="Verdana" runat="server" ShowFooter="true"
                        BorderStyle="Double" BorderColor="#0083C1" OnRowEditing="gvmyIssue_RowEditing"
                        OnRowUpdating="gvmyIssue_RowUpdating">
                        <RowStyle BackColor="Gainsboro" />
                        <AlternatingRowStyle BackColor="White" />
                        <HeaderStyle BackColor="#0083C1" ForeColor="White" />
                        <FooterStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Issue ID" SortExpression="SubTaskID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIssueID" Text='<%# Eval("IssueID") %>' Width="5px" runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hidenfield1" runat="server" Value='<%# Eval("IssueID") %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Issue Name" SortExpression="IssueName">
                                <ItemTemplate>
                                    <%# Eval("IssueName")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="100px" ID="txtIssueName" Text='<%# Eval("IssueName")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                                <ItemTemplate>
                                    <%# Eval("Priority")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="100px" ID="txtPriority" Text='<%# Eval("Priority")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Date" SortExpression="OpenDate">
                                <ItemTemplate>
                                    <%# Eval("OpenDate")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtOpenDate" Text='<%# Eval("OpenDate")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Close Date" SortExpression="CloseDate">
                                <ItemTemplate>
                                    <%# Eval("CloseDate")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtCloseDate" Text='<%# Eval("CloseDate")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project ID" SortExpression="Project Name">
                                <ItemTemplate>
                                    <%# Eval("Name")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtProjectID" Text='<%# Eval("Name")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment" SortExpression="Comment" ControlStyle-CssClass="dateTextBox">
                                <ItemTemplate>
                                    <%# Eval("Comment")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtComment" Text='<%# Eval("Comment")%>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Issue By" SortExpression="EmployeeName">
                                <ItemTemplate>
                                    <%# Eval("EmployeeName")%></ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="70px" ID="txtEmployeeName" Text='<%# Eval("EmployeeName ")%>'
                                        runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" Text="Replay" CommandArgument='<%# Eval("IssueID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
