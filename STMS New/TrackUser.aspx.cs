﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;

public partial class TrackUser : System.Web.UI.Page
{
    static int selectedID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void btnShowUserLocation_Click(object sender, EventArgs e)
    {
        try
        {
            string qry = "";
            if (rbtnAll.Checked == true)
            {
                qry = "Select tbl_UserMaster.Name," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLatitude," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLongitude From tbl_UserMaster," + Connection.tbl_ChkInOutDetailsName + " Where " + Connection.tbl_ChkInOutDetailsName + ".UserID=tbl_UserMaster.UserID and CheckOutTime is null And InddorOutdoor=1";
            }
            if (rbtnTeamWise.Checked == true)
            {
                selectedID = int.Parse(cmbName.SelectedValue.ToString());
                qry = "Select tbl_UserMaster.Name," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLatitude," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLongitude From tbl_UserMaster," + Connection.tbl_ChkInOutDetailsName + ",tbl_TeamMaster Where " + Connection.tbl_ChkInOutDetailsName + ".UserID=tbl_UserMaster.UserID and CheckOutTime is null And InddorOutdoor=1 and tbl_TeamMaster.TeamID=" + selectedID;
            }
            if (rbtnSingleEmp.Checked == true)
            {
                selectedID = int.Parse(cmbName.SelectedValue.ToString());
                qry = "Select tbl_UserMaster.Name," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLatitude," + Connection.tbl_ChkInOutDetailsName + ".LastVisitedLocationLongitude From tbl_UserMaster," + Connection.tbl_ChkInOutDetailsName + " Where " + Connection.tbl_ChkInOutDetailsName + ".UserID=tbl_UserMaster.UserID and CheckOutTime is null And InddorOutdoor=1 And " + Connection.tbl_ChkInOutDetailsName + ".UserID=" + selectedID;
            }
            //DataTable dt = new DataTable();
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            DataTable dbData = new DataTable();
            dbData.Load(cmd.ExecuteReader());

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("UserName"));
            dt.Columns.Add(new DataColumn("Latitude"));
            dt.Columns.Add(new DataColumn("Longitude"));
            DataRow row = dt.NewRow();
            for (int i = 0; i < dbData.Rows.Count; i++)
            {
                row = dt.NewRow();
                row["UserName"] = dbData.Rows[i]["Name"];
                row["Latitude"] = dbData.Rows[i]["LastVisitedLocationLatitude"];
                row["Longitude"] = dbData.Rows[i]["LastVisitedLocationLongitude"];
                dt.Rows.Add(row);
            }
            Connection.conn.Close();

            rptMarkers.DataSource = dt;
            rptMarkers.DataBind();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void rbtnAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbtnAll.Checked == true)
            {
                cmbName.Items.Clear();
                cmbName.Enabled = false;
            }
            if (rbtnSingleEmp.Checked == true)
            {
                cmbName.Enabled = true;
                lblTeam.Text = "Select Employee *";

                string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                cmbName.Items.Clear();
                cmbName.DataSource = cmd.ExecuteReader();
                cmbName.DataTextField = "Name";
                cmbName.DataValueField = "UserID";
                cmbName.DataBind();
                Connection.conn.Close();
            }
            if (rbtnTeamWise.Checked == true)
            {
                cmbName.Enabled = true;
                lblTeam.Text = "Select Team *";

                string qry = "SELECT TeamID,Name FROM tbl_TeamMaster;";

                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                cmbName.Items.Clear();
                cmbName.DataSource = cmd.ExecuteReader();
                cmbName.DataTextField = "Name";
                cmbName.DataValueField = "TeamID";
                cmbName.DataBind();
                Connection.conn.Close();
            }
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void rbtnTeamWise_CheckedChanged(object sender, EventArgs e)
    {
        rbtnAll_CheckedChanged(sender, e);
    }
    protected void rbtnSingleEmp_CheckedChanged(object sender, EventArgs e)
    {
        rbtnAll_CheckedChanged(sender, e);

        //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_UserMaster", Connection.conn);
        //Connection.conn.Open();
        //MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
        //cmbName.Items.Clear();

        //while (dr.Read())
        //{
        //    cmbName.Items.Add(dr[2].ToString());
        //}
        //Connection.conn.Close();

    }
    protected void cmbName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}