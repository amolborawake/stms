﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MonthlyReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //set as current month &year as selected 23/03/2016
            cmbYear.Items.FindByValue(System.DateTime.Now.Year.ToString()).Selected = true;  
            string mnth = System.DateTime.Now.Month.ToString();
            if (mnth.Length == 1)
                mnth = "0" + mnth;
            cmbMonth.ClearSelection();
            cmbMonth.Items.FindByValue(mnth).Selected = true;
           
     

    }
    protected void btnBrowse_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtOutputPath.Text != "")
            {
                string filePath = "@" + txtOutputPath.Text.ToString();
                System.Diagnostics.Process.Start(txtOutputPath.Text);
            }
        }
        catch
        {

        }
    }
}