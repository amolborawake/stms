﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true" CodeFile="TrackUser.aspx.cs" Inherits="TrackUser" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:Literal ID="js" runat="server"></asp:Literal>
  <body>
  <script type="text/javascript">
 
    var markers = [
    <asp:Repeater ID="rptMarkers" runat="server">
     <ItemTemplate>
             {
                "title": '<%# Eval("UserName") %>',
                "lat": '<%# Eval("Latitude") %>',
                "lng": '<%# Eval("Longitude") %>'
                
            }
    </ItemTemplate>
    <SeparatorTemplate>
        ,
    </SeparatorTemplate>
    </asp:Repeater>
    ];
  </script>
    <script type="text/javascript">

        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(18.5107889, 73.7903456),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                //  marker:true
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvCanvas"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
  <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Track User"></asp:Label></b>
            </div>
            
            
            <div class="MiddleContentDiv" style="padding-left: 3%;">
                <asp:RadioButton ID="rbtnAll" runat="server" Text="All Outdoor Employees" 
                    GroupName="TrackType" oncheckedchanged="rbtnAll_CheckedChanged" 
                    AutoPostBack="True" Checked="True" />
                <asp:RadioButton ID="rbtnTeamWise" runat="server" Text="Team Wise" 
                    GroupName="TrackType" oncheckedchanged="rbtnTeamWise_CheckedChanged" 
                    AutoPostBack="True" />
                <asp:RadioButton ID="rbtnSingleEmp" runat="server" Text="Single Employee" 
                    GroupName="TrackType" oncheckedchanged="rbtnSingleEmp_CheckedChanged" 
                    AutoPostBack="True" />
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblTeam" runat="server" Text="Select Team*"></asp:Label>
                <asp:DropDownList
                    ID="cmbName" runat="server" CssClass="combobox" 
                    Enabled="False" onselectedindexchanged="cmbName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="EmpMasterConDiv">
                &nbsp;</div>
            <div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnShowUserLocation" runat="server" Text="Show User Location" 
                    onclick="btnShowUserLocation_Click" />
                
            </div>
            <div style="padding-top: 5px; text-align: center;width: 97%; height: 500px">
                <div id="dvCanvas" style="width: 100%; height: 100%;"></div>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    </body>
</asp:Content>

