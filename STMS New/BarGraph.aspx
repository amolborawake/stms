﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="BarGraph.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Bar Chart"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblEmpName" runat="server" Text="Project Name*"></asp:Label>
                <asp:DropDownList ID="cmbEmpName" runat="server" CssClass="combobox" AutoPostBack="True"
                    OnSelectedIndexChanged="cmbEmpName_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbEmpName"
                    Display="Dynamic" ErrorMessage="Please Select Project Name." ForeColor="Red"
                    InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnPreview" runat="server" Text="Preview" Visible="False" />
            </div>
            <div class="EmpMasterConDiv">
                &nbsp;
            </div>
            <div style="padding-left: 9%;">
                <br />
                <table>
                    <tr>
                        <td>
                            <div style="overflow: scroll; height: 300px;">
                                <asp:GridView ID="piechartgrid" runat="server" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="TaskName" />
                                        <asp:BoundField DataField="PlanTime" HeaderText="PlanTime" />
                                        <asp:BoundField DataField="ActualTime" HeaderText="ActualTime" />
                                        <%-- <asp:boundfield datafield="PercentComplete" headertext="PercentComplete"/>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                        <td>
                            <div>
                                <%--chart div--%>
                                <asp:Chart ID="Chart1" runat="server" Width="600px" Height="300px">
                                    <Legends>
                                        <asp:Legend Alignment="Center" Name="Default" LegendStyle="Column" />
                                    </Legends>
                                    <Series>
                                        <asp:Series Name="PlanTime" ChartType="Bar">
                                        </asp:Series>
                                        <asp:Series Name="ActualTime" ChartType="Bar">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                                <%--<asp:Chart ID="Chart1" runat="server" Width="600px">
                                <Series>             
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </ChartAreas>
                               </asp:Chart>--%>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
