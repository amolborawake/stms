﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="LoginPage.aspx.cs" Inherits="LoginPage" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        
    </style>
    <script type="text/javascript">
        $(window).load(function () {
            $('#nivo-slider').nivoSlider();
        });
    </script>
    <div class="header">
        <div class="logoDiv">
            <img style="width: auto; height: auto; display: inline;" src="Images/Logo.png" />
        </div>
        <div class="title">
            <h1>
                <asp:Label ID="lblMainHeading" runat="server" Text="Resource And Project Management System"></asp:Label>
            </h1>
        </div>
        <div class="loginDisplay">
        </div>
        <div class="clear hideSkiplink">
            <asp:Menu ID="NavigationMenu" runat="server" CssClass="menu" EnableViewState="false"
                IncludeStyleBlock="false" Orientation="Horizontal">
                <Items>
                    <asp:MenuItem NavigateUrl="~/Home.aspx" Text="Home" />
                    <asp:MenuItem NavigateUrl="#" Text="Company Profile" />
                    <asp:MenuItem NavigateUrl="#" Text="Contact Us" />
                </Items>
            </asp:Menu>
        </div>
    </div>
    <div class="loginDisplay">
    </div>
    <%--<div class="SliderDiv">--%>
    <div class=" SliderDiv">
        <div id="nivo-slider" class="nivoSlider" style="margin-top: 12%;">
            <img src="Images/1.gif" alt="" />
            <img src="Images/2.png" alt="" />
            <img src="Images/4.jpg" alt="" />
            <img src="Images/6.jpg" alt="" />
            <img src="Images/7.jpg" alt="" />
            <img src="Images/8.jpg" alt="" />
        </div>
    </div>
    <%--</div>--%>
    <div class="loginMainDiv">
        <div style="text-align: center;">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <b class="login">Login</b>
                </div>
                <div style="width: 80%; margin-top: 10px;">
                    User Name
                </div>
                <div>
                    <asp:TextBox CssClass="textbox" ID="txtUserName" runat="server"></asp:TextBox>
                </div>
                <div style="width: 80%; margin-top: 10px;">
                    Password
                </div>
                <div>
                    <asp:TextBox CssClass="textbox" ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                </div>
                <div style="margin-top: 10px; color: Black; display: none;">
                    <a href="#" id="forgotpassword">Forgot Password..</a>
                </div>
                <div style="width: 90%; padding-top: 25px;">
                    <asp:Button CssClass="btn-login" ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
                    <asp:Button Visible="false" CssClass="btn" ID="btnCancel" runat="server" Text="Cancel"
                        OnClick="btnCancel_Click" />
                </div>
                <div style="width: 90%; padding-top: 5px; padding-left: 25PX;">
                    <asp:Label ID="lbl_Error" runat="server" Text="Please enter valid User Name and Password."
                        ForeColor="Red" Visible="False"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
