﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using System.Text;

public partial class TrackUserRoute : System.Web.UI.Page
{
    static int i;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rbtnTeamWise_CheckedChanged(sender, e);
        }
    }
    protected void rbtnTeamWise_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbtnSingleEmp.Checked == true)
            {
                cmbName.Enabled = true;
                lblTeam.Text = "Select Employee *";

                string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                cmbName.Items.Clear();
                cmbName.DataSource = cmd.ExecuteReader();
                cmbName.DataTextField = "Name";
                cmbName.DataValueField = "UserID";
                cmbName.DataBind();
                Connection.conn.Close();
            }
            if (rbtnTeamWise.Checked == true)
            {
                cmbName.Enabled = true;
                lblTeam.Text = "Select Team *";

                string qry = "SELECT TeamID,Name FROM tbl_TeamMaster;";

                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                cmbName.Items.Clear();
                cmbName.DataSource = cmd.ExecuteReader();
                cmbName.DataTextField = "Name";
                cmbName.DataValueField = "TeamID";
                cmbName.DataBind();
                Connection.conn.Close();
            }

        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void rbtnSingleEmp_CheckedChanged(object sender, EventArgs e)
    {
        rbtnTeamWise_CheckedChanged(sender, e);
        //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_UserMaster", Connection.conn);
        //Connection.conn.Open();
        //MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
        //cmbName.Items.Clear();

        //while (dr.Read())
        //{
        //    cmbName.Items.Add(dr[2].ToString());
        //}
        //Connection.conn.Close();
    }

    protected void btnShowRoute_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        try
        {

            
            dt.Columns.Add(new DataColumn("Latitude"));
            dt.Columns.Add(new DataColumn("Longitude"));
           // dt.Columns.Add(new DataColumn("SeqID"));

            DataRow row = dt.NewRow();

            int selectedID = int.Parse(cmbName.SelectedValue.ToString());

            string qry = "";
            if (rbtnSingleEmp.Checked == true)
            {
                qry = "Select LocationRouteDetails From " + Connection.tbl_ChkInOutDetailsName + " Where CheckOutTime is null And InddorOutdoor=1 And " + Connection.tbl_ChkInOutDetailsName + ".UserID=" + selectedID;
            }
            if (rbtnTeamWise.Checked == true)
            {
                // qry = "Select LocationRouteDetails From tbl_ChkInOutDetails, tbl_UserMaster, tbl_User_Team Where tbl_UserMaster.UserID=tbl_ChkInOutDetails.UserID And CheckOutTime is null And InddorOutdoor=1 And tbl_User_Team.UserID=tbl_ChkInOutDetails.UserID And tbl_User_Team.TeamID=" + selectedID;
            }

            // string qry = "SELECT LocationRouteDetails FROM tbl_ChkInOutDetails where CheckInOutID=33";

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            byte[] buffer = (byte[])cmd.ExecuteScalar();
            Connection.conn.Close();
            
            
            string value = ASCIIEncoding.ASCII.GetString(buffer);

            if (value != null || value != "")
            {
                string[] Temp_latLandPoints = value.Split('\n');

                string[] TempLatLongValueArray;
                ArrayList LatLongValueArray = new ArrayList();
                for (int j = 0; j < Temp_latLandPoints.Length - 1; j++)
                {
                    TempLatLongValueArray = Temp_latLandPoints[j].Split(',');
                    LatLongValueArray.Add(TempLatLongValueArray[0].ToString());
                    LatLongValueArray.Add(TempLatLongValueArray[1].ToString());
                }


                for (i = 0; i < LatLongValueArray.Count - 1; i++)
                {
                    row = dt.NewRow();
                   
                    row["Latitude"] = LatLongValueArray[i];
                    i = i + 1;
                    row["Longitude"] = LatLongValueArray[i];
                   // row["SeqID"] = i;
                    dt.Rows.Add(row);
                }
                
                i = 0;

                //DataView view = dt.DefaultView;
                //view.Sort = "SeqID ASC";
                //DataTable sortedDt = view.ToTable();

                js.Text = GPSLib.PlotGPSPoints(dt);
            }
            //DataTable dt = new DataTable();
            //dt.Columns.Add(new DataColumn("Latitude"));
            //dt.Columns.Add(new DataColumn("Longitude"));

            //DataRow row = dt.NewRow();
            //row["Latitude"] = 18.501389;
            //row["Longitude"] = 73.857924;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501489;
            //row["Longitude"] = 73.857824;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501589;
            //row["Longitude"] = 73.857524;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501689;
            //row["Longitude"] = 73.857424;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501789;
            //row["Longitude"] = 73.857624;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501889;
            //row["Longitude"] = 73.857824;
            //dt.Rows.Add(row);

            //row = dt.NewRow();
            //row["Latitude"] = 18.501989;
            //row["Longitude"] = 73.857824;
            //dt.Rows.Add(row);

            //js.Text = GPSLib.PlotGPSPoints(dt); 
        }
        catch
        {
            Connection.conn.Close();
           // Response.Redirect("TrackUserRoute.aspx");
            js.Text = GPSLib.PlotGPSPoints(dt);
        }
    }
}