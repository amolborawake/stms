﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

public partial class IssueTracker : System.Web.UI.Page
{
    string currentDate = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy").ToString();
    MySqlDataAdapter da = null;
    DataSet ds = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            getproject();
            getemp();
            BindMyIssue();
            BindIncommingIssue();
            issueSeen();
        }
    }
    public void getproject()
    {
        try
        {

            string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_ProjectMaster";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Close();
            drpproject.DataSource = cmd.ExecuteReader();
            drpproject.DataTextField = "Project Name";
            drpproject.DataValueField = "ProjID";
            drpproject.DataBind();
            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    public void getemp()
    {
        try
        {
            string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";



            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();



            MySqlDataReader reader = cmd.ExecuteReader();

            reader.Close();
            drpresorses.DataSource = cmd.ExecuteReader();
            drpresorses.DataTextField = "Employee Name";
            drpresorses.DataValueField = "UserID";
            drpresorses.DataBind();
            drpresorses.Items.Remove(drpresorses.Items.FindByValue(Session["loggedInUserID"].ToString()));


            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnRaiseIssue_Click(object sender, EventArgs e)
    {
        try
        {

            if (btnRaiseIssue.Text == "Raise Issue")
            {

                string qry = "insert into tbl_Issue(ProjectID,IssueName,Priority,Comment,OpenDate,Status) values(" + drpproject.SelectedValue + ",'" + txtIssueName.Text + "'," + ddlPriority.SelectedValue + ",'" + txtComment.Text + "','" + currentDate + "'," + 1 + ");SELECT LAST_INSERT_ID();";
                MySql.Data.MySqlClient.MySqlCommand cmd2 = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                int IssueID = Convert.ToInt32(cmd2.ExecuteScalar());
                Connection.conn.Close();




                foreach (ListItem li in drpresorses.Items)
                {
                    if (li.Selected == true)
                    {
                        int uid = Convert.ToInt32(li.Value);

                        string qryUser = "insert into tbl_Issue_User(IssueID,IssueByID,IssueToID,Status) values(" + IssueID + "," + Session["loggedInUserID"].ToString() + "," + uid + "," + 1 + ")";

                        Connection.updateData(qryUser);


                    }



                }
                Clear();
                Connection.conn.Close();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Issue Send successfully');</script>");
                BindMyIssue();
            }
            else if (btnRaiseIssue.Text == "Modify")
            {

            }


        }
        catch
        {
            Connection.conn.Close();
        }

    }
    protected void rdbtnMyIssue_CheckedChanged(object sender, EventArgs e)
    {
        IncommingIssue.Visible = false;
        panelMyIssue.Visible = true;
    }
    protected void rdbtnIncommimg_CheckedChanged(object sender, EventArgs e)
    {
        panelMyIssue.Visible = false;
        IncommingIssue.Visible = true;
        BindIncommingIssue();
    }
    public void BindIncommingIssue()
    {
        try
        {
            string qry = "SELECT tbl_Issue.IssueID, tbl_Issue.IssueName, tbl_ProjectMaster.Name,(case WHEN tbl_Issue.Priority=0 THEN 'Low' WHEN tbl_Issue.Priority=1 THEN 'Medium' ELSE 'High' END) as Priority,tbl_Issue.OpenDate,(case WHEN tbl_Issue.CloseDate IS null THEN 'Open Issue' ELSE tbl_Issue.CloseDate END ) as CloseDate,tbl_Issue.Comment,tbl_Issue.Status ,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as EmployeeName from tbl_Issue,tbl_ProjectMaster,tbl_Issue_User,tbl_EmployeeMaster,tbl_UserMaster WHERE tbl_ProjectMaster.ProjID= tbl_Issue.ProjectID AND tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID AND tbl_UserMaster.UserID=tbl_Issue_User.IssueByID AND tbl_Issue_User.IssueID=tbl_Issue.IssueID AND tbl_Issue.CloseDate is null and tbl_Issue_User.IssueToID=" + Session["loggedInUserID"] + " ORDER BY tbl_Issue.Priority DESC";
            MySql.Data.MySqlClient.MySqlCommand cmd2 = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            da = new MySqlDataAdapter(cmd2);


            Connection.conn.Open();
            ds = new DataSet();
            da.Fill(ds);
            Connection.conn.Close();
            gvIncommingIssue.DataSource = ds.Tables[0];
            gvIncommingIssue.DataBind();
        }
        catch
        {
            Connection.conn.Close();
        }


    }
    public void BindMyIssue()
    {
        try
        {
            string qry = "SELECT DISTINCT tbl_Issue.IssueID, tbl_Issue.IssueName, tbl_ProjectMaster.Name,(case WHEN tbl_Issue.Priority=0 THEN 'Low' WHEN tbl_Issue.Priority=1 THEN 'Medium' ELSE 'High' END) as Priority , tbl_Issue.OpenDate,(case WHEN tbl_Issue.CloseDate IS null THEN 'Open Issue' ELSE tbl_Issue.CloseDate END ) as CloseDate,tbl_Issue.Comment,tbl_Issue.Status from tbl_Issue,tbl_ProjectMaster,tbl_Issue_User WHERE tbl_Issue.ProjectID=tbl_ProjectMaster.ProjID and tbl_Issue.IssueID=tbl_Issue_User.IssueID AND CloseDate IS null AND tbl_Issue_User.IssueByID=" + Session["loggedInUserID"] + " ORDER BY tbl_Issue.IssueID DESC";
            MySql.Data.MySqlClient.MySqlCommand cmd2 = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            da = new MySqlDataAdapter(cmd2);


            Connection.conn.Open();
            ds = new DataSet();
            da.Fill(ds);
            Connection.conn.Close();
            gvmyIssue.DataSource = ds.Tables[0];
            gvmyIssue.DataBind();
        }
        catch
        {
            Connection.conn.Close();
        }


    }



    protected void gvmyIssue_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //gvmyIssue.EditIndex = e.NewEditIndex;
        //BindMyIssue();
    }
    protected void gvmyIssue_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //    Label IssueID = gvmyIssue.Rows[e.RowIndex].FindControl("lblIssueID") as Label;
        //    TextBox IssueName = gvmyIssue.Rows[e.RowIndex].FindControl("txtIssueName") as TextBox;
        //    TextBox Priority = gvmyIssue.Rows[e.RowIndex].FindControl("txtPriority") as TextBox;
        //    TextBox OpenDate = gvmyIssue.Rows[e.RowIndex].FindControl("txtOpenDate") as TextBox;
        //    TextBox CloseDate = gvmyIssue.Rows[e.RowIndex].FindControl("txtCloseDate") as TextBox;

        //    TextBox ProjectID = gvmyIssue.Rows[e.RowIndex].FindControl("txtProjectID") as TextBox;

        //    TextBox Comment = gvmyIssue.Rows[e.RowIndex].FindControl("txtComment") as TextBox;


        //    TextBox Status = gvmyIssue.Rows[e.RowIndex].FindControl("txtStatus") as TextBox;

        //    GridViewRow row = gvmyIssue.Rows[e.RowIndex];


        //    Connection.conn.Open();


        //    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("Update tbl_TaskMaster set where TaskID=" + Convert.ToInt32(IssueID.Text), Connection.conn);

        //    cmd.ExecuteNonQuery();
        //    Connection.conn.Close();

        //    gvmyIssue.EditIndex = -1;

        //    BindMyIssue();

    }



    protected void gvmyIssue_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int IssueID = Convert.ToInt32(e.CommandArgument);
        string update_qry = "Update tbl_Issue set CloseDate ='" + currentDate + "' where 	IssueID =" + IssueID;
        Connection.updateData(update_qry);

        BindMyIssue();
    }
    public void issueSeen()
    {
        string qury = "UPDATE tbl_Issue_User SET tbl_Issue_User.STATUS=0 WHERE tbl_Issue_User.IssueToID=" + Session["loggedInUserID"];
        Connection.conn.Open();

        MySqlCommand cmd = new MySqlCommand(qury, Connection.conn);

        cmd.ExecuteNonQuery();
        Connection.conn.Close();
    }
    public void Clear()
    {
        drpproject.SelectedIndex = -1;
        drpresorses.SelectedIndex = -1;
        ddlPriority.SelectedIndex = -1;
        txtIssueName.Text = string.Empty;
        txtComment.Text = string.Empty;
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void gvmyIssue_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvmyIssue, "Select$" + e.Row.RowIndex);
            e.Row.ToolTip = "Click to select this row.";
        }
    }

    protected void gvmyIssue_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Connection.HighLightSelectedRow(gvmyIssue);
            int i = gvmyIssue.SelectedIndex;
            string id = (gvmyIssue.SelectedRow.FindControl("lblIssueID") as Label).Text;
            string qry = "SELECT IssueToID FROM tbl_Issue_User WHERE IssueID=" + id + " AND IssueByID=" + Session["loggedInUserID"];
            MySql.Data.MySqlClient.MySqlCommand cmd2 = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            da = new MySqlDataAdapter(cmd2);


            Connection.conn.Open();
            ds = new DataSet();
            da.Fill(ds);
            Connection.conn.Close();


            foreach (DataRow table in ds.Tables[0].Rows)
            {

                drpresorses.Items.FindByValue(table["IssueToID"].ToString()).Selected = true;
            }



            //txtIssueName.Text = (gvmyIssue.SelectedRow.FindControl("lblIssueName") as Label).Text;
            ////ddlPriority.Items.FindByText((gvmyIssue.SelectedRow.FindControl("lblIPriority") as Label).Text).Selected = true;
            //txtComment.Text = (gvmyIssue.SelectedRow.FindControl("lblComment") as Label).Text;
            //drpproject.Items.FindByText((gvmyIssue.SelectedRow.FindControl("lblName") as Label).Text).Selected = true;
        }
        catch
        {
            Connection.conn.Close();
        }
    }

}

