﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
public partial class UserMaster : System.Web.UI.Page
{
    static int userMasterID;
    static ArrayList idList;
    static ArrayList nameList;
    static ArrayList EmpIdList;
    static ArrayList EmpNameList;
    public void selectdata()
    {

        //  string qryStr = "select tbl_EmployeeMaster.FirstName,tbl_EmployeeMaster.MiddleName,tbl_EmployeeMaster.SurnameName,tbl_UserMaster.Name As UserName,tbl_UserMaster.InserFlag,tbl_UserMaster.UpdateFlag from tbl_UserMaster,tbl_EmployeeMaster where tbl_UserMaster.EmpID = tbl_EmployeeMaster.EmpID";
        string qryStr = "select concat(tbl_EmployeeMaster.FirstName,' ',tbl_EmployeeMaster.MiddleName,' ',tbl_EmployeeMaster.SurnameName) As 'Employee Name',tbl_UserMaster.Name As 'User Name' from tbl_UserMaster,tbl_EmployeeMaster where tbl_UserMaster.EmpID = tbl_EmployeeMaster.EmpID";
        gv_UserMaster.DataSource = Connection.loadData(qryStr);
        gv_UserMaster.DataBind();

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                string EmpFullName;
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_EmployeeMaster", Connection.conn);
                Connection.conn.Open();
                MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
                cmbEmpName.Items.Clear();
                EmpIdList = new ArrayList();
                EmpNameList = new ArrayList();
                while (dr.Read())
                {
                    EmpFullName = dr[4].ToString() + " " + dr[5].ToString() + " " + dr[6].ToString();
                    cmbEmpName.Items.Add(EmpFullName);
                    EmpIdList.Add(dr[0]);
                    EmpNameList.Add(EmpFullName);
                }
                dr.Close();
                cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_UserMaster", Connection.conn);
                dr = cmd.ExecuteReader();

                idList = new ArrayList();
                nameList = new ArrayList();

                while (dr.Read())
                {
                    idList.Add(dr[0]);
                    nameList.Add(dr[2]);
                }

                Connection.conn.Close();
                selectdata();
            }
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        // cmbEmpName.Text = String.Empty;
        txtUserName.Text = String.Empty;
        txtPassword.Text = String.Empty;
        txtRetypePassword.Text = String.Empty;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean insertFlag = false, updateFlag = false;
            if (chkInsert.Checked == true && chkUpdate.Checked == false)
                insertFlag = true;
            else if (chkInsert.Checked == false && chkUpdate.Checked == true)
                updateFlag = true;
            else if (chkInsert.Checked == true && chkUpdate.Checked == true)
            {
                insertFlag = true;
                updateFlag = true;
            }
            else
            {
                insertFlag = false;
                updateFlag = false;
            }
            if (btnAdd.Text == "Add")
            {
                int indexOfSelItem = (int)EmpNameList.IndexOf(cmbEmpName.SelectedValue);
                int id = (int)EmpIdList[indexOfSelItem];
                int cnt = 0;
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select EmpID from tbl_UserMaster where EmpID = " + id, Connection.conn);
                Connection.conn.Open();
                MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cnt++;
                }

                dr.Close();
                Connection.conn.Close();
                if (cnt >= 1)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('User Name Already Exists');</script>");
                }
                else
                {
                    string qry = "insert into tbl_UserMaster(EmpID,Name,Password,InserFlag,UpdateFlag,Status) values(" + id + ",'" + txtUserName.Text.ToString() + "','" + txtPassword.Text.ToString() + "'," + insertFlag + "," + updateFlag + ",0)";
                    Connection.updateData(qry);
                }
                Connection.conn.Close();
            }
            else if (btnAdd.Text == "Modify")
            {
                string update_qry = "Update tbl_UserMaster set Name='" + txtUserName.Text.ToString() + "', Password ='" + txtPassword.Text.ToString() + "', InserFlag =" + insertFlag + ", UpdateFlag =" + updateFlag + "  where UserID=" + idList[gv_UserMaster.SelectedIndex];
                Connection.updateData(update_qry);
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtRetypePassword.Text = string.Empty;
                btnAdd.Text = "Add";
            }
            selectdata();

        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_UserMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Connection.HighLightSelectedRow(gv_UserMaster);
            userMasterID = gv_UserMaster.SelectedIndex;
            txtUserName.Text = gv_UserMaster.SelectedRow.Cells[1].Text.ToString();
            cmbEmpName.SelectedValue = gv_UserMaster.SelectedRow.Cells[0].Text.ToString();
            //CheckBox chk = gv_UserMaster.SelectedRow.Cells[2].Controls[0] as CheckBox;
            //if (chk != null && chk.Checked)
            //{
            //    chkInsert.Checked = true;
            //}
            //else
            //{
            //    chkInsert.Checked = false;
            //}
            //chk = gv_UserMaster.SelectedRow.Cells[3].Controls[0] as CheckBox;
            //if (chk != null && chk.Checked)
            //{
            //    chkUpdate.Checked = true;
            //}
            //else
            //{
            //    chkUpdate.Checked = false;
            //}
            btnAdd.Text = "Modify";

        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_UserMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gv_UserMaster, "Select$" + e.Row.RowIndex);
            e.Row.ToolTip = "Click to select this row.";
        }
    }
}