﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="UserMaster.aspx.cs" Inherits="UserMaster" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="User Master"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblSelectEmp" runat="server" Text="Select Employee*"></asp:Label>
                <asp:DropDownList ID="cmbEmpName" runat="server" CssClass="combobox" AutoPostBack="True">
                    <asp:ListItem>--Please Select--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbEmpName"
                    Display="Dynamic" ErrorMessage="Please select Employee" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblUserName" runat="server" Text="User Name*"></asp:Label>
                <asp:TextBox ID="txtUserName" runat="server" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName"
                    Display="Dynamic" ErrorMessage="Please Enter User Name" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUserName"
                    ErrorMessage="Enter min 6 Character. " ForeColor="Red" ValidationExpression="^[\s\S]{6,50}$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtUserName"
                    ErrorMessage="Spaces are not allowed" ForeColor="Red" ValidationExpression="[^\s]+"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblPassword" runat="server" Text="Password*"></asp:Label>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                    Display="Dynamic" ErrorMessage="Please Enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPassword"
                    ErrorMessage="Enter min 6 Character." ForeColor="Red" ValidationExpression="^[\s\S]{6,50}$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblRetypePassword" runat="server" Text="Retype Password*"></asp:Label>
                <asp:TextBox ID="txtRetypePassword" runat="server" TextMode="Password" MaxLength="100"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                    ControlToValidate="txtRetypePassword" ErrorMessage="Password not match." ForeColor="Red"></asp:CompareValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblAuthority" runat="server" Text="Authority To"
                    Visible="false"></asp:Label><asp:CheckBox ID="chkInsert" runat="server" Text="Insert"
                        Checked="true" Visible="false" /><asp:CheckBox ID="chkUpdate" Visible="false" runat="server"
                            Text="Update" />
            </div>
            <div class="EmpMasterConDiv">
                &nbsp;</div>
            <div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" ValidationGroup="UnvalidatedControls" />
                <asp:Button ID="btnClose" runat="server" Text="Close" Visible="False" />
            </div>
            <div style="text-align: center; overflow: scroll; width: 97%;" class="EmpMasterConDiv">
                <div style="height: 200px;">
                    <asp:GridView ID="gv_UserMaster" runat="server" CellPadding="4" ForeColor="#333333"
                        GridLines="Horizontal" ShowHeaderWhenEmpty="True" HorizontalAlign="Center" OnSelectedIndexChanged="gv_UserMaster_SelectedIndexChanged"
                        OnRowDataBound="gv_UserMaster_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
