﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI.DataVisualization.Charting;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            getproject();
            loadgrid();
        }

    }
    public void getproject() //rohit load project
    {
        try
        {
            // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
            string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.StartDate,tbl_ProjectMaster.EndDate,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_ProjectMaster";
            //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

            //Connection.conn.Close();
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();

            // cmd.ExecuteReader();

            MySqlDataReader reader = cmd.ExecuteReader();

            reader.Close();
            cmbEmpName.DataSource = cmd.ExecuteReader();
            cmbEmpName.DataTextField = "Project Name";
            cmbEmpName.DataValueField = "ProjID";
            cmbEmpName.DataBind();

            //drpproject.Items.Insert(0, "-- Select --");
            Connection.conn.Close();
        }
        catch (Exception ex)
        {
            Connection.conn.Close();
        }
    }
    protected void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadgrid();
    }
    public void loadgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            DataTable FromTable = new DataTable();

            int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
            MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select Name,PlanTime,ActualTime from tbl_TaskMaster where ProjID=" + id, Connection.conn);
            Connection.conn.Open();
            dataAdapter.Fill(ds);
            Connection.conn.Close();
            FromTable = ds.Tables[0];
            if (FromTable.Rows.Count > 0)
            {
                piechartgrid.DataSource = ds;
                piechartgrid.DataBind();

            }
            LoadChartData(FromTable);
        }
        catch (Exception ex)
        {
            Connection.conn.Close();
        }
    }

    private void LoadChartData(DataTable initialDataSource)
    {        

            foreach (DataRow dr in initialDataSource.Rows)
            {
                
                double y = Convert.ToDouble(dr["PlanTime"]);
                double x = Convert.ToDouble(dr["ActualTime"]);
                Chart1.Series["PlanTime"].Points.AddXY(dr["Name"].ToString(), y);
                Chart1.Series["ActualTime"].Points.AddXY(dr["Name"].ToString(), x);

                
            }
           
    }

   
}