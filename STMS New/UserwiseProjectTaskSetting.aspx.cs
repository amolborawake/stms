﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
public partial class UserwiseProjectTaskSetting : System.Web.UI.Page
{
    static ArrayList idList;
    static ArrayList nameList;
    static ArrayList ProjectIdList;
    static ArrayList ProjectNameList;
    static ArrayList arrTaskId;
    static ArrayList taskIdData;
    bool isIndexChanged = false;
    static int UserId;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            btnAdd.Text = "Add";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_UserMaster", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();

            if (lstUserName.Items.Count == 0)
            {

                idList = new ArrayList();
                nameList = new ArrayList();

                while (dr.Read())
                {
                    lstUserName.Items.Add(dr[2].ToString());

                    idList.Add(dr[0]);
                    nameList.Add(dr[2]);

                }
            }
            dr.Close();

            Connection.conn.Close();

        }
        catch
        {
            Connection.conn.Close();
        }
    }


    protected void lstUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lstProjectName.Items.Clear();
            lstTaskname.Items.Clear();
            lstProjectName.ClearSelection();
            lstTaskname.ClearSelection();
            isIndexChanged = true;

            UserId = (int)idList[lstUserName.SelectedIndex];
            string UserName = nameList[lstUserName.SelectedIndex].ToString();

            ProjectIdList = new ArrayList();
            ProjectNameList = new ArrayList();

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select Distinct tbl_ProjectMaster.ProjID, tbl_ProjectMaster.Name  from tbl_TaskMaster, tbl_User_Team, tbl_Project_Team, tbl_ProjectMaster where tbl_TaskMaster.ProjID=tbl_ProjectMaster.ProjID And tbl_Project_Team.TeamID = tbl_User_Team.TeamID AND tbl_Project_Team.ProjectID = tbl_ProjectMaster.ProjID AND tbl_User_Team.UserID = " + UserId + " AND tbl_ProjectMaster.Status = '1'", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                lstProjectName.Items.Add(dr["Name"].ToString());
                ProjectIdList.Add(dr["ProjID"]);
                ProjectNameList.Add(dr["Name"].ToString());
            }

            dr.Close();

            cmd = new MySql.Data.MySqlClient.MySqlCommand("select ProjID from tbl_User_Project where UserID = " + UserId, Connection.conn);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                lstProjectName.Items[Convert.ToInt32(ProjectIdList.IndexOf(dr["ProjID"]))].Attributes["style"] = "color:red";

            }

            dr.Close();
            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }

    protected void lstProjectName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int ProjectId = (int)ProjectIdList[lstProjectName.SelectedIndex];
            string ProjectName = ProjectNameList[lstProjectName.SelectedIndex].ToString();
            taskIdData = new ArrayList();
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_TaskMaster where ProjID = " + ProjectId + " Order By TaskID ", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            lstTaskname.Items.Clear();
            while (dr.Read())
            {
                lstTaskname.Items.Add(dr[2].ToString());
                taskIdData.Add(dr[0]);
            }

            dr.Close();

            cmd = new MySql.Data.MySqlClient.MySqlCommand("select TaskID from tbl_User_Project where ProjID = " + ProjectId + " And UserID=" + UserId, Connection.conn);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                lstTaskname.Items[Convert.ToInt32(taskIdData.IndexOf(dr["TaskID"]))].Selected = true;
            }

            dr.Close();
            Connection.conn.Close();

        }
        catch
        {
            Connection.conn.Close();
        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            int totalItemCnt = lstProjectName.Items.Count;
            int selectedItemCnt = lstProjectName.GetSelectedIndices().Count();

            string selItemDeptListBox = lstUserName.SelectedItem.ToString();

            if (selectedItemCnt == 0)
            {
                btnAdd.Text = "Add";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Please select items from Project Name');</script>");
            }
            else
            {
                btnAdd.Text = "Update";

                MySql.Data.MySqlClient.MySqlCommand cmd;
                Connection.conn.Open();

                cmd = new MySql.Data.MySqlClient.MySqlCommand("DELETE from tbl_User_Project where UserID = " + idList[lstUserName.GetSelectedIndices()[0]], Connection.conn);

                cmd.ExecuteNonQuery();
                
                string query = "";
                try
                {
                    
                    for (int i = 0; i < lstTaskname.GetSelectedIndices().Count(); i++)
                    {
                        query = query + "INSERT into tbl_User_Project values(" + idList[lstUserName.GetSelectedIndices()[0]] + "," + ProjectIdList[lstProjectName.GetSelectedIndices()[0]] + "," + taskIdData[lstTaskname.GetSelectedIndices()[i]] + ")" + ";";
                    }

                    //for (int i = 0; i < lstTaskname.GetSelectedIndices().Count();i++ )
                    //{
                    //    query=query+"INSERT into tlb tbl_User_Project values("+idList[lstUserName.GetSelectedIndices()[0]]+","+ProjectIdList[lstProjectName.GetSelectedIndices()[0]]+"."+arrTaskId[lstTaskname.GetSelectedIndices()[i]]+")"+";";
                    //}

                       //    for (int i = 0; i < lstTaskname.Items.Count; i++)
                        //    {
                        //        if (lstTaskname.Items[i].Selected == true)
                        //        {
                        //            query = query + "INSERT into tbl_User_Project values(" + idList[lstUserName.GetSelectedIndices()[0]] + ", " + ProjectIdList[lstProjectName.GetSelectedIndices()[0]] + "," + arrTaskId[lstTaskname.GetSelectedIndices()[i]] + ")" + ";";

                        //        }
                        //for (int i = 0; i < lstTaskname.GetSelectedIndices().Count(); i++)
                        //{
                        //    query = query + "INSERT into tbl_User_Project values(" + idList[lstUserName.GetSelectedIndices()[0]] + ", " + ProjectIdList[lstProjectName.GetSelectedIndices()[0]] + "," + arrTaskId[lstTaskname.GetSelectedIndices()[i]] + ")" + ";";
                        //}

                    cmd = new MySql.Data.MySqlClient.MySqlCommand(query, Connection.conn);
                    cmd.ExecuteNonQuery();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Data saved successfully');</script>");

                }
                catch (Exception odbcEx)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Selection is not properly');</script>");

                }
                Connection.conn.Close();

            }
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        lstUserName.Items.Clear();
    }
}