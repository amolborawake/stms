﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

public partial class CheckInOut : System.Web.UI.Page
{
    static int ProjID = 0, TaskID = 0, TeamID = 0;
    static int IndoorOutdoor = 0;
    string InTime, OutTime;
    static int selectedID;
    static int notCheckedOutID;
    TimeSpan totalTimeSpent = TimeSpan.Zero;
    static string firstCheckIn = "";
    static string lastCheckUut = "";
    string tbl_TimesheetName = "";
    string tbl_ChkInOutDetailsName = "";
    static string currentDate = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy").ToString();
    string[] a = new string[3];

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            totalTimeSpent = TimeSpan.Zero;
            a = currentDate.Split('/');
            tbl_TimesheetName = "tbl_Timesheet" + a[2].ToString() + a[1].ToString();
            tbl_ChkInOutDetailsName = "tbl_ChkInOutDetails" + a[2].ToString() + a[1].ToString();

            checkBreakDetail();

            if (!IsPostBack)
            {
                string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_User_Project,tbl_ProjectMaster WHERE tbl_ProjectMaster.ProjID=tbl_User_Project.ProjID and tbl_User_Project.UserID=" + Session["loggedInUserID"];
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();

                cmbProjName.DataSource = cmd.ExecuteReader();
                cmbProjName.DataTextField = "Project Name";
                cmbProjName.DataValueField = "ProjID";
                cmbProjName.DataBind();
                cmbProjName.Items.Insert(0, "-- Select --");
                Connection.conn.Close();
                selectdata();


                qry = "SELECT TeamID,Name FROM tbl_TeamMaster;";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                Connection.conn.Open();
                cmbName.Items.Clear();
                cmbName.DataSource = cmd.ExecuteReader();
                cmbName.DataTextField = "Name";
                cmbName.DataValueField = "TeamID";
                cmbName.DataBind();
                Connection.conn.Close();
            }
        }
        catch
        {
            Connection.conn.Close();
        }


    }

    void checkBreakDetail()
    {
        string date = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy");
        string qry = "Select * from " + Connection.tbl_BreakTimeName + " where Date='" + date + "' and UserID=" + Session["loggedInUserID"].ToString() + " and BreakEndTime IS NULL";
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        DataTable dt = new DataTable();
        Connection.conn.Open();
        dt.Load(cmd.ExecuteReader());
        Connection.conn.Close();
        if (dt.Rows.Count != 0)
        {
            Server.Transfer("TakeBreak.aspx");
        }
    }

    public void selectdata()
    {
        string qry = "SELECT tbl_ProjectMaster.ProjID,tbl_TaskMaster.TaskID," + Connection.tbl_ChkInOutDetailsName + ".CheckInOutID,tbl_ProjectMaster.Name as 'Project Name',tbl_TaskMaster.Name as 'Task Name'," + Connection.tbl_ChkInOutDetailsName + ".CheckInTime," + Connection.tbl_ChkInOutDetailsName + ".CheckOutTime," + Connection.tbl_ChkInOutDetailsName + ".SpentTime," + Connection.tbl_ChkInOutDetailsName + ".InddorOutdoor," + Connection.tbl_ChkInOutDetailsName + ".AddedLater from tbl_ProjectMaster inner join " + Connection.tbl_ChkInOutDetailsName + " on tbl_ProjectMaster.ProjID=" + Connection.tbl_ChkInOutDetailsName + ".ProjID inner join tbl_TaskMaster on tbl_TaskMaster.TaskID=" + Connection.tbl_ChkInOutDetailsName + ".TaskID and " + Connection.tbl_ChkInOutDetailsName + ".Date='" + (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy") + "' and UserID=" + Session["loggedInUserID"];
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();
        DataTable dt = new DataTable();
        dt.Load(cmd.ExecuteReader());
        // MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
        gv_CheckInOut.DataSource = dt;
        gv_CheckInOut.DataBind();

        if (dt.Rows.Count == 0)
        {
            btnChkOut.Enabled = false;
            btnChkIn.Enabled = true;
            btnUpdate.Enabled = true;
            btnTakeBreak.Enabled = false;
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            firstCheckIn = dt.Rows[0]["CheckInTime"].ToString();
            if (dt.Rows[dt.Rows.Count - 1]["CheckOutTime"].ToString() != "")
            {
                lastCheckUut = dt.Rows[dt.Rows.Count - 1]["CheckOutTime"].ToString();
            }


            if (dt.Rows[i]["CheckOutTime"].ToString() == "")
            {
                btnChkOut.Enabled = true;
                btnChkIn.Enabled = false;
                btnUpdate.Enabled = false;
                btnTakeBreak.Enabled = true;
                btnChangeTask.Enabled = true;
                notCheckedOutID = (int)dt.Rows[i]["CheckInOutID"];
            }
            else
            {
                btnChkOut.Enabled = false;
                btnChkIn.Enabled = true;
                // btnUpdate.Enabled = true;
                btnTakeBreak.Enabled = false;
                btnChangeTask.Enabled = false;
            }
        }
        Connection.conn.Close();


    }


    protected void cmbProjName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            cmbTaskName.Items.Clear();

            ProjID = Convert.ToInt16(cmbProjName.SelectedItem.Value.ToString());

            string qry = "SELECT DISTINCT tbl_TaskMaster.TaskID,tbl_TaskMaster.Name as 'Task Name' FROM tbl_User_Project,tbl_TaskMaster WHERE tbl_TaskMaster.TaskID=tbl_User_Project.TaskID and tbl_User_Project.ProjID=" + ProjID + " and tbl_User_Project.UserID=" + Session["loggedInUserID"];
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();

            cmbTaskName.DataSource = cmd.ExecuteReader();
            cmbTaskName.DataTextField = "Task Name";
            cmbTaskName.DataValueField = "TaskID";
            cmbTaskName.DataBind();
            cmbTaskName.Items.Insert(0, "-- Select --");
            Connection.conn.Close();
            selectdata();

        }
        catch
        {
            Connection.conn.Close();
        }

    }
    protected void btnChkIn_Click(object sender, EventArgs e)
    {
        //try
        //{

        ArrayList chkValueList = new ArrayList();
        ArrayList UIDList = new ArrayList();
        int i = 0;
        bool chkBoxSelectFlag = false;
        foreach (GridViewRow gvrow in GridView1.Rows)
        {
            CheckBox chk = (CheckBox)gvrow.FindControl("chkStatus");
            UIDList.Add(GridView1.Rows[i].Cells[2].Text.ToString());


            if (chk != null)
            {
                if (chk.Checked == true)
                {
                    chkBoxSelectFlag = true;
                    string str_dayChkInOutID = UIDList[i].ToString() + (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("ddMMyyyy");
                    int dayChkInOutID = int.Parse(str_dayChkInOutID);
                    TaskID = Convert.ToInt16(cmbTaskName.SelectedItem.Value.ToString());
                    string Intime = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("HH:mm"); // to manage server time
                    string qry = "insert into " + Connection.tbl_ChkInOutDetailsName + "(DayCheckInOutID,Date,UserID,ProjID,TaskID,InddorOutdoor,CheckInTime) values(" + dayChkInOutID + ",'" + (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("dd/MM/yyyy") + "'," + UIDList[i].ToString() + "," + ProjID + "," + TaskID + "," + IndoorOutdoor + ",'" + Intime + "'); SELECT LAST_INSERT_ID();";

                    MySql.Data.MySqlClient.MySqlCommand cmd2 = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                    Connection.conn.Open();
                    notCheckedOutID = Convert.ToInt32(cmd2.ExecuteScalar());
                    Connection.conn.Close();
                }
            }
            i++;
        }
        if (chkBoxSelectFlag == true)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Selected Employee Checked In Successfully.');</script>");
        }

        btnChkIn.Enabled = false;
        btnChkOut.Enabled = true;
        btnChangeTask.Enabled = true;


        //selectdata();

        //}
        //catch
        //{
        //    Connection.conn.Close();
        //}
    }
    protected void btnChkOut_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList chkValueList = new ArrayList();
            ArrayList UIDList = new ArrayList();
            int i = 0;
            bool chkBoxSelectFlag = false;
            foreach (GridViewRow gvrow in GridView1.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkStatus");
                UIDList.Add(GridView1.Rows[i].Cells[2].Text.ToString());


                if (chk != null)
                {
                    if (chk.Checked == true)
                    {
                        int approveStatusFlag = 0;
                        string recordFoundDate = "";
                        checkoutMethod();
                        selectdata();
                        btnChkIn.Enabled = true;

                        InTime = firstCheckIn;//cmbInHrs.Text.ToString() + ":" + cmbInMin.Text.ToString();
                        OutTime = lastCheckUut;//cmbOutHrs.Text.ToString() + ":" + cmbOutMin.Text.ToString();

                        string qry = "SELECT Date FROM " + tbl_ChkInOutDetailsName + " where Date='" + currentDate + "' and UserID=" + UIDList[i].ToString() + " and CheckOutTime is not null";
                        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
                        Connection.conn.Open();
                        MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            recordFoundDate = dr["Date"].ToString();
                        }
                        Connection.conn.Close();


                        string qry1 = "SELECT * FROM " + tbl_TimesheetName + " where UserID=" + UIDList[i].ToString() + " and date='" + currentDate + "'";
                        MySql.Data.MySqlClient.MySqlCommand cmd1 = new MySql.Data.MySqlClient.MySqlCommand(qry1, Connection.conn);
                        Connection.conn.Open();
                        MySql.Data.MySqlClient.MySqlDataReader dr1 = cmd1.ExecuteReader();
                        while (dr1.Read())
                        {
                            approveStatusFlag = (int)dr1["ApproveStatus"];
                        }


                        if (currentDate == recordFoundDate)
                        {
                            if (dr1.HasRows == false)
                            {
                                dr1.Close();
                                string str_dayChkInOutID = UIDList[i].ToString() + a[0].ToString() + a[1].ToString() + a[2].ToString();
                                int dayChkInOutID = int.Parse(str_dayChkInOutID);

                                string qry2 = "insert into " + tbl_TimesheetName + "(UserID,DayCheckInOutID,Date,Intime,Outtime,ApproveStatus) values(" + UIDList[i].ToString() + "," + dayChkInOutID + ",'" + currentDate + "','" + InTime + "','" + OutTime + "',0)";
                                Connection.updateData(qry2);
                            }
                            else
                            {
                                dr1.Close();
                                if (approveStatusFlag == 1)
                                {
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Timesheet already approved.');</script>");
                                }
                                else
                                {
                                    string qry2 = "Update " + tbl_TimesheetName + " set Intime='" + InTime + "', Outtime='" + OutTime + "' where UserID=" + UIDList[i].ToString() + " and Date='" + currentDate + "'";
                                    Connection.updateData(qry2);
                                }
                            }

                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Check In Out record Not found');</script>");
                        }
                        Connection.conn.Close();

                        //showData();
                    }
                }
            }
            i++;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Timesheet Submited Successfully.');</script>");
        }
        catch
        {
            Connection.conn.Close();
        }
    }


    protected void gv_CheckInOut_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[8].Text.ToString() == "0")
                    e.Row.Cells[8].Text = "Indoor";
                else if (e.Row.Cells[8].Text.ToString() == "1")
                    e.Row.Cells[8].Text = "Outdoor";

                if (e.Row.Cells[9].Text.ToString() == "&nbsp;")
                    e.Row.Cells[9].Text = "NO";
                //else if (e.Row.Cells[9].Text.ToString() == "Added Later")
                //    e.Row.Cells[9].ForeColor = Color.Red;

                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gv_CheckInOut, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Click to select this row.";
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl = new Label();
                lbl.Text = "Worked Time =";
                e.Row.Cells[6].Controls.Add(lbl);
                Label lbl1 = new Label();
                lbl1.Text = totalTimeSpent.ToString();
                e.Row.Cells[7].Controls.Add(lbl1);

            }

            if (e.Row.Cells[7].Text.ToString() != "SpentTime" && e.Row.Cells[7].Text.ToString() != "&nbsp;")
            {
                string s = e.Row.Cells[7].Text.ToString();
                if (s.Length < 2)
                {
                    s = "0" + e.Row.Cells[7].Text.ToString() + ".00";
                    e.Row.Cells[7].Text = s;
                }
                else
                {
                    string[] temp = new string[2];
                    string hr, mm;
                    temp = s.Split('.');
                    if (temp[0].ToString().Length < 2)
                        hr = "0" + temp[0].ToString();
                    else
                        hr = temp[0].ToString();

                    if (temp[1].ToString().Length < 2)
                        mm = temp[1].ToString() + "0";
                    else
                        mm = temp[1].ToString();

                    s = hr + "." + mm;
                    e.Row.Cells[7].Text = s;

                }
                string a = s.Replace(".", ":");
                totalTimeSpent = totalTimeSpent.Add(TimeSpan.Parse(a));

            }
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            btnUpdate.Enabled = false;
            btnChkIn.Enabled = true;

            string update_qry = "Update " + Connection.tbl_ChkInOutDetailsName + " set ProjID=" + ProjID + ",TaskID=" + TaskID + ",InddorOutdoor=" + IndoorOutdoor + " where CheckInOutID=" + selectedID;
            Connection.updateData(update_qry);
            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_CheckInOut_RowCreated(object sender, GridViewRowEventArgs e) //To hide the coloumn from gridview
    {
        try
        {
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;

        }
        catch
        {

        }
    }
    protected void cmbTaskName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //TaskID = (int)List_TaskID[List_TaskName.IndexOf(cmbTaskName.SelectedValue)];
        }
        catch
        {
        }
    }
    protected void rbtnIndoor_CheckedChanged(object sender, EventArgs e)
    {
        IndoorOutdoor = 0;

    }
    protected void rbtnOutdoor_CheckedChanged(object sender, EventArgs e)
    {
        IndoorOutdoor = 1;
    }
    protected void btnTakeBreak_Click(object sender, EventArgs e)
    {
        Server.Transfer("TakeBreak.aspx");
    }
    protected void btnChangeTask_Click(object sender, EventArgs e)
    {
        //if (btnChangeTask.Text == "Change Task")
        //{

        checkoutMethod();
        //    btnChangeTask.Text = "Save";
        //    btnChangeTask.Enabled = true;
        //    btnChkIn.Enabled = false;
        //    btnChkOut.Enabled = true;
        //}
        //else if (btnChangeTask.Text == "Save")
        //{    

        //gv_CheckInOut = new GridView();//to prenvent double timespent addition
        btnChkIn_Click(sender, e);
        //    btnChangeTask.Text = "Change Task";
        //}
    }
    public void checkoutMethod()
    {
        try
        {
            //btnChkIn.Enabled = true;
            btnChkOut.Enabled = false;
            cmbProjName.Enabled = true;
            cmbTaskName.Enabled = true;
            btnTakeBreak.Enabled = false;
            string Intime = string.Empty;
            ArrayList breakFromTime = new ArrayList();
            ArrayList breakToTime = new ArrayList();

            string qry1 = "SELECT FromTime,ToTime FROM tbl_BreakTimeMaster";
            MySql.Data.MySqlClient.MySqlCommand cmd1 = new MySql.Data.MySqlClient.MySqlCommand(qry1, Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                breakFromTime.Add(dr1[0].ToString());
                breakToTime.Add(dr1[1].ToString());
            }
            Connection.conn.Close();

            string qry = "SELECT CheckInTime FROM " + Connection.tbl_ChkInOutDetailsName + " where CheckInOutID=" + notCheckedOutID;
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Intime = dr[0].ToString();
            }
            Connection.conn.Close();


            string Outtime = (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("HH:mm"); // to manage server time
            string difference = DateTime.Parse(Outtime).Subtract(DateTime.Parse(Intime)).ToString("t");
            string breakTime = "00:00";


            for (int i = 0; i < breakFromTime.Count; i++)
            {
                if (DateTime.Parse(breakFromTime[i].ToString()) > DateTime.Parse(Intime) && DateTime.Parse(breakToTime[i].ToString()) < DateTime.Parse(Outtime))
                {
                    breakTime = TimeSpan.Parse(breakTime.ToString()).Add((TimeSpan.Parse(breakToTime[i].ToString()).Subtract(TimeSpan.Parse(breakFromTime[i].ToString())))).ToString();
                }
            }
            difference = DateTime.Parse(difference).Subtract(DateTime.Parse(breakTime)).ToString("t");

            string spentTime1 = DateTime.Parse(difference).ToString("HH:mm");
            double spentTime = double.Parse(spentTime1.Replace(":", "."));
            //string update_qry = "Update tbl_ChkInOutDetails set CheckOutTime='" + System.DateTime.Now.ToShortTimeString() + "' where CheckInOutID=" + notCheckedOutID;
            string update_qry = "Update " + Connection.tbl_ChkInOutDetailsName + " set CheckOutTime='" + Outtime + "', SpentTime=" + spentTime + " where CheckInOutID=" + notCheckedOutID;
            Connection.updateData(update_qry);
            //selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }

    protected void cmbName_SelectedIndexChanged(object sender, EventArgs e)
    {
        TeamID = Convert.ToInt16(cmbName.SelectedItem.Value.ToString());

        string qry = "SELECT concat(tbl_EmployeeMaster.FirstName,' ',tbl_EmployeeMaster.MiddleName,' ',tbl_EmployeeMaster.SurnameName) AS 'Employee Name', tbl_UserMaster.UserID FROM tbl_User_Team,tbl_EmployeeMaster,tbl_Attendence,tbl_UserMaster WHERE tbl_UserMaster.EmpID=tbl_EmployeeMaster.EmpID AND tbl_User_Team.UserID=tbl_UserMaster.UserID AND tbl_Attendence.UID=tbl_User_Team.UserID AND tbl_Attendence.Date=" + (System.DateTime.Now.AddHours(12).AddMinutes(30)).ToString("ddMMyyyy").ToString() + " AND tbl_User_Team.TeamID=" + TeamID + " and tbl_Attendence.Outtime=''";
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();

        GridView1.DataSource = cmd.ExecuteReader();
        GridView1.DataBind();

        Connection.conn.Close();
    }
}