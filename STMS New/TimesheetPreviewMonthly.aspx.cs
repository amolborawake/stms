﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
public partial class CheckInOut : System.Web.UI.Page
{
    String[] timeList;
    int m;
    public void getEmployeeNames()
    {
        string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();
        cmbEmpName.Items.Clear();      
        cmbEmpName.DataSource = cmd.ExecuteReader();
        cmbEmpName.DataTextField = "Employee Name";
        cmbEmpName.DataValueField = "UserID";
        cmbEmpName.DataBind();

        Connection.conn.Close();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (!Page.IsPostBack)
        {
          
            cmbYear.Items.FindByValue(System.DateTime.Now.Year.ToString()).Selected = true;  //set current year as selected
            string mnth = System.DateTime.Now.Month.ToString();
            if (mnth.Length == 1)
                mnth = "0" + mnth;
            cmbMonth.ClearSelection();
            cmbMonth.Items.FindByValue(mnth).Selected = true;
            getEmployeeNames();

            gv_TimesheetMonthly.DataSource = null;
            gv_TimesheetMonthly.DataBind();
            btnPreviewMonth_Click(sender, e);
        }
    }

    protected void btnPreviewMonth_Click(object sender, EventArgs e)
    {
        try
        {
           // gv_TimesheetMonthly.ClearSelection();


            string tbl_ChkInOutDetailsName = "tbl_ChkInOutDetails" + cmbYear.SelectedItem.Value.ToString() + cmbMonth.SelectedItem.Value.ToString();
            Connection.conn.Open();
            string dateformat = string.Empty;
            DataSet ds = new DataSet();

            //string qry = "SELECT tbl_ProjectMaster.Name as 'Project Name'," + tbl_ChkInOutDetailsName + ".SpentTime as 'SpentTime'," + tbl_ChkInOutDetailsName + ".Date FROM tbl_ProjectMaster," + tbl_ChkInOutDetailsName + " WHERE tbl_ProjectMaster.ProjID=" + tbl_ChkInOutDetailsName + ".ProjID and " + tbl_ChkInOutDetailsName + ".UserID=" + int.Parse(cmbEmpName.SelectedItem.Value.ToString());//+ " and " + tbl_ChkInOutDetailsName + ".Date='" + dateformat + "/07/2015' group by tbl_ProjectMaster.Name"; //group by "  + Connection.tbl_ChkInOutDetailsName + ".Date";
            string qry = "SELECT tbl_ProjectMaster.Name as 'Project Name'," + tbl_ChkInOutDetailsName + ".SpentTime as 'SpentTime'," + tbl_ChkInOutDetailsName + ".Date FROM tbl_TaskMaster,tbl_ProjectMaster," + tbl_ChkInOutDetailsName + " WHERE tbl_ProjectMaster.ProjID=" + tbl_ChkInOutDetailsName + ".ProjID and tbl_TaskMaster.TaskID=" + tbl_ChkInOutDetailsName + ".TaskID and " + tbl_ChkInOutDetailsName + ".UserID=" + int.Parse(cmbEmpName.SelectedItem.Value.ToString());//+ " and " + tbl_ChkInOutDetailsName + ".Date='" + dateformat + "/07/2015' group by tbl_ProjectMaster.Name"; //group by "  + Connection.tbl_ChkInOutDetailsName + ".Date";
            MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(qry, Connection.conn);
            da.SelectCommand.CommandTimeout = 120;
            da.Fill(ds);
            //gv_TimesheetMonthly.DataSource = ds;
            //gv_TimesheetMonthly.DataBind();

            DataTable dt = new DataTable();
            dt.Columns.Add("Project Name");
            for (int i = 1; i <= 31; i++)
            {
                string day = string.Empty;
                if (i.ToString().Length == 1)
                    day = "0" + i.ToString();
                else
                    day = i.ToString();

                string ColumnName = day;// +"/" + cmbMonth.SelectedItem.Value.ToString() + "/" + cmbYear.SelectedItem.Value.ToString();
                dt.Columns.Add(ColumnName);
            }
            DataView view = ds.Tables[0].DefaultView;
            DataTable distinctValues = view.ToTable(true, "Project Name");
            TimeSpan totalSpentTime = TimeSpan.Zero;
            timeList = new String[100];
            TimeSpan ts = TimeSpan.Zero;
            foreach (DataRow drProjName in distinctValues.Rows)
            {
                DataRow drnewRow = dt.NewRow();

                int flagProjName = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    if (drProjName["Project Name"].Equals(dr["Project Name"]))
                    {
                        if (flagProjName == 0)
                        {
                            drnewRow["Project Name"] = dr["Project Name"];
                            flagProjName = 1;
                        }
                        string[] a = new string[3];
                        a = (dr["Date"].ToString()).Split('/');
                        string newRowName = a[0].ToString();
                        string[] tempTime = (dr["SpentTime"]).ToString().Split('.');
                        string strtime = Convert.ToString((dr["SpentTime"]).ToString());
                        if (tempTime.Length > 1)
                        {
                            if (tempTime[1].Length == 1)
                            {
                                tempTime[1] = tempTime[1] + "0";
                            }
                            strtime = tempTime[0] + ":" + tempTime[1];
                        }
                        //string strtime = Convert.ToString((dr["SpentTime"]).ToString().Replace('.', ':'));
                        if (strtime.Length == 1) { strtime = "0" + strtime + ":00"; }
                        if (strtime != "")
                        {

                            ts = TimeSpan.Zero;


                            string str = drnewRow[newRowName].ToString();
                            if (str == null || str == "") { }
                            else
                            {
                                ts = ts + TimeSpan.Parse(str, CultureInfo.InvariantCulture);
                            }
                            ts = ts + TimeSpan.Parse(strtime, CultureInfo.InvariantCulture);

                            drnewRow[newRowName] = (ts.ToString()).Substring(0, 5);


                            string str1 = timeList[Convert.ToInt32(newRowName)];
                            if (str1 == null) { totalSpentTime = TimeSpan.Zero; }
                            else
                            {
                                totalSpentTime = TimeSpan.Parse(str1);
                            }
                            totalSpentTime = totalSpentTime + TimeSpan.Parse(strtime, CultureInfo.InvariantCulture);
                            timeList[Convert.ToInt32(newRowName)] = (totalSpentTime.ToString()).Substring(0, 5);


                        }
                    }
                }
                dt.Rows.Add(drnewRow);
            }

            gv_TimesheetMonthly.DataSource = dt;
            gv_TimesheetMonthly.DataBind();
            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    public decimal total { get; set; }
    protected void gv_TimesheetMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lbl = new Label();
            lbl.Text = "Total";
            e.Row.Cells[0].Controls.Add(lbl);
            Label lbl1 = new Label();

            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                string str = timeList[i];
                if (str == null) { str = ""; }
                else
                {
                    str = str.Substring(0, 5);
                }
                e.Row.Cells[i].Text = str;
            }

        }

    }
    protected void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        gv_TimesheetMonthly.DataSource = null;
        gv_TimesheetMonthly.DataBind();
        btnPreviewMonth_Click(sender, e);
    }
    protected void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        gv_TimesheetMonthly.DataSource = null;
        gv_TimesheetMonthly.DataBind();
        btnPreviewMonth_Click(sender, e);
    }
    protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        gv_TimesheetMonthly.DataSource = null;
        gv_TimesheetMonthly.DataBind();
        btnPreviewMonth_Click(sender, e);
    }
}
