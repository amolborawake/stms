﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Net;
/// <summary>
/// Summary description for Connection
/// </summary>
public class Connection
{
    public static MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
    //public static int loggedInEmpCode;
    //public static string loggedInEmpName;
    //public static int loggedInUserID;
    // public static int selected_DayCheckInOutID;
    //public static int selected_CheckInOutID;
    // public static int CheckInOutIDForSign;
    public static string tblNameYearMonth = string.Empty;
    public static string tbl_ChkInOutDetailsName = string.Empty;
    public static string tbl_TimesheetName = string.Empty;
    public static string tbl_BreakTimeName = string.Empty;
    public static bool CheckForInternetConnection()
    {
        try
        {
            using (var client = new WebClient())
            using (var stream = client.OpenRead("http://www.google.com"))
            {
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public static IDataReader loadData(string qry)
    {
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        if (Connection.conn.State == ConnectionState.Closed)
            Connection.conn.Open();
        MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
        var dt = new DataTable();
        dt.Load(dr);
        Connection.conn.Close();
        return dt.CreateDataReader();
    }

    //public static void insertData(string qry)
    //{
    //    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
    //    Connection.conn.Open();
    //    cmd.ExecuteNonQuery();
    //    Connection.conn.Close();
    //}

    public static void updateData(string qry)
    {
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        if (Connection.conn.State == ConnectionState.Closed)
            Connection.conn.Open();
        cmd.ExecuteNonQuery();
        Connection.conn.Close();
    }

    public static void HighLightSelectedRow(GridView gv_temp)
    {
        foreach (GridViewRow row in gv_temp.Rows)
        {
            if (row.RowIndex == gv_temp.SelectedIndex)
            {
                row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                row.ToolTip = string.Empty;
            }
            else
            {
                row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                row.ToolTip = "Click to select this row.";
            }
        }
    }

    public static TimeSpan getTotalBrakeTime(string date, int uid)
    {
        string[] a = new string[3];
        a = date.Split('/');
        string tbl_BreakTimeName = "tbl_BreakTime" + a[2].ToString() + a[1].ToString();


        string qry = "Select TotalBreakTime from " + tbl_BreakTimeName + " where Date='" + date + "' and UserID=" + uid;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        DataTable dt = new DataTable();
        Connection.conn.Open();
        //string totalbreak=cmd.ExecuteScalar().ToString();
        dt.Load(cmd.ExecuteReader());
        Connection.conn.Close();
        TimeSpan ts = TimeSpan.Zero;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ts = ts + TimeSpan.Parse(dt.Rows[i]["TotalBreakTime"].ToString().Replace(".", ":"));
        }
        //string totalbreak = ts.ToString();

        return ts;
    }

}