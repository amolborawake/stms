﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    public static string message { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string saveproject()
    {
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
        Connection.conn.Open();
        string oldpassword = HttpContext.Current.Request.QueryString["oldpassword"];
        string newpassword = HttpContext.Current.Request.QueryString["newpassword"];
        int id = Convert.ToInt16(HttpContext.Current.Session["loggedInUserID"]);
        string cmdstr = "SELECT Password FROM tbl_UserMaster where UserID=" + id;
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(cmdstr, Connection.conn);
        MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
        adp.Fill(ds);
        cmd.ExecuteNonQuery();
        Connection.conn.Close();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            if (oldpassword != "" && newpassword != "")
            {
                string p=FromTable.Rows[0]["Password"].ToString();
                if (oldpassword == p)
                {
                    string qry = "Update tbl_UserMaster set Password='" + newpassword + "' where UserID=" + id;
                    Connection.updateData(qry);
                    message = "Password Updated";
                    return message;
                }
                else
                {
                    message = "Old Password Not Match";
                    return message;
                }
            }
            else
            {
                message = "Please Enter Proper Password";
                return message;
            }

        }
        Connection.conn.Close();

        return message;

    }
}