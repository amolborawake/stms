﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="TimesheetPreviewDaily.aspx.cs" Inherits="CheckInOut" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=dtpDate.ClientID %>").dynDateTime({
                showsTime: false,
                ifFormat: "%d/%m/%Y",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: true,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });

    </script>
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 1%; height: 21px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Daily Timesheet Preview"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblEmpName" runat="server" Text="Employee Name*"></asp:Label><asp:DropDownList
                    ID="cmbEmpName" runat="server" CssClass="combobox" AutoPostBack="True" 
                    onselectedindexchanged="cmbEmpName_SelectedIndexChanged">
                    <asp:ListItem>-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbEmpName"
                    Display="Dynamic" ErrorMessage="Please Select Employee." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblSelectDate" runat="server" Text="Select Date*"></asp:Label>
                <asp:TextBox ID="dtpDate" runat="server" AutoPostBack="True" 
                    ontextchanged="dtpDate_TextChanged"></asp:TextBox><img src="Images/calender.png" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="dtpDate"
                    Display="Dynamic" ErrorMessage="Select Date" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <%--<div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnPreview" runat="server" Text="Preview" 
                    OnClick="btnPreview_Click" Visible="False" /></div>--%>
           <div style="text-align: center; overflow: scroll; width: 97%;" class="EmpMasterConDiv">
                <div style="height: 180px;">
                <br />
                <asp:GridView ID="gv_TimesheetDaily" HorizontalAlign="Center" runat="server" CellPadding="4"
                    ForeColor="#333333" GridLines="Horizontal" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                    OnRowDataBound="gv_TimesheetDaily_RowDataBound" ShowFooter="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField HeaderText="Project Name" DataField="Project Name" />
                        <asp:BoundField HeaderText="Task Name" DataField="Task Name" />
                        <asp:BoundField HeaderText="Intime(HH:mm)" DataField="CheckInTime" />
                        <asp:BoundField HeaderText="Outtime(HH:mm)" DataField="CheckOutTime" />
                        <asp:BoundField HeaderText="Spent Time(HH:mm)" DataField="SpentTime" />
                        <asp:BoundField HeaderText="Added Later" DataField="AddedLater" />
                        <asp:BoundField HeaderText="InddorOutdoor" DataField="InddorOutdoor" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
             </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
