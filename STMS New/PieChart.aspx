﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true" CodeFile="PieChart.aspx.cs" Inherits="_Default" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="PI Chart"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblEmpName" runat="server" Text="Project Name*"></asp:Label>
                <asp:DropDownList ID="cmbEmpName" runat="server" CssClass="combobox" 
                    AutoPostBack="True" onselectedindexchanged="cmbEmpName_SelectedIndexChanged" >
                  </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbEmpName"
                    Display="Dynamic" ErrorMessage="Please Select Project Name." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            
            
            <div style="padding-top: 5px; text-align: center;">
                <asp:Button ID="btnPreview" runat="server" Text="Preview" 
                    onclick="btnPreview_Click" Visible="False" /></div>
            <div class="EmpMasterConDiv">
                &nbsp;</div>
            <div style="padding-left:20%;">
                <br />
                <table>
                <tr>
                  <td>
                     <div>
                       <asp:GridView ID="piechartgrid" runat="server"></asp:GridView>
                     </div>
                  </td>
                  <td>
                      <div>
                   <%--<asp:Chart ID="Chart1" runat="server">
                           <Series>                    
                                  <asp:Series Name="PlanStart" IsValueShownAsLabel="true" ChartType="Pie" LegendText="Apple"></asp:Series>                  
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <Area3DStyle Enable3D="True" />
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>--%>
                        <asp:chart id="Chart1" runat="server" Width="400px">
                          <titles>
                            <asp:Title ShadowOffset="3" Name="Title1" />
                          </titles>
                          <legends>
                            <asp:Legend Alignment="Center" Name="Default" LegendStyle="Column" />
                          </legends>
                          <series>
                            <asp:Series Name="Default" ChartType="Pie" />
                          </series>
                          <chartareas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                          </chartareas>
                        </asp:chart>
                        </div>
                  </td>
                </tr>
                </table>
                
               
            </div>
            
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>

