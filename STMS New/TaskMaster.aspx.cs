﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
public partial class TaskMaster : System.Web.UI.Page
{
    static int TaskID;
    static ArrayList idList;
    static ArrayList nameList;
    static int selectedProjectID;
    public void selectdata()
    {

        string qryStr = "select tbl_TaskMaster.TaskID,tbl_ProjectMaster.Name as 'Project Name',tbl_TaskMaster.Name as 'Task Name',tbl_TaskMaster.TaskDetails as 'Task Details',tbl_TaskMaster.PlanStart as 'Plan Start Date',tbl_TaskMaster.PlanEnd as 'Plan End Date'  from tbl_ProjectMaster,tbl_TaskMaster where tbl_ProjectMaster.ProjID=tbl_TaskMaster.ProjID";
        gv_TaskMaster.DataSource = Connection.loadData(qryStr);
        gv_TaskMaster.DataBind();


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            PlanStartDate.Attributes.Add("readonly", "readonly");
            PlanEndDate.Attributes.Add("readonly", "readonly");

           // set as current month &year as selected 23/03/2016
            PlanStartDate.Text = System.DateTime.Now.AddHours(12).AddMinutes(30).ToString("dd/MM/yyyy");
            selectedProjectID = cmbProjectName.SelectedIndex;

            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_ProjectMaster", Connection.conn);
            //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select tbl_ProjectMaster.Name,tbl_TaskMaster.Name,tbl_TaskMaster.TaskDetails from tbl_ProjectMaster,tbl_TaskMaster where tbl_ProjectMaster.ProjID=tbl_TaskMaster.ProjID", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            cmbProjectName.Items.Clear();
            idList = new ArrayList();
            nameList = new ArrayList();
            while (dr.Read())
            {
                cmbProjectName.Items.Add(dr[1].ToString());
                idList.Add(dr[0]);
                nameList.Add(dr[1]);
            }
            cmbProjectName.Items.Insert(0, "--Please Select--");

            Connection.conn.Close();

            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        PlanStartDate.Text = String.Empty;
        PlanEndDate.Text = String.Empty;
        txtTaskName.Text = String.Empty;
        txtTaskDetails.Text = String.Empty;
        btnAdd.Text = "Add";
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (btnAdd.Text == "Add")
            {
                string qry = "insert into tbl_TaskMaster(Name,TaskDetails,PlanStart,PlanEnd,ProjID) values('" + txtTaskName.Text.ToString() + "','" + txtTaskDetails.Text.ToString() + "','" + PlanStartDate.Text.ToString() + "','" + PlanEndDate.Text.ToString() + "'," + idList[selectedProjectID - 1] + ")";
                Connection.updateData(qry);
                
            }
            else if (btnAdd.Text == "Modify")
            {
                string update_qry = "Update tbl_TaskMaster set Name='" + txtTaskName.Text.ToString() + "',TaskDetails='" + txtTaskDetails.Text.ToString() + "',PlanStart='" + PlanStartDate.Text.ToString() + "',PlanEnd='" + PlanEndDate.Text.ToString() + "' where TaskID=" + TaskID;
                Connection.updateData(update_qry);
                btnAdd.Text = "Add";
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Details saved successfully');</script>");
            selectdata();
            PlanStartDate.Text = String.Empty;
            PlanEndDate.Text = String.Empty;
            txtTaskName.Text = String.Empty;
            txtTaskDetails.Text = String.Empty;

        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TaskMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Connection.HighLightSelectedRow(gv_TaskMaster);
            TaskID = int.Parse(gv_TaskMaster.SelectedRow.Cells[0].Text);
            cmbProjectName.SelectedValue = gv_TaskMaster.SelectedRow.Cells[1].Text.ToString();
            txtTaskName.Text = gv_TaskMaster.SelectedRow.Cells[2].Text.ToString();
            txtTaskDetails.Text = gv_TaskMaster.SelectedRow.Cells[3].Text.ToString();
            PlanStartDate.Text = gv_TaskMaster.SelectedRow.Cells[4].Text.ToString();
            PlanEndDate.Text = gv_TaskMaster.SelectedRow.Cells[5].Text.ToString();
            btnAdd.Text = "Modify";
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TaskMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gv_TaskMaster, "Select$" + e.Row.RowIndex);
            e.Row.ToolTip = "Click to select this row.";
        }
        e.Row.Cells[0].Visible = false;
    }
}