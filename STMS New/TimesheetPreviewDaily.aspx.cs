﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

public partial class CheckInOut : System.Web.UI.Page
{
    TimeSpan totalTimeSpent = TimeSpan.Zero;
    public void getEmployeeNames()
    {
        string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
       // Connection.conn.Close();
        Connection.conn.Open();
        cmbEmpName.Items.Clear();
        cmbEmpName.DataSource = cmd.ExecuteReader();
        cmbEmpName.DataTextField = "Employee Name";
        cmbEmpName.DataValueField = "UserID";
        cmbEmpName.DataBind();
        Connection.conn.Close();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        dtpDate.Attributes.Add("readonly", "readonly");
           
        if (!Page.IsPostBack)
        {
            dtpDate.Text = System.DateTime.Now.AddHours(12).AddMinutes(30).ToString("dd/MM/yyyy"); //22/02/2016
        
            getEmployeeNames();
            loadrecord();
        }
    }
    public void loadrecord()
    {
        try
        {
            string[] a = new string[3];
            string date = dtpDate.Text.ToString();
            a = date.Split('/');
            string tbl_ChkInOutDetailsName = "tbl_ChkInOutDetails" + a[2].ToString() + a[1].ToString(); //Convert.ToDateTime(dtpDate.Text).ToString("yyyyMM"); //Convert.ToDateTime(Convert.ToDateTime(dtpDate.Text).ToString("dd/MM/yyyy")).ToString("yyyyMM");

            string qry = "SELECT tbl_ProjectMaster.Name as 'Project Name',tbl_TaskMaster.Name as 'Task Name'," + tbl_ChkInOutDetailsName + ".CheckInTime," + tbl_ChkInOutDetailsName + ".CheckOutTime," + tbl_ChkInOutDetailsName + ".SpentTime," + tbl_ChkInOutDetailsName + ".AddedLater, (CASE WHEN " + tbl_ChkInOutDetailsName + ".InddorOutdoor='0' THEN 'Indoor' WHEN " + tbl_ChkInOutDetailsName + ".InddorOutdoor='1' THEN 'Outdoor' END) as 'InddorOutdoor' FROM tbl_ProjectMaster,tbl_TaskMaster," + tbl_ChkInOutDetailsName + " WHERE tbl_ProjectMaster.ProjID=" + tbl_ChkInOutDetailsName + ".ProjID and tbl_TaskMaster.TaskID=" + tbl_ChkInOutDetailsName + ".TaskID and " + tbl_ChkInOutDetailsName + ".UserID=" + int.Parse(cmbEmpName.SelectedItem.Value.ToString()) + " and " + tbl_ChkInOutDetailsName + ".Date='" + dtpDate.Text.ToString() + "'";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            gv_TimesheetDaily.DataSource = Connection.loadData(qry);
            gv_TimesheetDaily.DataBind();
            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnPreview_Click(object sender, EventArgs e)
    {
        try
        {
            string[] a = new string[3];
            string date = dtpDate.Text.ToString();
            a = date.Split('/');
            string tbl_ChkInOutDetailsName = "tbl_ChkInOutDetails" + a[2].ToString() + a[1].ToString(); //Convert.ToDateTime(dtpDate.Text).ToString("yyyyMM"); //Convert.ToDateTime(Convert.ToDateTime(dtpDate.Text).ToString("dd/MM/yyyy")).ToString("yyyyMM");

            string qry = "SELECT tbl_ProjectMaster.Name as 'Project Name',tbl_TaskMaster.Name as 'Task Name'," + tbl_ChkInOutDetailsName + ".CheckInTime," + tbl_ChkInOutDetailsName + ".CheckOutTime," + tbl_ChkInOutDetailsName + ".SpentTime," + tbl_ChkInOutDetailsName + ".AddedLater, " + tbl_ChkInOutDetailsName + ".InddorOutdoor" + " FROM tbl_ProjectMaster,tbl_TaskMaster," + tbl_ChkInOutDetailsName + " WHERE tbl_ProjectMaster.ProjID=" + tbl_ChkInOutDetailsName + ".ProjID and tbl_TaskMaster.TaskID=" + tbl_ChkInOutDetailsName + ".TaskID and " + tbl_ChkInOutDetailsName + ".UserID=" + int.Parse(cmbEmpName.SelectedItem.Value.ToString()) + " and " + tbl_ChkInOutDetailsName + ".Date='" + dtpDate.Text.ToString() + "'";
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
            Connection.conn.Open();
            gv_TimesheetDaily.DataSource = Connection.loadData(qry);
            gv_TimesheetDaily.DataBind();
            Connection.conn.Close();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TimesheetDaily_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells[4].Text.ToString() != "Spent Time(HH:mm)" && e.Row.Cells[4].Text.ToString() != "&nbsp;")
        {
            string s = e.Row.Cells[4].Text.ToString();
            if (s.Length < 2)
            {
                s = "0" + e.Row.Cells[4].Text.ToString() + ".00";
                e.Row.Cells[4].Text = s;
            }
            else
            {
                string[] temp = new string[2];
                string hr, mm;
                temp = s.Split('.');
                if (temp[0].ToString().Length < 2)
                    hr = "0" + temp[0].ToString();
                else
                    hr = temp[0].ToString();

                if (temp[1].ToString().Length < 2)
                    mm = temp[1].ToString() + "0";
                else
                    mm = temp[1].ToString();

                s = hr + "." + mm;
                e.Row.Cells[4].Text = s;

                //if (e.Row.Cells[6].Text.ToString() == "0")
                //    e.Row.Cells[6].Text = "Indoor";
                //else if (e.Row.Cells[6].Text.ToString() == "1")
                //    e.Row.Cells[6].Text = "Outdoor";
            }
            string a = s.Replace(".", ":");
            totalTimeSpent = totalTimeSpent.Add(TimeSpan.Parse(a));

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lbl = new Label();
            lbl.Text = "Total Worked Time =";
            e.Row.Cells[3].Controls.Add(lbl);

            string date = dtpDate.Text.ToString();
            int uid = int.Parse(cmbEmpName.SelectedItem.Value.ToString());
            TimeSpan TotalBrakeTime = Connection.getTotalBrakeTime(date, uid);

            //for fix break time deduction
            //TimeSpan fixBreakTimeTotal = TimeSpan.Zero;
            //string qryStr = "select BreakTime from tbl_BreakTimeMaster";
            //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qryStr, Connection.conn);
            //Connection.conn.Open();
            //MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            //while (dr.Read())
            //{
            //    fixBreakTimeTotal = fixBreakTimeTotal.Add(TimeSpan.Parse(dr[0].ToString()));
            //}
            //Connection.conn.Close();

            Label lbl1 = new Label();
            //lbl1.Text = totalTimeSpent.Subtract(TotalBrakeTime).Subtract(fixBreakTimeTotal).ToString();
            lbl1.Text = totalTimeSpent.Subtract(TotalBrakeTime).ToString();
            e.Row.Cells[4].Controls.Add(lbl1);

        }
        if (e.Row.RowType != DataControlRowType.Footer)
        {
            if (e.Row.Cells[5].Text.ToString() == "&nbsp;")
            {
                e.Row.Cells[5].Text = "NO";
            }
        }
    }
    protected void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //btnPreview_Click(sender, e);
        loadrecord();
    }
    protected void dtpDate_TextChanged(object sender, EventArgs e)
    {
        btnPreview_Click(sender, e);
    }
}