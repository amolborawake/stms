﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmployeeMasterPage.master" AutoEventWireup="true"
    CodeFile="TeamCheckInOut.aspx.cs" Inherits="CheckInOut" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="float: right; width: 82%; background-color: aliceblue; min-height: 420px;
        height: auto; padding-bottom: 5%;">
        <div style="padding-left: 5%;">
            <div align="center" style="padding-top: 2%; height: 48px;">
                <b>
                    <asp:Label ID="lblHeadings" runat="server" Text="Check In-Out"></asp:Label></b>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblProjName" runat="server" Text="Project Name*"></asp:Label><asp:DropDownList
                    ID="cmbProjName" runat="server" CssClass="combobox" OnSelectedIndexChanged="cmbProjName_SelectedIndexChanged"
                    AutoPostBack="True">
                    <asp:ListItem>-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbProjName"
                    Display="Dynamic" ErrorMessage="Please Select Project." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv">
                <asp:Label CssClass="lblWidth" ID="lblTaskName" runat="server" Text="Task Name*"></asp:Label><asp:DropDownList
                    ID="cmbTaskName" runat="server" CssClass="combobox" OnSelectedIndexChanged="cmbTaskName_SelectedIndexChanged">
                    <asp:ListItem>-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="cmbTaskName"
                    Display="Dynamic" ErrorMessage="Please Select Task." ForeColor="Red" InitialValue="-- Select --"></asp:RequiredFieldValidator>
            </div>
            <div class="MiddleContentDiv" style="padding-left: 13%;">
                <asp:RadioButton ID="rbtnIndoor" runat="server" Text="Indoor" GroupName="CheckInOutType"
                    Checked="True" OnCheckedChanged="rbtnIndoor_CheckedChanged" Visible="False" />
                <asp:RadioButton ID="rbtnOutdoor" runat="server" Text="Outdoor" GroupName="CheckInOutType"
                    OnCheckedChanged="rbtnOutdoor_CheckedChanged" Visible="False" />
            </div>
        </div>
        <div class="EmpMasterConDiv">
            &nbsp;</div>
        <div style="display: -webkit-box; padding-left: 16%;">
            <div style="padding-left: 5%;">
                <asp:Label CssClass="lblWidth" ID="lblTeam" Width="100px" runat="server" Text="Select Team*"></asp:Label>
                <asp:DropDownList ID="cmbName" runat="server" Width="120px" OnSelectedIndexChanged="cmbName_SelectedIndexChanged"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div style="display: -webkit-box; padding-left: 5%; height: 150px;">
                <div>
                    <asp:Label CssClass="lblWidth" ID="Label1" Width="100px" runat="server" Text="Select Team Member*"></asp:Label>
                </div>
                <div style="overflow: scroll; width: 220px; margin-left: 5%;">
                    <asp:GridView ID="GridView1" HorizontalAlign="Center" runat="server" CellPadding="4"
                        ForeColor="#333333" GridLines="Horizontal" ShowHeaderWhenEmpty="True" 
                        AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkStatus" runat="server"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Employee Name" DataField="Employee Name" />
                            <asp:BoundField HeaderText="User ID" DataField="UserID" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
        <br />
        <div style="padding-top: 5px; text-align: center;">
            <asp:Button ID="btnChkIn" runat="server" Text="Check In" OnClick="btnChkIn_Click" />
            <asp:Button ID="btnChangeTask" runat="server" Text="Change Task" Enabled="False"
                OnClick="btnChangeTask_Click" />
            <asp:Button ID="btnChkOut" runat="server" Text="Check Out" OnClick="btnChkOut_Click"
                CausesValidation="False" />
            <asp:Button ID="btnTakeBreak" runat="server" Text="Take Break" OnClick="btnTakeBreak_Click"
                Visible="False" />
            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"
                Enabled="False" Visible="False" />
        </div>
        <div style="overflow: scroll">
            <br />
            <asp:GridView ID="gv_CheckInOut" HorizontalAlign="Center" runat="server" CellPadding="4"
                ForeColor="#333333" GridLines="Horizontal" ShowHeaderWhenEmpty="True" OnRowCreated="gv_CheckInOut_RowCreated"
                OnRowDataBound="gv_CheckInOut_RowDataBound" ShowFooter="True" Visible="False">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
    </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
