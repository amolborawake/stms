﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class TeamMaster : System.Web.UI.Page
{
    static int TeamID;
    static ArrayList idList;
    static ArrayList nameList;
    public void selectdata()
    {
        string qryStr = "select Name as 'Team' from tbl_TeamMaster";
        gv_TeamMaster.DataSource = Connection.loadData(qryStr);
        gv_TeamMaster.DataBind();


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from tbl_TeamMaster", Connection.conn);
            Connection.conn.Open();
            MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
            idList = new ArrayList();
            nameList = new ArrayList();
            while (dr.Read())
            {
                idList.Add(dr[0]);
                nameList.Add(dr[1]);
            }

            dr.Close();
            Connection.conn.Close();
            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtTeamName.Text = String.Empty;
        btnAdd.Text = "Add";
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnAdd.Text == "Add")
            {
                string qry = "insert into tbl_TeamMaster(Name) values('" + txtTeamName.Text.ToString() + "')";
                Connection.updateData(qry);
            }
            else if (btnAdd.Text == "Modify")
            {
                string update_qry = "Update tbl_TeamMaster set Name='" + txtTeamName.Text.ToString() + "' where TeamID=" + idList[gv_TeamMaster.SelectedIndex];
                Connection.updateData(update_qry);
                txtTeamName.Text = string.Empty;
                btnAdd.Text = "Add";
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Details saved successfully');</script>");
            txtTeamName.Text = String.Empty;

            selectdata();
        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TeamMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Connection.HighLightSelectedRow(gv_TeamMaster);
            TeamID = gv_TeamMaster.SelectedIndex;
            txtTeamName.Text = gv_TeamMaster.SelectedRow.Cells[0].Text.ToString();
            btnAdd.Text = "Modify";

        }
        catch
        {
            Connection.conn.Close();
        }
    }
    protected void gv_TeamMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gv_TeamMaster, "Select$" + e.Row.RowIndex);
            e.Row.ToolTip = "Click to select this row.";
        }
    }
}