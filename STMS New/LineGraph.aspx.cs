﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            getproject();
            loadgrid();
        }
    }
    //public void chartbasic()
    //{
    //    //chtCategoriesProductCountBarChart.Series["Categories"].XValueMember = "Date";
    //    //chtCategoriesProductCountBarChart.Series["Categories"].YValueMembers = "PlannedTime";
    //    //chtCategoriesProductCountBarChart.DataSource = lst1.ToList();
    //    //chtCategoriesProductCountBarChart.DataBind(); 

    //    if (lst1.Count() > 0)
    //    {
    //        Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
    //        Chart1.BorderlineColor = System.Drawing.Color.FromArgb(26, 59, 105);
    //        Chart1.BorderlineWidth = 3;
    //        Chart1.BackColor = Color.Empty;

    //        Chart1.ChartAreas.Add("chtArea");
    //        Chart1.ChartAreas[0].AxisX.Title = "Date";
    //        Chart1.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Verdana", 11, System.Drawing.FontStyle.Bold);
    //        Chart1.ChartAreas[0].AxisY.Title = "Hours";
    //        Chart1.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Verdana", 11, System.Drawing.FontStyle.Bold);
    //        Chart1.ChartAreas[0].BorderDashStyle = ChartDashStyle.Solid;
    //        Chart1.ChartAreas[0].BorderWidth = 2;

    //        Chart1.Legends.Add("ActualTime");
    //        Chart1.Series.Add("ActualTime");
    //        Chart1.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
    //        Chart1.Series[0].Points.DataBindXY(lst1, "Date", lst1, "ActualTime");

    //        Chart1.Series[0].IsVisibleInLegend = true;
    //        Chart1.Series[0].IsValueShownAsLabel = true;
    //        Chart1.Series[0].ToolTip = "Data Point Y Value: #VALY{G}";

    //        // Setting Line Width
    //        Chart1.Series[0].BorderWidth = 3;
    //        Chart1.Series[0].Color = Color.Red;


    //        Chart1.Series.Add("PlannedTime");
    //        Chart1.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
    //        Chart1.Series[1].Points.DataBindXY(lst1, "Date", lst1, "PlannedTime");

    //        Chart1.Series[1].IsVisibleInLegend = true;
    //        Chart1.Series[1].IsValueShownAsLabel = true;
    //        Chart1.Series[1].ToolTip = "Data Point Y Value: #VALY{G}";

    //        Chart1.Series[1].BorderWidth = 3;
    //        Chart1.Series[1].Color = Color.Green;

    //        // Setting Line Shadow
    //        //Chart1.Series[0].ShadowOffset = 5;

    //        //Legend Properties
    //        Chart1.Legends[0].LegendStyle = LegendStyle.Table;
    //        Chart1.Legends[0].TableStyle = LegendTableStyle.Wide;
    //        Chart1.Legends[0].Docking = Docking.Bottom;
            
    //    }


    //}

    public void chartbasic()
    {
        //chtCategoriesProductCountBarChart.Series["Categories"].XValueMember = "Date";
        //chtCategoriesProductCountBarChart.Series["Categories"].YValueMembers = "PlannedTime";
        //chtCategoriesProductCountBarChart.DataSource = lst1.ToList();
        //chtCategoriesProductCountBarChart.DataBind(); 

        if (lst1.Count() > 0)
        {
            Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
            Chart1.BorderlineColor = System.Drawing.Color.FromArgb(26, 59, 105);
            Chart1.BorderlineWidth = 3;
            Chart1.BackColor = Color.Empty;

            Chart1.ChartAreas.Add("chtArea");
            Chart1.ChartAreas[0].AxisX.Title = "Date";
            Chart1.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Verdana", 11, System.Drawing.FontStyle.Bold);
            Chart1.ChartAreas[0].AxisY.Title = "Hours";
            Chart1.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Verdana", 11, System.Drawing.FontStyle.Bold);
            Chart1.ChartAreas[0].BorderDashStyle = ChartDashStyle.Solid;
            Chart1.ChartAreas[0].BorderWidth = 2;

            Chart1.Legends.Add("ActualTime");
            Chart1.Series.Add("ActualTime");
            Chart1.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
            Chart1.Series[0].Points.DataBindXY(lst1, "Date", lst1, "ActualTime");

            Chart1.Series[0].IsVisibleInLegend = true;
            Chart1.Series[0].IsValueShownAsLabel = true;
            Chart1.Series[0].ToolTip = "Data Point Y Value: #VALY{G}";

            // Setting Line Width
            Chart1.Series[0].BorderWidth = 3;
            Chart1.Series[0].Color = Color.Red;

           


            Chart1.Series.Add("PlannedTime");
            Chart1.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
            Chart1.Series[1].Points.DataBindXY(lst1, "Date", lst1, "PlannedTime");

            Chart1.Series[1].IsVisibleInLegend = true;
            Chart1.Series[1].IsValueShownAsLabel = true;
            Chart1.Series[1].ToolTip = "Data Point Y Value: #VALY{G}";

            Chart1.Series[1].BorderWidth = 3;
            Chart1.Series[1].Color = Color.Green;

            



            Chart1.Series.Add("Complete(%)");
            Chart1.Series[2].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StepLine;
            Chart1.Series[2].Points.DataBindXY(lst1, "Date", lst1, "PercentComplete");

            Chart1.Series[2].IsVisibleInLegend = true;
            Chart1.Series[2].IsValueShownAsLabel = false;
            Chart1.Series[2].ToolTip = "Data Point Y Value: #VALY{G}";

            Chart1.Series[2].BorderWidth = 3;
            Chart1.Series[2].Color = Color.Blue;
           
            


            //Legend Properties
            Chart1.Legends[0].LegendStyle = LegendStyle.Table;
            Chart1.Legends[0].TableStyle = LegendTableStyle.Wide;
            Chart1.Legends[0].Docking = Docking.Bottom;

        }


    }
    public void getproject() //rohit load project
    {
        // string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";
        string qry = "SELECT DISTINCT tbl_ProjectMaster.ProjID,tbl_ProjectMaster.StartDate,tbl_ProjectMaster.EndDate,tbl_ProjectMaster.Name as 'Project Name' FROM tbl_ProjectMaster";
        //string qry = "SELECT tbl_UserMaster.UserID,concat(FirstName ,' ', MiddleName,' ' , SurnameName) as 'Employee Name',tbl_UserMaster.Name FROM tbl_EmployeeMaster,tbl_UserMaster where tbl_EmployeeMaster.EmpID=tbl_UserMaster.EmpID";

        //Connection.conn.Close();
        MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(qry, Connection.conn);
        Connection.conn.Open();

        // cmd.ExecuteReader();

        MySqlDataReader reader = cmd.ExecuteReader();
        //projectList = new List<Tuple<string, string, string>>();
        //while (reader.Read())
        //{
        //    projectList.Add(new Tuple<string, string, string>(reader["Project Name"] as string, reader["StartDate"] as string, reader["EndDate"] as string));
        //}
        reader.Close();
        cmbEmpName.DataSource = cmd.ExecuteReader();
        cmbEmpName.DataTextField = "Project Name";
        cmbEmpName.DataValueField = "ProjID";
        cmbEmpName.DataBind();

        //drpproject.Items.Insert(0, "-- Select --");
        Connection.conn.Close();
    }
    List<lineclass> lst = new List<lineclass>();
    List<addlineclass> lst1 = new List<addlineclass>();
    public void loadgrid()
   {
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
       
        int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
        MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select StartDate,EndDate,PlanTime,ActualTime from tbl_ProjectMaster where ProjID=" + id, Connection.conn);
        Connection.conn.Open();
        dataAdapter.Fill(ds);
        Connection.conn.Close();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            string startdt1 = Convert.ToString(FromTable.Rows[0]["StartDate"].ToString());
            string enddt2 = Convert.ToString(FromTable.Rows[0]["EndDate"].ToString());
            double ptime= Convert.ToDouble(FromTable.Rows[0]["PlanTime"].ToString());
            DateTime dt1 = Convert.ToDateTime(convertdate(startdt1));
            DateTime dt2 = Convert.ToDateTime(convertdate(enddt2));

            double count = (dt2 - dt1).TotalDays;

            double stime = ptime / (count+1);


            for (DateTime date = dt1; date <= dt2; date = date.AddDays(1))//use get days
            {
                lineclass l = new lineclass();

                string dq = Convert.ToDateTime(date).ToShortDateString();
                string dq1 = convertdate1(dq);



                MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter1 = new MySql.Data.MySqlClient.MySqlDataAdapter("Select SpentTime,Date from tbl_ProjectDailySpentTime where ProjID=" + id + " and Date='" + dq1 + "'", Connection.conn);
                Connection.conn.Open();
                ds = new DataSet();
                dataAdapter1.Fill(ds);
                Connection.conn.Close();
                DataTable FromTable1 = new DataTable();
                FromTable1 = ds.Tables[0];
                if (FromTable1.Rows.Count > 0)
                {
                    double spenttime = Convert.ToDouble(FromTable1.Rows[0]["SpentTime"].ToString());
                    string spentdate = Convert.ToString(FromTable1.Rows[0]["Date"].ToString());
                    DateTime dt3 = Convert.ToDateTime(convertdate(spentdate));
                    if (date == dt3)
                    {
                        l.Date = date;
                        l.ActualTime = spenttime;
                        l.PlannedTime = stime;
                        //l.TotalTime=l.ActualTime-

                        lst.Add(l);
                    }
                   
                }
                else
                {
                    l.Date = date;
                    l.ActualTime = 0.00;
                    l.PlannedTime = stime;

                    lst.Add(l);
                }
                              

            }
        }

        piechartgrid.DataSource = addlist(lst).ToList();
        piechartgrid.DataBind();

        chartbasic();

       // piechartgrid.DataSource = ds;
       // piechartgrid.DataBind();
    }

    public void loadgrid1()
    {
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();

        int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
        MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select StartDate,EndDate,PlanTime,ActualTime from tbl_ProjectMaster where ProjID=" + id, Connection.conn);
        Connection.conn.Open();
        dataAdapter.Fill(ds);
        Connection.conn.Close();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            string startdt1 = Convert.ToString(FromTable.Rows[0]["StartDate"].ToString());
            string enddt2 = Convert.ToString(FromTable.Rows[0]["EndDate"].ToString());
            double ptime = Convert.ToDouble(FromTable.Rows[0]["PlanTime"].ToString());
            DateTime dt1 = Convert.ToDateTime(convertdate(startdt1));
            DateTime dt2 = Convert.ToDateTime(convertdate(enddt2));

            double count = (dt2 - dt1).TotalDays;
            double stime = ptime; /// count;


            for (DateTime date = dt1; date <= dt2; date = date.AddDays(1))//use get days
            {
                lineclass l = new lineclass();

                string dq = Convert.ToDateTime(date).ToShortDateString();
                string dq1 = convertdate1(dq);



                MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter1 = new MySql.Data.MySqlClient.MySqlDataAdapter("Select SpentTime,Date from tbl_ProjectDailySpentTime where ProjID=" + id + " and Date='" + dq1 + "'", Connection.conn);
                Connection.conn.Open();
                ds = new DataSet();
                dataAdapter1.Fill(ds);
                Connection.conn.Close();
                DataTable FromTable1 = new DataTable();
                FromTable1 = ds.Tables[0];
                if (FromTable1.Rows.Count > 0)
                {
                    double spenttime = Convert.ToDouble(FromTable1.Rows[0]["SpentTime"].ToString());
                    string spentdate = Convert.ToString(FromTable1.Rows[0]["Date"].ToString());
                    DateTime dt3 = Convert.ToDateTime(convertdate(spentdate));
                    if (date == dt3)
                    {
                        l.Date = date;
                        l.ActualTime = spenttime;
                        l.PlannedTime = stime;
                        //l.TotalTime=l.ActualTime-

                        lst.Add(l);
                    }

                }
                else
                {
                    l.Date = date;
                    l.ActualTime = 0.00;
                    l.PlannedTime = stime;

                    lst.Add(l);
                }


            }
        }

        piechartgrid.DataSource = addlist1(lst).ToList();
        piechartgrid.DataBind();

        chartbasic();

        // piechartgrid.DataSource = ds;
        // piechartgrid.DataBind();
    }
    public double  sum12 { get; set; }
    public double sum13 { get; set; }
    public double  cal { get; set; }

    public void getline()//use constant line
    {
        DataSet ds = new DataSet();
        DataTable FromTable = new DataTable();
       
        int id = Convert.ToInt32(cmbEmpName.SelectedItem.Value);
        MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter("Select PlanTime,Percetageofcom from tbl_ProjectMaster where ProjID=" + id, Connection.conn);
        Connection.conn.Open();
        dataAdapter.Fill(ds);
        Connection.conn.Close();
        FromTable = ds.Tables[0];
        if (FromTable.Rows.Count > 0)
        {
            double ptime= Convert.ToDouble(FromTable.Rows[0]["PlanTime"].ToString());
            double pertime = Convert.ToDouble(FromTable.Rows[0]["Percetageofcom"].ToString());
            double d = ptime * pertime / 100;
            cal = d;
        }
    }

    public List<addlineclass> addlist(List<lineclass> lst)
   {
       getline();
        foreach (var temp in lst)
        {
            
            addlineclass q = new addlineclass();
            string t = Convert.ToDateTime(temp.Date).ToString("MM/dd/yyyy");
            q.Date =t;
            sum12 += temp.ActualTime;
            q.ActualTime =Math.Round(sum12);
            sum13 += temp.PlannedTime;
            q.PlannedTime = Math.Round(sum13);
            q.PercentComplete = cal;
            lst1.Add(q);
        }

        return lst1;
    }
    public List<addlineclass> addlist1(List<lineclass> lst)
    {
        getline();
        foreach (var temp in lst)
        {
            addlineclass q = new addlineclass();
            string t = Convert.ToDateTime(temp.Date).ToString("MM/dd/yyyy");
            q.Date = t;
            sum12 += temp.ActualTime;
            q.ActualTime = Math.Round(sum12);
            sum13 = temp.PlannedTime;
            q.PlannedTime = Math.Round(sum13);
            q.PercentComplete = cal;
            lst1.Add(q);
        }

        return lst1;
    }

    public string convertdate(string dt) //use mm/dd/yyyy
    {
        string date = dt;
        string[] a = new string[3];
        a = date.Split('/');

        string rdate = a[1].ToString() +"/"+ a[0].ToString() +"/"+a[2].ToString();
        return rdate;
    }
    public string convertdate1(string dt) //use dd/mm/yyyy
    {
        string date = dt;
        string[] a = new string[3];
        a = date.Split('/');

        string rdate = a[0].ToString() + "/" + a[1].ToString() + "/" + a[2].ToString();
        //DateTime df = Convert.ToDateTime(rdate);
        string dg = Convert.ToDateTime(rdate).ToString("dd/MM/yyyy");
        return dg;
    }
    protected void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string value = Convert.ToString(drpcharttype.SelectedItem.Value);
        if (value == "LineChart1")
        {
            loadgrid();
        }
        else
        {
            if (value == "LineChart2")
            {
                loadgrid1();
            }
        }
    }
    protected void drpcharttype_SelectedIndexChanged(object sender, EventArgs e)
    {
        string value = Convert.ToString(drpcharttype.SelectedItem.Value);
        if (value == "LineChart1")
        {
            loadgrid();
        }
        else
        {
            if (value == "LineChart2")
            {
                loadgrid1();
            }
        }
    }
}
public class lineclass
{
    public DateTime Date { get; set; }
    public double ActualTime { get; set; }
    public double PlannedTime { get; set; }
    public string PercentComplete { get; set; }
}
public class addlineclass
{
    public string Date { get; set; }
    public double ActualTime { get; set; }
    public double PlannedTime { get; set; }
    public double PercentComplete { get; set; }
    
}